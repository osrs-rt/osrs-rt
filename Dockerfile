FROM node:lts-alpine

# Setting working directory. All the path will be relative to WORKDIR
WORKDIR /usr/src/app

# Installing dependencies
COPY package*.json ./
COPY yarn.lock ./
RUN yarn --frozen-lockfile --production

# Copying source files
#COPY server server
#COPY databases databases
#COPY sharedCode sharedCode

# Copy build output
COPY dist dist

# Building app
#RUN yarn build:graphql
#RUN yarn build:database

# Running the app
CMD [ "yarn", "start:graphql" ]
