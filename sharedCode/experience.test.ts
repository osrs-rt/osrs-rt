import { getExperienceForLevel, getLevelForExperience } from './experience';

describe('getExperienceForLevel', () => {
  it('should throw if level is less than 1', function() {
    const fn = () => getExperienceForLevel(0);

    expect(fn).toThrow('Cannot calculate experience for a level less than 1');
  });

  it('should return 200,000,000 if passed a level greater than 126', function() {
    const xp = getExperienceForLevel(127);

    expect(xp).toBe(200000000);
  });

  it('should return 0 when given a level of 1', () => {
    const xp = getExperienceForLevel(1);

    expect(xp).toBe(0);
  });

  it('should return 83 when given a level of 2', () => {
    const xp = getExperienceForLevel(2);

    expect(xp).toBe(83);
  });

  it('should return 174 when given a level of 3', () => {
    const xp = getExperienceForLevel(3);

    expect(xp).toBe(174);
  });

  it('should return 1154 when given a level of 10', () => {
    const xp = getExperienceForLevel(10);

    expect(xp).toBe(1154);
  });

  it('should return 4470 when given a level of 20', () => {
    const xp = getExperienceForLevel(20);

    expect(xp).toBe(4470);
  });

  it('should return 101333 when given a level of 50', () => {
    const xp = getExperienceForLevel(50);

    expect(xp).toBe(101333);
  });

  it('should return 13034431 when given a level of 99', () => {
    const xp = getExperienceForLevel(99);

    expect(xp).toBe(13034431);
  });
});

describe('getLevelForExperience', () => {
  it('should return 1 when passed an xp value of 0', () => {
    const level = getLevelForExperience(0);

    expect(level).toBe(1);
  });

  it('should return 2 when passed an xp value of 83', () => {
    const level = getLevelForExperience(83);

    expect(level).toBe(2);
  });

  it('should return 99 when passed an xp value of 200,000,000 and is not including virtual levels', () => {
    const level = getLevelForExperience(200000000, false);

    expect(level).toBe(99);
  });

  it('should return 126 when passed an xp value of 200,000,000 and is including virtual levels', () => {
    const level = getLevelForExperience(200000000, true);

    expect(level).toBe(126);
  });
});
