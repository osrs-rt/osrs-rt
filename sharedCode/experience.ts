const cache: { [index: number]: number } = {};

/** The maximum XP for a skill defined by Jagex is 200 million XP */
const MAX_XP = 200000000;

export function getExperienceForLevel(level: number) {
  if (level < 1)
    throw new Error('Cannot calculate experience for a level less than 1');
  if (level > 126) return MAX_XP; // 200mil is the max amount of xp
  if (level === 1) return 0;

  if (cache[level]) {
    return cache[level];
  }

  const forThisLevel = 0.25 * sum(level - 1);

  const xp = Math.floor(forThisLevel);
  cache[level] = xp;
  return xp;
}

function sum(level: number): number {
  if (level === 0) return 0;

  return Math.floor(level + 300 * Math.pow(2, level / 7)) + sum(level - 1);
}

export function getLevelForExperience(xp: number, includeVirtual: boolean = false): number {
  let level = 1;
  let xpForLevel;
  do {
    level++;

    if (!includeVirtual && level > 99) break;

    xpForLevel = getExperienceForLevel(level)
  } while (xp >= xpForLevel && xpForLevel !== MAX_XP);
  return level - 1;
}
