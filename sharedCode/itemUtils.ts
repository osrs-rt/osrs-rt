import get from 'lodash/get';


const COINS_IDS = [617, 995, 6964, 8890, 14440, 18028];

export function calculatePrice(item: { quantity: number }) {
  const rsbuddy = get(item, ['price', 'rsbuddy'], 0);
  const runelite = get(item, ['price', 'runelite'], 0);
  const price = runelite | rsbuddy;
  return item.quantity * price;
}

export function isCoins(itemId: number): boolean {
  return COINS_IDS.includes(itemId);
}

// TODO: USE REAL TYPE
export function itemDtoToModel({ item, ...others }: any) {
  return {
    ...item,
    ...others
  };
}
