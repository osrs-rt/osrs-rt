import React, { ComponentType, ErrorInfo } from 'react';
import initApollo from './initApollo';
import Head from 'next/head';
import { getDataFromTree } from '@apollo/react-ssr';
import { NextComponentType, NextPageContext} from 'next';
import { Router } from 'next/router';
import { ApolloClient } from 'apollo-boost';
import { AppProps } from 'next/app';
import sentry from '../utils/sentry';

const { captureException } = sentry();

interface Context extends NextPageContext {
  Component: ComponentType;
  router: Router;
}

interface ApolloProps extends AppProps {
  httpUri: string;
  websocketUri: string;
  apolloState?: any;
}

export interface ApolloClientProps extends AppProps {
  apolloClient: ApolloClient<any>;
  error?: Error;
}

type AppNode = NextComponentType<any, any, ApolloClientProps>;

interface ApolloAppState {
  error?: Error;
  errorEventId?: string;
}

export default (App: AppNode) => {
  return class Apollo extends React.Component<ApolloProps, ApolloAppState> {
    static displayName = 'withApollo(App)';
    private readonly apolloClient: ApolloClient<any>;
    static async getInitialProps(ctx: Context) {
      const { Component, router } = ctx;

      let appProps = {};
      if (App.getInitialProps) {
        appProps = await App.getInitialProps(ctx);
      }

      // Run all GraphQL queries in the component tree
      // and extract the resulting data

      const apollo = initApollo(null);
      try {
        if (!process.browser) {
          // Run all GraphQL queries
          await getDataFromTree(
            <App
              pageProps={appProps}
              Component={Component}
              router={router}
              apolloClient={apollo}
            />
          );

          // getDataFromTree does not call componentWillUnmount
          // head side effect therefore need to be cleared manually
          Head.rewind();
        }
      } catch (error) {
        // Prevent Apollo Client GraphQL errors from crashing SSR.
        // Handle them in components via the data.error prop:
        // https://www.apollographql.com/docs/react/api/react-apollo.html#graphql-query-data-error
        console.error('Error while running `getDataFromTree`', error);

        // Capture errors that happen during a page's getInitialProps.
        // This will work on both client and server sides.
        const errorEventId = captureException(error, ctx);
        return {
          hasError: true,
          errorEventId,
          pageProps: {},
        };
      }

      // Extract query data from the Apollo store
      const apolloState = apollo.cache.extract();

      return {
        pageProps: appProps,
        apolloState
      };
    }

    static getDerivedStateFromProps(props: any, state: ApolloAppState) {
      // If there was an error generated within getInitialProps, and we haven't
      // yet seen an error, we add it to this.state here
      return {
        error: props.error || state.error || false,
        errorEventId: props.errorEventId || state.errorEventId || undefined
      };
    }

    static getDerivedStateFromError(error: Error) {
      // React Error Boundary here allows us to set state flagging the error (and
      // later render a fallback UI).
      return { error };
    }

    componentDidCatch(error: Error, errorInfo: ErrorInfo) {
      const errorEventId = captureException(error, { errorInfo });

      // Store the event id at this point as we don't have access to it within
      // `getDerivedStateFromError`.
      this.setState({ error, errorEventId });
    }

    constructor(props: ApolloProps) {
      super(props);
      this.apolloClient = initApollo(props.apolloState);
      this.state = {
        error: undefined,
        errorEventId: undefined
      };
    }

    render() {
      return (
        <App
          error={this.state.error}
          pageProps={this.props.pageProps}
          Component={this.props.Component}
          router={this.props.router}
          apolloClient={this.apolloClient}
        />
      );
    }
  };
};
