const MILLION = 1000000;
const THOUSAND = 1000;

const MINUTES_IN_SECONDS = 60;
const HOURS_IN_SECONDS = 60 * MINUTES_IN_SECONDS;
const DAY_IN_SECONDS = 24 * HOURS_IN_SECONDS;

export function formatNumber(number?: number | null, showZero = false): string {
  if (number == null || number <= 0 || number === 0) return showZero ? '0' : '';

  const integerPart = Math.floor(number);
  const floatString = number.toString().substr(integerPart.toString().length);

  const numberString = integerPart.toString();
  let resultString = '';

  for (let i = numberString.length - 1; i >= 0; i--) {
    resultString = numberString[i] + resultString;

    if ((numberString.length - i) % 3 === 0 && i > 0) {
      resultString = ',' + resultString;
    }
  }

  resultString += floatString;
  return resultString;
}

export function formatNumberWithSuffix(number: number): string {
  if (number >= 10 * MILLION) {
    return Math.floor(number / MILLION).toString() + 'M';
  }

  if (number >= 100 * THOUSAND) {
    return Math.floor(number / THOUSAND).toString() + 'K';
  }

  return number.toString();
}

export function formatTime(seconds: number): string {
  const time = getTimeBreakdown(seconds);

  const msgParts = [];
  if (time.days) {
    msgParts.push(pluralise(time.days, 'day'));
  }
  if (time.hours) {
    msgParts.push(pluralise(time.hours, 'hour'));
  }
  if (time.minutes && !time.days) {
    msgParts.push(pluralise(time.minutes, 'minute'));
  }
  if (time.seconds && !time.days && !time.hours) {
    msgParts.push(pluralise(time.seconds, 'second'));
  }

  return msgParts.join(' ');
}

function pluralise(value: number, unit: string): string {
  let msg = value + ' ' + unit;
  if (value > 1) {
    msg += 's';
  }
  return msg;
}

interface TimeBreakdown {
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
}

function getTimeBreakdown(totalSeconds: number): TimeBreakdown {
  let days = 0,
    hours = 0,
    minutes = 0;
  while (totalSeconds >= DAY_IN_SECONDS) {
    totalSeconds -= DAY_IN_SECONDS;
    days++;
  }
  while (totalSeconds >= HOURS_IN_SECONDS) {
    totalSeconds -= HOURS_IN_SECONDS;
    hours++;
  }
  while (totalSeconds >= MINUTES_IN_SECONDS) {
    totalSeconds -= MINUTES_IN_SECONDS;
    minutes++;
  }

  return {
    days,
    hours,
    minutes,
    seconds: Math.ceil(totalSeconds)
  };
}

export function stringToNumber(numberOrString: string | number): number {
  if (typeof numberOrString === 'string') {
    return parseInt(numberOrString.replace(/,/g, ''), 10);
  }
  return numberOrString;
}
