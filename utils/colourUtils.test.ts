import { percentToHex, adjustAlphaOfShadow } from './colourUtils';

describe('percentToHex', () => {
  it('should return 00 when passed 0', function() {
    expect(percentToHex(0)).toBe('00');
  });

  it('should return 80 when passed 0.5', function() {
    expect(percentToHex(0.5)).toBe('80');
  });

  it('should return FF when passed 1', function() {
    expect(percentToHex(1)).toBe('FF');
  });
});

describe('adjustAlphaOfShadow', function() {
  it('should set the alpha to 0 when passed a percent of 0', function() {
    const input = 'rgba(255,255,255,1)';
    expect(adjustAlphaOfShadow(input, 0)).toBe('rgba(255,255,255,0)');
  });

  it('should leave the alpha as 0.5 when passed a percent of 1', function() {
    const input = 'rgba(255,255,255,0.5)';
    expect(adjustAlphaOfShadow(input, 1)).toBe('rgba(255,255,255,0.5)');
  });

  it('should leave the spaces as defined when passed a percent of 0.5 and an rgba function with spaces', function() {
    const input = 'rgba( 255, 255, 255, 1 )';
    expect(adjustAlphaOfShadow(input, 0.5)).toBe('rgba( 255, 255, 255, 0.5 )');
  });

  it('should correctly half the alpha when passed a percentage of 0.5', function() {
    const input = 'rgba(255,255,255,0.5)';
    expect(adjustAlphaOfShadow(input, 0.5)).toBe('rgba(255,255,255,0.25)');
  });

  it('should half all alpha channels in an input string that contains multiple rgba channels when passed a percentage of 0.5', function() {
    const input = 'rgba(255,255,255,0.5), rgba(0,0,0,0.5)';
    expect(adjustAlphaOfShadow(input, 0.5)).toBe(
      'rgba(255,255,255,0.25), rgba(0,0,0,0.25)'
    );
  });

  it.todo('should double the alpha of an input if passed a percentage of 2');
  it.todo('should throw an error if passed a percentage less than 1');
  it.todo(
    'should return an alpha of 1 if passed a percentage that would increase the alpha above 1'
  );
});
