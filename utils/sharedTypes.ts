import { FunctionComponent } from 'react';
import { PageContext } from '../pages/_app';

export type AllSkillNames =
  | 'overall'
  | 'attack'
  | 'defence'
  | 'strength'
  | 'hitpoints'
  | 'ranged'
  | 'prayer'
  | 'magic'
  | 'cooking'
  | 'woodcutting'
  | 'fletching'
  | 'fishing'
  | 'firemaking'
  | 'crafting'
  | 'smithing'
  | 'mining'
  | 'herblore'
  | 'agility'
  | 'thieving'
  | 'slayer'
  | 'farming'
  | 'runecrafting'
  | 'hunter'
  | 'construction';

export interface Item {
  icon: string;
  id: number;
  name: string;
  quantity: number;
  highalch: number;
  lowalch: number;
  tradable: boolean;
}

interface Price {
  rsbuddy?: number;
  runelite?: number;
  highalch?: number;
  lowalch?: number;
}

export interface ContainerItem {
  id: number;
  name: string;
  price?: Price;
  buy_limit?: number;
}

export interface ContainerItemQuery {
  item: ContainerItem;
  quantity?: number;
  index?: number;
}

export type MyAppPage = FunctionComponent<{
  username?: string;
  pageContext: PageContext;
  error?: Error;
}> & { title?: string };

export interface XpChangeEvent {
  skill: AllSkillNames;
  xpGained: number;
  xpTotal: number;
}

export interface XpChangeSubscription {
  xpChanges: Array<XpChangeEvent>;
  playerId: string;
}

export type XpDictionary = Partial<Record<AllSkillNames, number>>;
