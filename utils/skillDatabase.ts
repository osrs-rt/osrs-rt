interface AgilityCourse {
  name: string;
  requiredLevel: number;
  xpPerLap: number,
  approxXpPerHour: number
}

export const agility: Array<AgilityCourse> = [
  {
    name: 'Gnome Stronghold',
    requiredLevel: 1,
    xpPerLap: 86.5,
    approxXpPerHour: 8900 - (8900 - 7400) / 2
  },
  {
    name: 'Penguin Course',
    requiredLevel: 30,
    xpPerLap: 540,
    approxXpPerHour: 30000
  },
  {
    name: 'Barbarian Outpost',
    requiredLevel: 35,
    xpPerLap: 153.5,
    approxXpPerHour: 11000
  },
  {
    name: 'Ape Atoll',
    requiredLevel: 48,
    xpPerLap: 580,
    approxXpPerHour: 52000
  },
  {
    name: 'Wilderness Course',
    requiredLevel: 52,
    xpPerLap: 571,
    approxXpPerHour: 45600
  },
  {
    name: 'Werewolf Course',
    requiredLevel: 60,
    xpPerLap: 730,
    approxXpPerHour: 60000
  },
  {
    name: 'Rellekka Rooftops',
    requiredLevel: 80,
    xpPerLap: 780,
    approxXpPerHour: 50000
  },
  {
    name: 'Ardougne Rooftops',
    requiredLevel: 90,
    xpPerLap: 793,
    approxXpPerHour: 62300
  }
];
