import {
  formatNumber,
  formatTime,
  stringToNumber,
  formatNumberWithSuffix
} from './stringFormatters';

describe('formatNumber', () => {
  it("should display '' when passed in null", () => {
    const actual = formatNumber(null);

    expect(actual).toBe('');
  });

  it("should display '' when passed in 0 and showZero parameter is false", () => {
    const actual = formatNumber(0);

    expect(actual).toBe('');
  });

  it("should display '0' when passed in 0 and showZero parameter is true", () => {
    const actual = formatNumber(0, true);

    expect(actual).toBe('0');
  });

  it("should display '' when passed in -1", () => {
    const actual = formatNumber(-1);

    expect(actual).toBe('');
  });

  it("should display '1' when passed 1 as argument", () => {
    const actual = formatNumber(1);

    expect(actual).toBe('1');
  });

  it("should display '12' when passed 12 as argument", () => {
    const actual = formatNumber(12);

    expect(actual).toBe('12');
  });

  it("should display '123' when passed 123 as argument", () => {
    const actual = formatNumber(123);

    expect(actual).toBe('123');
  });

  it("should display '12.34567' when passed 12.34567 as an argument", () => {
    const actual = formatNumber(12.34567);

    expect(actual).toBe('12.34567');
  });

  it("should display '1,234.567' when passed 1234.567 as an argument", () => {
    const actual = formatNumber(1234.567);

    expect(actual).toBe('1,234.567');
  });

  it("should display '1,234' when passed 1234 as argument", () => {
    const actual = formatNumber(1234);

    expect(actual).toBe('1,234');
  });

  it("should display '12,345' when passed 12345 as argument", () => {
    const actual = formatNumber(12345);

    expect(actual).toBe('12,345');
  });

  it("should display '123,456' when passed 123456 as argument", () => {
    const actual = formatNumber(123456);

    expect(actual).toBe('123,456');
  });

  it("should display '1,234,567' when passed 1234567 as argument", () => {
    const actual = formatNumber(1234567);

    expect(actual).toBe('1,234,567');
  });

  it("should display '12,345,678' when passed 12345678 as argument", () => {
    const actual = formatNumber(12345678);

    expect(actual).toBe('12,345,678');
  });

  it("should display '123,456,789' when passed 123456789 as argument", () => {
    const actual = formatNumber(123456789);

    expect(actual).toBe('123,456,789');
  });

  it("should display '1,234,567,890' when passed 1234567890 as argument", () => {
    const actual = formatNumber(1234567890);

    expect(actual).toBe('1,234,567,890');
  });
});

describe('formatTime', () => {
  it('should return `1 second` when given seconds value of 1', () => {
    const string = formatTime(1);

    expect(string).toBe('1 second');
  });

  it('should return `1 minute` when given seconds value of 60', () => {
    const string = formatTime(60);

    expect(string).toBe('1 minute');
  });

  it('should return `1 hour` when given seconds value of 3600', () => {
    const string = formatTime(3600);

    expect(string).toBe('1 hour');
  });

  it('should return `1 day` when given seconds value of 86400', () => {
    const string = formatTime(86400);

    expect(string).toBe('1 day');
  });

  it('should return 2 seconds when given seconds value of 2', function() {
    const string = formatTime(2);

    expect(string).toBe('2 seconds');
  });
});

describe('stringToNumber', () => {
  it('should return 0 when passed 0', () => {
    const actual = stringToNumber(0);
    expect(actual).toBe(0);
  });

  it('should return 1 when passed 1', () => {
    const actual = stringToNumber(1);
    expect(actual).toBe(1);
  });

  it("should return 0 when passed '0'", () => {
    const actual = stringToNumber('0');
    expect(actual).toBe(0);
  });

  it("should return 1 when passed '1'", () => {
    const actual = stringToNumber('1');
    expect(actual).toBe(1);
  });

  it("should return 1000 when passed '1,000'", () => {
    const actual = stringToNumber('1,000');
    expect(actual).toBe(1000);
  });

  it("should return 10000000 when passed '1000000'", () => {
    const actual = stringToNumber('1,000,000');
    expect(actual).toBe(1000000);
  });
});

describe('formatNumberWithSuffix', () => {
  it('should return number as if less than 100000', () => {
    const actual = formatNumberWithSuffix(999);
    expect(actual).toBe('999');
  });

  it('should return 1000 when given 1000', () => {
    const actual = formatNumberWithSuffix(1000);
    expect(actual).toBe('1000');
  });

  it('should return 100K when given 100000', () => {
    const actual = formatNumberWithSuffix(100000);
    expect(actual).toBe('100K');
  });

  it('should return 999K when given 999999', () => {
    const actual = formatNumberWithSuffix(999999);
    expect(actual).toBe('999K');
  });

  it('should return 1000K when given 1 million', () => {
    const actual = formatNumberWithSuffix(1000000);
    expect(actual).toBe('1000K');
  });

  it('should should return 10M when given 10 million', () => {
    const actual = formatNumberWithSuffix(10000000);
    expect(actual).toBe('10M');
  });
});
