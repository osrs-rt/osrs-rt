export function percentToHex(valNum: number): string {
  const decimalValue = Math.round(valNum * 255);

  return decimalValue
    .toString(16)
    .toUpperCase()
    .padStart(2, '0');
}

export /**
 *
 * @param shadowString - Contains the string used to set a box-shadow
 * @param percentToChangeBy - THe amount to change the alpha by where 1 = 100% of initial value (unchanged) and 0 is 0% (reduced to an alpha of 0)
 */
function adjustAlphaOfShadow(
  shadowString: string,
  percentToChangeBy: number
): string {
  const alphaRegex = /(rgba\((?:\s*[\d]{1,3}\s*,\s*){3}\s*)([\d]\.?[\d]*)(\s*\))/g;
  return shadowString.replace(
    alphaRegex,
    (match, rgb, alpha, postAlpha) => {
      if (Number.isNaN(alpha)) {
        return match;
      }

      const newAlpha = alpha * percentToChangeBy;
      return rgb + newAlpha + postAlpha;
    }
  );
}
