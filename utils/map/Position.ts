import { LatLngTuple } from 'leaflet';

export default class Position {
  x: number;
  y: number;
  z: number;

  constructor(xOrPositionObject: number | { x: number, y: number, z: number }, y?:number, z?: number) {
    if (typeof xOrPositionObject === 'object') {
      this.x = xOrPositionObject.x;
      this.y = xOrPositionObject.y;
      this.z = xOrPositionObject.z;
    } else {
      this.x = Math.round(xOrPositionObject);
      this.y = Math.round(y as number);
      this.z = z as number;
    }
  }

  toLatLng(): LatLngTuple {
    return [this.y, this.x];
  }

  equals(position: Position) {
    return (
      this.x === position.x && this.y === position.y && this.z === position.z
    );
  }

  toString() {
    return `(${this.x}, ${this.y}, ${this.z})`;
  }
}
