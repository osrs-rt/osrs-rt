import {
  ApolloLink, FetchResult, Observable, Operation
} from 'apollo-link';
import socketIO  from 'socket.io-client';
import $$observable from 'symbol-observable';

export type SocketIOLinkOptions = {
  uri: string
};

interface DataCallbackMap  {
  [key: string]: (error: Error[], result: any) => void;
}

interface SocketIOOperation extends Operation {
  id: string | number;
}

let nextOperationId = 0;

export default class SocketIOLink extends ApolloLink {
  _options: SocketIOLinkOptions;
  socket: SocketIOClient.Socket;
  _dataCallbacks: DataCallbackMap = {};

  constructor(options: SocketIOLinkOptions) {
    super();
    console.debug('Constructing socket');
    this._options = options;

    this.socket = socketIO(options.uri);

    this.socket.on('data', this.onData);
  }

  onData = (msg: any, operationId: number | string) => {
    console.debug(msg, operationId, this._dataCallbacks);
    const callback = this._dataCallbacks[operationId];
    if (!callback) return;

    const { errors, ...payload } = msg;
    callback(errors, payload);
  };

  // request(operation: Operation, forward?: NextLink): Observable<FetchResult> | null;

  // forEach(fn: (value: T) => void): Promise<void>;
  // map<R>(fn: (value: T) => R): Observable<R>;
  // filter(fn: (value: T) => boolean): Observable<T>;
  // reduce<R = T>(fn: (previousValue: R | T, currentValue: T) => R | T, initialValue?: R | T): Observable<R | T>;
  // flatMap<R>(fn: (value: T) => ZenObservable.ObservableLike<R>): Observable<R>;
  // from<R>(observable: Observable<R> | ZenObservable.ObservableLike<R> | ArrayLike<R>): Observable<R>;
  // of<R>(...args: Array<R>): Observable<R>;

  request = (operation: SocketIOOperation): Observable<FetchResult> => {
    // @ts-ignore
    return ({
      [$$observable]() {
        return this;
      },
      // subscribe(observerOrNext: ((value: T) => void) | ZenObservable.Observer<T>, error?: (error: any) => void, complete?: () => void): ZenObservable.Subscription;
      subscribe: (observerOrNext: ((value: any) => void | ZenObservable.Observer<any>), onError: (error: any) => void, onComplete: () => void): ZenObservable.Subscription => {
        const observer = this._getObserver(observerOrNext, onError, onComplete);

        operation.id = ++nextOperationId;
        console.debug('Adding subscription with id', operation.id);

        this._dataCallbacks[operation.id] = (error: Error[], result: any) => {
          if (error === null && result === null) {
            if (observer.complete) {
              observer.complete();
            }
          } else if (error) {
            if (observer.error) {
              observer.error(error[0]);
            }
          } else {
            if (observer.next) {
              observer.next(result);
            }
          }
        };
        this.socket.emit('start', operation);
        // TODO UNSUBSCRIBE. SHOULD SEND STOP. SERVER SUPPORT NEEDS ADDING

        return {
          unsubscribe() {},
          closed: false
        }
      },
    });
  };

  _getObserver<T> (
    observerOrNext: (value: T) => void | ZenObservable.Observer<T>,
    error: (errors: Error) => void,
    complete: () => void
  ): ZenObservable.Observer<T> {
    if (typeof observerOrNext === 'function') {
      return {
        next: (v) => observerOrNext(v),
        error: (e) => error && error(e),
        complete: () => complete && complete()
      };
    }

    return observerOrNext;
  }
}
