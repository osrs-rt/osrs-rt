import { ApolloClient, ApolloClientOptions, HttpLink, InMemoryCache, split } from 'apollo-boost';
import fetch from 'cross-fetch';
// import SocketIOLink from './SocketIOLink';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';

let apolloClient: ApolloClient<any> | null = null;

// Polyfill fetch() on the server (used by apollo-client)
if (!process.browser) {
  // @ts-ignore
  global.fetch = fetch;
}

function create(initialState: any) {
  const httpUri = process.env.graphql_url || '';
  const websocketUri = process.env.graphql_subscriptions_url || '';

  console.log(`Apollo HTTP URL is ${httpUri}`);

  // Check out https://github.com/zeit/next.js/pull/4611 if you want to use the AWSAppSyncClient
  const apolloClientOptions: ApolloClientOptions<any> = {
    // connectToDevTools: process.browser,
    ssrMode: !process.browser, // Disables forceFetch on the server (so queries are only run once)
    cache: new InMemoryCache().restore(initialState || {})
  };

  const httpLink = new HttpLink({
    uri: httpUri,
    credentials: 'same-origin' // Additional fetch() options like `credentials` or `headers`
  });

  if (process.browser) {
    console.log(`Apollo WS URL is ${websocketUri}`);
    const webSocketLink = new WebSocketLink({
      uri: websocketUri,
      options: { reconnect: true }
    });
    // using the ability to split links, you can send data to each link
    // depending on what kind of operation is being sent
    apolloClientOptions.link = split(
      // split based on operation type
      ({ query }) => {
        const definition = getMainDefinition(query);
        return (
          definition.kind === 'OperationDefinition' &&
          definition.operation === 'subscription'
        );
      },
      webSocketLink,
      httpLink
    );
  } else {
    apolloClientOptions.link = httpLink;
  }

  return new ApolloClient(apolloClientOptions);
}

export default function initApollo(initialState: any) {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (!process.browser) {
    return create(initialState);
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = create(initialState);
  }

  return apolloClient;
}
