import {
  createMuiTheme,
  PaletteColorOptions,
  ThemeOptions
} from '@material-ui/core/styles';
import { red, green, purple, grey } from '@material-ui/core/colors';

let mainColour: PaletteColorOptions = green;
if (process.env.theme_colour) {
  mainColour = {
    main: process.env.theme_colour
  };
}

// Create a theme instance.
const themeOptions: ThemeOptions = {
  palette: {
    primary: mainColour,
    secondary: purple,
    error: {
      main: red.A400
    },
    background: {
      default: grey['200']
    }
  },
  overrides: {
    MuiCardContent: {
      root: {
        '&:last-child': {
          paddingBottom: 16
        }
      }
    }
  }
};
const theme = createMuiTheme(themeOptions);

const makeDarkTheme = (theme: Theme): Theme => ({
  ...theme,
  palette: {
    ...theme.palette,
    type: 'dark',
    text: {
      ...theme.palette.text,
      primary: 'white'
    }
  }
});
export const darkTheme = makeDarkTheme(theme);

if (process.env.NODE_ENV === 'development') {
  // Add the theme to the window in development so we can always see what it looks like
  // @ts-ignore
  global.theme = theme;
}

export type Theme = typeof theme;

export default theme;
