const fetch = require('cross-fetch');
const cheerio = require('cheerio');
const _ = require('lodash');
const fs = require('fs-extra');
const path = require('path');

// TODO: HANDLE MINIQUESTS

const USE_CACHE = true;
const DEBUG_QUEST_FILTER = '';

const ALL_SKILLS = [
  'overall',
  'attack',
  'defence',
  'strength',
  'hitpoints',
  'ranged',
  'prayer',
  'magic',
  'cooking',
  'woodcutting',
  'fletching',
  'fishing',
  'firemaking',
  'crafting',
  'smithing',
  'mining',
  'herblore',
  'agility',
  'thieving',
  'slayer',
  'farming',
  'runecrafting',
  'hunter',
  'construction'
];

const skillRegex = new RegExp(`(\\d+)\\s+(${ALL_SKILLS.join('|')})`, 'i');
// console.log(skillRegex);

const WIKI_BASE_URL = 'https://oldschool.runescape.wiki';
const QUEST_LIST_URL = 'https://oldschool.runescape.wiki/w/Quests/List';

function questNameToId(questName) {
  return _.snakeCase(questName).toUpperCase();
}

async function getQuestListPage() {
  let html;
  const filename = `__cache/_quest_list.html`;
  if (USE_CACHE && fs.pathExistsSync(filename)) {
    html = fs.readFileSync(filename);
  } else {
    const response = await fetch(QUEST_LIST_URL);
    html = await response.text();

    if (USE_CACHE) {
      fs.ensureDirSync(path.dirname(filename));
      fs.writeFileSync(filename, html, 'utf8');
    }
  }

  return cheerio.load(html);
}

async function getQuestPage(quest) {
  let html;
  const filename = `__cache/${quest.id}.html`;
  if (USE_CACHE && fs.pathExistsSync(filename)) {
    html = fs.readFileSync(filename);
  } else {
    const response = await fetch(quest.url);
    html = await response.text();

    if (USE_CACHE) {
      fs.ensureDirSync(path.dirname(filename));
      fs.writeFileSync(filename, html, 'utf8');
    }
  }

  return cheerio.load(html);
}

async function getCoreQuestInfo() {
  const quests = {};

  const $ = await getQuestListPage();
  $('.wikitable.lighttable tbody > tr').each((index, table) => {
    const cells = $(table).find('td');

    const questCell = $(cells[0])
      .text()
      .trim();
    let questNumber = parseInt(questCell, 10);
    let subQuest = false;
    const decimalIndex = questCell.indexOf('.');
    if (decimalIndex > -1) {
      const decimalPart = questCell.substr(decimalIndex + 1);
      const numeric = parseInt(decimalPart, 10);
      questNumber += numeric / 100;
      subQuest = true;
    }

    const quest = {
      number: questNumber,
      name: $(cells[1])
        .text()
        .trim(),
      difficulty: $(cells[2])
        .text()
        .trim(),
      length: $(cells[3])
        .text()
        .trim(),
      questPoints: parseInt(
        $(cells[4])
          .text()
          .trim(),
        10
      ),
      url:
        WIKI_BASE_URL +
        $(cells[1])
          .find('a')
          .attr('href')
    };
    if (subQuest) {
      quest.subQuest = true;
    }

    const id = questNameToId(quest.name);

    if (!id) {
      return;
    }

    quest.id = id;
    quests[id] = quest;
  });

  return quests;
}

async function getQuestRequirements(quest, listOfAllQuests) {
  console.log(`Getting quest requirements for ${quest.name}`);
  const requirements = {
    quests: [],
    skills: {}
    // questPoints: 0 // TODO: DO IT
    // others: []
  };

  const $ = await getQuestPage(quest);

  const headerCell = $('th')
    .filter((index, element) => {
      return (
        $(element)
          .text()
          .trim() === 'Requirements'
      );
    })
    .first();
  const requirementsCell = $(headerCell).next('td');

  const recommended = requirementsCell.find('*').filter((index, element) => {
    return (
      $(element)
        .text()
        .trim() === 'Recommended:'
    );
  });

  const essentialRequirements = (recommended.length
    ? (recommended.closest('ul').length
        ? recommended.closest('ul')
        : recommended
      ).prevAll('ul')
    : requirementsCell.find('ul')
  ).children('li');

  const skills = $(essentialRequirements).find('img');
  $(skills).each((index, element) => {
    const text = $(element)
      .parent()
      .parent()
      .text();

    const match = skillRegex.exec(text);
    if (match) {
      requirements.skills[match[2].toLowerCase()] = parseInt(match[1], 10);
    }
  });

  // Find the first quest and then find all other quests at the same level as it
  const firstQuest = essentialRequirements
    .find('a')
    .filter((index, element) => {
      const linkText = $(element)
        .text()
        .trim();

      return listOfAllQuests.some(q => q.name === linkText);
    })
    .first();

  const allLinks = firstQuest
    .parent()
    .parent()
    .children()
    .children('a');
  allLinks.each((index, element) => {
    const linkText = $(element)
      .text()
      .trim();

    const quest = listOfAllQuests.find(q => q.name === linkText);
    if (quest != null) {
      requirements.quests.push(quest.id);
    }
  });

  // TODO: QUEST POINT REQUIREMENTS

  // TODO: SHOULD WE INCLUDE OTHER REQUIREMENTS? e.g. NON-SKILL AND QUEST BASED REQS ('The ability to defeat a powerful level 172 Giant Roc')
  console.log(`Done getting requirements for ${quest.name}:`, requirements);
  return requirements;
}

getCoreQuestInfo()
  .then(async quests => {
    const allPromises = Object.entries(quests)
      .filter(([questId]) => {
        return !DEBUG_QUEST_FILTER || questId === DEBUG_QUEST_FILTER;
      })
      .map(async ([questId, quest]) => {
        try {
          quests[questId].requirements = await getQuestRequirements(
            quest,
            Object.values(quests)
          );
        } catch (e) {
          console.error(
            `Error whilst gathering requirements for quest '${quest.name}': ${e.message}`
          );
        }
      });

    await Promise.all(allPromises);
    return quests;
  })
  .then(async quests => {
    // Object.values(quests).forEach(q => { console.log(q.number); });
    await fs.outputJson(
      path.join(__dirname, '../databases/quests.json'),
      quests,
      { spaces: 2 }
    );
    console.log('All done');
  });
