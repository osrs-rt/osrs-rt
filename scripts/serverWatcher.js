const fs = require('fs-extra');
const path = require('path');
const nodemon = require('nodemon');

nodemon({
  script: 'server/index.ts',
  ext: 'js json graphql graphqls ts tsx',
  ignore: [
    "*.test.js"
  ],
  watch: "server",
  execMap: {
    "js": "babel-node --extensions \".ts,.tsx,.js\"",
    "ts": "babel-node --extensions \".ts,.tsx,.js\"",
    "tsx": "babel-node --extensions \".ts,.tsx,.js\""
  }
});

nodemon.on('start', function () {
  console.log('App has started');
}).on('quit', function () {
  console.log('App has quit');
  process.exit();
}).on('restart', function (files) {
  console.log('App restarted due to changes in: ', files);

  if (files.some(s => /\.graphqls?$/.test(s))) {
    const pathToDelete = path.resolve(__dirname, '../node_modules/.cache/@babel');
    console.log(`GraphQL schema changed, removing babel cache located at ${pathToDelete}`);
    fs.removeSync(pathToDelete);
  }
});
