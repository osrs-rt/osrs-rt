const url = 'https://oldschool.runescape.wiki/w/Mining';
const cheerio = require('cheerio');
const fs = require('fs-extra');
const path = require('path');
const { fetchWithCache } = require('./common');
const { parseTable } = require('./common');

async function run() {
  const rawHtml = await fetchWithCache(url);

  const $ = cheerio.load(rawHtml);

  const table = $('.wikitable.sortable')[0];

  const items = parseTable(
    {
      item: 2,
      requiredLevel: {
        cell: 0,
        type: 'int'
      },
      xp: {
        cell: 3,
        type: 'float'
      },
      members: {
        cell: 5,
        type: 'boolean'
      }
    },
    table
  );

  return items;
}

// TODO: API VS SCRIPT (include in other scripts vs standalone)
run().then((items) => {
  fs.writeJSONSync(path.join(__dirname, '../../databases/mining.json'), items, { spaces: 2 });

  console.log('All done');
});
