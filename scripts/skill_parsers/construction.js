const url = 'https://oldschool.runescape.wiki/w/Construction';
const skill = 'construction';
const cheerio = require('cheerio');
const fs = require('fs-extra');
const path = require('path');
const { parseTable, fetchWithCache } = require('./common');
const _ = require('lodash');


// TODO: INCLUDE URL SO ITEMS CAN BE LINKED
async function run() {
  const rawHtml = await fetchWithCache(url);

  const $ = cheerio.load(rawHtml);

  const table = $('.wikitable')[2];

  const items = parseTable(
    {
      item: 1,
      requiredLevel: {
        cell: 3,
        type: 'int'
      },
      xp: {
        cell: 4,
        type: 'float'
      }
    },
    table
  );

  return _.sortBy(items, x => x.requiredLevel);
}

// TODO: API VS SCRIPT (include in other scripts vs standalone)
run().then(items => {
  fs.writeJSONSync(
    path.join(__dirname, `../../databases/${skill}.json`),
    items,
    { spaces: 2 }
  );

  console.log('All done');
});
