const cheerio = require('cheerio');
const path = require('path');
const fs = require('fs-extra');
const fetch = require('cross-fetch');

/**
 * @typedef {Object} ColumnDefinition
 * @property {number} cell    The index of the cell that data should be retrieved for
 * @property {'text'|'float'|'number'|'boolean'|'custom'}
 * @property {Function} fn    A custom function to run against cells in this column. The first argument is the cell
 */

/**
 * Parse a table retrieved from the OSRS wiki into an object
 * @param {Object<string, (number|ColumnDefinition)>} definitions    A mapping of table columns to data fields
 * @param {HTMLElement|string} table
 * @return {Array}
 */
function wikiTableParser(definitions, table) {
  const $ = cheerio.load(table);
  const data = [];
  $(table)
    .find('tbody tr')
    .each((i, row) => {
      const cells = $(row).find('td');
      if (!cells.length) return;

      const item = {};
      Object.entries(definitions).forEach(([key, definition]) => {
        const cellIndex = typeof definition === 'number' ? definition : definition.cell;
        const type = definition.type || 'text';

        const $cell = $(cells[cellIndex]);
        const rawData = $cell.text().trim();

        switch (type) {
          default:
          case 'text': item[key] = rawData; break;
          case 'float': item[key] = parseFloat(rawData); break;
          case 'int': item[key] = parseInt(rawData); break;
          case 'boolean': item[key] = $cell.find('a').attr('title') === 'Members'; break;
          case 'custom': item[key] = definition.fn($cell)
        }

      });
      data.push(item);
    });

  return data;
}

/**
 * Fetch a html document from disk if it exists and is not older than a day. otherwise retrieve from the given URL
 * @param {string} url
 * @returns {Promise<string>}
 */
async function fetchWithCache(url) {
  // https://oldschool.runescape.wiki/w/Construction#Spellbook%20altar

  let pathname = new URL(url).pathname;
  const currentExt = path.extname(pathname);
  const filename = pathname.replace(new RegExp(currentExt + '$'), '.html');
  const filePath = path.join(__dirname, `__cache/`, filename);

  try {
    // Check if the cache file exists
    const stats = fs.statSync(filePath);

    // Check if it was modified less than 24 hours ago
    const previousDay = new Date().setDate(new Date().getDate() - 1);
    if (stats.mtime >= previousDay) {
      console.debug('Retrieving file from cache');
      // Read the file and return it
      return fs.readFile(filePath, 'utf8');
    }
  } catch (error) {
    // do nothing since the file doesn't exist
  }

  console.debug('File not found in cache. Downloading fresh copy.');
  // Download the file
  const response = await fetch(url);
  const text = await response.text();

  fs.ensureDirSync(path.dirname(filePath));
  await fs.writeFileSync(filePath, text, 'utf8');
  return text;
}

module.exports = {
  parseTable: wikiTableParser,
  fetchWithCache
};
