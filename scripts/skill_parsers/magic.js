const url = 'https://oldschool.runescape.wiki/w/Standard_spells';
const cheerio = require('cheerio');
const fs = require('fs-extra');
const path = require('path');
const { fetchWithCache, parseTable } = require('./common');

async function run() {
  const rawHtml = await fetchWithCache(url);

  const $ = cheerio.load(rawHtml);

  const table = $('.wikitable.sortable')[0];

  // TODO: OTHER SPELLBOOKS

  const items = parseTable(
    {
      spell: 2,
      requiredLevel: {
        cell: 3,
        type: 'int'
      },
      xp: {
        cell: 5,
        type: 'float'
      },
      fixedXp: {
        cell: 5,
        type: 'custom',
        // XP can be greater for combat spells indicated by the '+' symbol (e.g. 11.5+)
        fn($cell) {
          return !/\+$/.test($cell.text().trim());
        }
      },
      runes: {
        cell: 4,
        type: 'custom',
        fn($cell) {
          const links = $cell.find('a');
          if (!links.length) return [];
          const runes = links
            .map((_, link) => {
              return {
                rune: $(link).attr('title'),
                quantity: parseInt(
                  $(link)
                    .prev('sup')
                    .text()
                )
              };
            })
            .get();
          return runes;
        }
      }
    },
    table
  );

  return items;
}

// TODO: API VS SCRIPT (include in other scripts vs standalone)
run().then(items => {
  fs.writeJSONSync(path.join(__dirname, '../../databases/magic.json'), items, {
    spaces: 2
  });

  console.log('All done');
});
