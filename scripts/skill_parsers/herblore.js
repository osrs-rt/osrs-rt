const url = 'https://oldschool.runescape.wiki/w/Herblore';
const skill = 'herblore';
const cheerio = require('cheerio');
const fs = require('fs-extra');
const path = require('path');
const { parseTable, fetchWithCache } = require('./common');
const _ = require('lodash');

// TODO: INCLUDE URL SO ITEMS CAN BE LINKED
async function run() {
  const rawHtml = await fetchWithCache(url);

  const $ = cheerio.load(rawHtml);

  const herbTable = $('.wikitable')[0];
  const potionTable = $('.wikitable')[1];
  const tarTable = $('.wikitable')[2];
  const mixTable = $('.wikitable')[3];

  // Cleaning herbs
  const herbs = parseTable(
    {
      item: 2,
      requiredLevel: {
        cell: 0,
        type: 'int'
      },
      xp: {
        cell: 3,
        type: 'float'
      }
    },
    herbTable
  );
  herbs.forEach(h => (h.type = 'Grimy herb'));

  const potions = parseTable(
    {
      item: 2,
      requiredLevel: {
        cell: 0,
        type: 'int'
      },
      xp: {
        cell: 3,
        type: 'float'
      }
    },
    potionTable
  );
  potions.forEach(p => (p.type = 'Potion'));

  const tars = parseTable(
    {
      item: 2,
      requiredLevel: {
        cell: 0,
        type: 'int'
      },
      xp: {
        cell: 3,
        type: 'float'
      }
    },
    tarTable
  );
  tars.forEach(p => (p.type = 'Swamp tar'));

  const mixes = parseTable(
    {
      item: 2,
      requiredLevel: {
        cell: 0,
        type: 'int'
      },
      xp: {
        cell: 3,
        type: 'float'
      }
    },
    mixTable
  );
  mixes.forEach(p => (p.type = 'Barbarian mix'));

  return _.sortBy([...herbs, ...potions, ...mixes, ...tars], x => x.requiredLevel);
}

// TODO: API VS SCRIPT (include in other scripts vs standalone)
run().then(items => {
  fs.writeJSONSync(
    path.join(__dirname, `../../databases/${skill}.json`),
    items,
    { spaces: 2 }
  );

  console.log('All done');
});
