const fs = require('fs');
const path = require('path');
const https = require('https');

const filePath = path.resolve(__dirname, '../databases/items.json');
const outstream = fs.createWriteStream(filePath);

const url =
  'https://raw.githubusercontent.com/osrsbox/osrsbox-db/master/docs/items-complete.json';

https.get(url, res => {
  res.pipe(outstream);
});
