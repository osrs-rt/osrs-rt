This project makes use of the following technologies:

#### Frontend
  - [Next.js](https://nextjs.org)
  - [Material UI](https://www.material-ui.com)
  - [ZEIT Now](http://now.sh)

#### Backend
  - [express.js](https://expressjs.com)
  - [GraphQL](https://graphql.org)
  - [Redis](https://redis.io)
  - [Docker](https://www.docker.com)
  - [Microsoft Azure](https://azure.microsoft.com)
  
  
  
## Development
The only external dependencies required to run the development environment are:
  - Node.js >= 10 including [yarn](https://yarnpkg.com)
  - An accessible Redis server
  
Configuration of which redis server can be accessed and other development time settings is done by copying the _.env.schema_ file to a _.env_ file in the same location.
Any updates to this file require any running development server to be restarted.

To get started:
```shell script
yarn install
yarn dev
```

The frontend is now available on http://localhost:3000 whilst the GraphQL backend is available on http://localhost:3001.



### Suggested tools
Some tools that aid in development would include any GraphQL query runner such as [Insomnia](https://insomnia.rest) would aid in server side development.
