import React from 'react';
import { AllSkillNames } from '../utils/sharedTypes';
import { makeStyles } from '@material-ui/styles';
import { Theme } from '../utils/muiTheme';

interface PropTypes {
  skill: AllSkillNames;
}

const useStyles = makeStyles((theme: Theme) => {
  return {
    root: {
      width: 14,
      height: 14,
      "&:first-child:not(:only-child)": {
        marginRight: '0.5em'
      }
    }
  }
});

const SkillIcon = ({ skill }: PropTypes) => {
  const classes = useStyles();
  const url = `/static/skill_icons/${skill.toLowerCase()}.png`;

  return <img className={classes.root} height={14} src={url} alt={skill + ' icon'} />;
};

export default SkillIcon;
