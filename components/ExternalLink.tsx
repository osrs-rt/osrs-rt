import React, { useState, FunctionComponent } from 'react';
import Link, { LinkProps } from '@material-ui/core/Link';
import makeStyles from '@material-ui/styles/makeStyles';
import LaunchIcon from '@material-ui/icons/Launch';
import { Theme } from '../utils/muiTheme';

const useStyles = makeStyles<Theme>(theme => ({
  icon: {
    position: 'absolute',
    left: '100%',
    marginLeft: theme.spacing(1)
  },
  link: {
    position: 'relative'
  }
}));

const ExternalLink: FunctionComponent<LinkProps> = ({ children, ...linkProps }) => {
  const classes = useStyles();
  const [hasMouseOver, setHasMouseOver] = useState(false);

  return (
    <Link
      className={classes.link}
      {...linkProps}
      onMouseEnter={() => {
        setHasMouseOver(true);
      }}
      onMouseLeave={() => {
        setHasMouseOver(false);
      }}
    >
      {children}
      {hasMouseOver && <LaunchIcon fontSize="small" className={classes.icon} />}
    </Link>
  );
};

export default ExternalLink;
