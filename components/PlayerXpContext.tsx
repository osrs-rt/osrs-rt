import { createContext, FunctionComponent, useContext, useEffect } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { XpChangeSubscription, XpDictionary } from '../utils/sharedTypes';
import getPlayerXp from '../queries/GetPlayerXp.graphql';
import subscribeToXpChanges from '../queries/SubscribeToXpChanged.graphql';
import { UpdateQueryFn } from 'apollo-client/core/watchQueryOptions';

interface GetPlayerXpResponse {
  player: {
    xp: XpDictionary;
  };
}

const context = createContext<XpDictionary>({});

export function usePlayerXp() {
  return useContext(context);
}

export const XpConsumer = context.Consumer;

export interface XpProviderProps {
  username?: string;
}

const xpUpdateToPlayerXpResponseMapper: UpdateQueryFn<
  GetPlayerXpResponse,
  { playerId: string },
  XpChangedSubscriptionEvent
> = (previousQueryResult, { subscriptionData: { data } }) => {
  console.debug('Received xp change event:', data);

  if (!data || !data.xpChanged) return previousQueryResult;

  // TODO: SHOULD PROBABLY GO THROUGH DATA ARRAY ENSURING WE ONLY HAVE (AT MOST) ONE OF EACH SKILL. NEEDS TO USE MAX

  return data.xpChanged.xpChanges.reduce((current, { skill, xpTotal }) => {
    return {
      ...current,
      player: {
        ...current.player,
        xp: {
          ...current.player.xp,
          [skill]: xpTotal
        }
      }
    };
  }, previousQueryResult);
};

export const XpProvider: FunctionComponent<XpProviderProps> = ({
  username,
  children
}) => {
  if (!username) return <>{children}</>;
  return <XpFetchProvider username={username} children={children} />;
};

const XpFetchProvider: FunctionComponent<Required<XpProviderProps>> = ({
  username,
  children
}) => {
  const variables = {
    playerId: username
  };

  const { data, loading, subscribeToMore, called } = useQuery<
    GetPlayerXpResponse
  >(getPlayerXp, {
    variables
  });

  useEffect(() => {
    if (loading || !called) return;

    console.debug('Subscribing to changes to xp');
    const unsubscribe = subscribeToMore({
      document: subscribeToXpChanges,
      variables,
      updateQuery: xpUpdateToPlayerXpResponseMapper
    });

    return () => {
      console.debug('Unsubscribing from changes to xp');
      unsubscribe();
    };
  }, [called, loading]);

  if (loading) return <>{children}</>;

  // console.log(query);

  return (
    <context.Provider value={data?.player.xp ?? {}}>
      {children}
    </context.Provider>
  );
};

export interface XpChangedSubscriptionEvent {
  xpChanged: XpChangeSubscription;
}
