import React, { Component } from 'react';
import query from '../queries/GetInventory.graphql';
import subscription from '../queries/SubscribeToInventoryChanged.graphql';
import { Query } from 'react-apollo';
import InventoryViewer, { InventoryItem } from './InventoryViewer';
import { itemDtoToModel } from '../sharedCode/itemUtils';
import { ContainerItemQuery } from '../utils/sharedTypes';

interface InventoryViewerContainerProps {
  username: string;
  className?: string;
}

class InventoryViewerContainer extends Component<InventoryViewerContainerProps> {
  _ensureInventoryIsFull = (args:Array<InventoryItem> = []) => {
    const inventory = new Array(28);
    inventory.fill(null, 0, 28);
    let index = 0;
    args.forEach(item => {
      const itemIndex = item.index || index++;
      inventory[itemIndex] = item;
    });

    return inventory;
  };

  render() {
    const { username, ...containerProps } = this.props;

    if (!username) return null;

    return (
      <Query<{ playerInventory: Array<ContainerItemQuery> }> query={query} variables={{ playerId: username }}>
        {({ data: originalData, subscribeToMore }) => {
          if (!originalData) {
            return <span>No data</span>;
          }
          const inventory = this._ensureInventoryIsFull(
            (originalData.playerInventory || []).map(itemDtoToModel)
          );

          const subscribeToInventoryChanges = () => {
            console.log('Subscribing to inventory changes');

            const unsubscribe = subscribeToMore({
              variables: {
                playerId: username
              },
              document: subscription,
              updateQuery(prev, { subscriptionData }: { subscriptionData: any }) {
                console.log('New inventory data received', subscriptionData);
                if (
                  !subscriptionData.data ||
                  !subscriptionData.data.inventoryChanged
                )
                  return prev;
                return {
                  playerInventory:
                    subscriptionData.data.inventoryChanged.newItems
                };
              }
            });

            return () => {
              console.log('Unsubscribing from from inventory changes');
              unsubscribe();
            }
          };

          return (
            <InventoryViewer
              inventory={inventory}
              subscribeToInventoryChanges={subscribeToInventoryChanges}
              {...containerProps}
            />
          );
        }}
      </Query>
    );
  }
}

export default InventoryViewerContainer;
