import React, { FunctionComponent, useEffect } from 'react';
import ItemImage from './ItemImage';
import Number from './Number';
import amber from '@material-ui/core/colors/amber';
import grey from '@material-ui/core/colors/grey';
import makeStyles from '@material-ui/core/styles/makeStyles';
import RunescapeTypography from './RunescapeTypography';
import LinearProgress from '@material-ui/core/LinearProgress';
import green from '@material-ui/core/colors/green';
import commonColours from '@material-ui/core/colors/common';
import sumBy from 'lodash/sumBy';
import { Theme } from '../utils/muiTheme';

type GrandExchangeOffersProps = {
  offers: Array<any>; // TODO: REAL TYPE
  onMount: () => void;
};

const useStyles = makeStyles<Theme>(theme => {
  const offerSlotWidth = 150;

  return {
    root: {
      padding: theme.spacing(1),
      display: 'inline-flex',
      flexWrap: 'wrap',
      // width: offerSlotWidth * 4,  // TODO: WHAT ABOUT MOBILE?
      // height: 32 * 8,
      background: '#494034bb',
      boxSizing: 'content-box',
      border: `1px solid ${amber[500]}`,
      outline: `1px solid ${grey[700]}`
    },
    offerSlot: {
      border: `1px solid ${amber[500]}`,
      outline: `1px solid ${grey[700]}`,
      width: offerSlotWidth,
      margin: theme.spacing(1)
    },
    itemImageContainer: {
      background: 'rgba(128,128,128,1)',
      width: 42,
      height: 42,
      display: 'inline-flex',
      alignItems: 'center',
      justifyContent: 'center',
      border: `1px solid ${amber[500]}`,
      outline: `1px solid ${grey[700]}`,
      boxSizing: 'content-box',
      marginRight: '0.25em'
    },
    title: {
      textAlign: 'center',
      fontSize: '1.25em',
      borderBottom: `1px solid ${amber[500]}`
    },
    itemRow: {
      display: 'flex',
      padding: '0.5em 0.25em'
    },
    progressBarRoot: {
      height: 15,
      boxShadow: 'inset 3px 3px 0px 0px rgba(0,0,0,0.37)',
      border: `1px solid ${commonColours.black}`
    },
    progressBar: {
      backgroundColor: 'rgba(0,0,0,0.25)'
    },
    progressBarIncomplete: {
      backgroundColor: '#FF9040',
      boxShadow: 'inset 3px 3px 0px 0px rgba(0,0,0,0.37)'
    },
    progressBarComplete: {
      backgroundColor: green[700],
      boxShadow: 'inset 3px 3px 0px 0px rgba(0,0,0,0.37)'
    },
    price: {
      textAlign: 'center'
    },
    totalText: {
      fontSize: '1.25em',
      height: 32,
      // lineHeight: '32px',
      width: '100%',
      textAlign: 'center'
    }
  };
});

const GrandExchangeOffers: FunctionComponent<GrandExchangeOffersProps> = props => {
  // TODO: DISPLAY MESSAGE IF NO OFFERS
  const totalValue = sumBy(props.offers, offer => {
    const leftToTransfer = offer.totalQuantity - offer.transferredQuantity;
    return offer.wealthTransferred + leftToTransfer * offer.pricePerItem;
  });

  const classes = useStyles();

  useEffect(() => {
    return props.onMount();
  }, [props.onMount]);

  return (
    <div className={classes.root}>
      <RunescapeTypography className={classes.totalText}>
        Grand Exchange Value:
        <Number value={totalValue} />
      </RunescapeTypography>

      {props.offers.map((offer: any) => {
        const isBuying =
          offer.state === 'BUYING' ||
          offer.state === 'CANCELLED_BUY' ||
          offer.state === 'BOUGHT';
        const isEmpty = offer.state === 'EMPTY';

        const isComplete = offer.transferredQuantity === offer.totalQuantity;

        const progress =
          (offer.transferredQuantity / offer.totalQuantity) * 100;
        const title = isBuying ? 'Buy' : isEmpty ? 'Empty' : 'Sell';
        const progressBarClass = isComplete
          ? classes.progressBarComplete
          : classes.progressBarIncomplete;

        return (
          <div key={offer.index} className={classes.offerSlot}>
            <div className={classes.title}>
              <RunescapeTypography colour="orange">{title}</RunescapeTypography>
            </div>
            <div className={classes.itemRow}>
              <span className={classes.itemImageContainer}>
                <ItemImage
                  itemId={offer.itemId}
                  itemName={offer.itemName}
                  quantity={offer.totalQuantity}
                  icon={offer.itemIcon}
                />
              </span>
              <span>
                <RunescapeTypography>{offer.itemName}</RunescapeTypography>
              </span>
            </div>
            <LinearProgress
              value={progress}
              variant="determinate"
              classes={{
                root: classes.progressBarRoot,
                colorPrimary: classes.progressBar,
                barColorPrimary: progressBarClass
              }}
            />
            <div className={classes.price}>
              <RunescapeTypography>
                <Number value={offer.pricePerItem} /> coins
              </RunescapeTypography>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default GrandExchangeOffers;
