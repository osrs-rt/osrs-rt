import React, { useState, FunctionComponent, useEffect } from 'react';
import Slide from '@material-ui/core/Slide';
import SkillIcon from './SkillIcon';
import { ExitHandler } from 'react-transition-group/Transition';
import { XpChangeEvent } from '../utils/sharedTypes';
import RunescapeTypography from './RunescapeTypography';
import { makeStyles } from '@material-ui/styles';

export interface XpNotificationProps {
  xpChanges: Array<XpChangeEvent>;
  onAnimationComplete?: ExitHandler;
}

const useStyles = makeStyles({
  root: {
    position: 'absolute'
  },
  gain: {
    display: 'flex',
    alignItems: 'center'
  }
});

const XpNotification: FunctionComponent<XpNotificationProps> = props => {
  const [visible, setVisible] = useState(true);
  const classes = useStyles();

  // Immediately slide the component out
  useEffect(() => {
    setVisible(false);
  });

  return (
    <Slide
      direction="down"
      in={visible}
      appear={false}
      unmountOnExit={true}
      timeout={2000}
      onExited={props.onAnimationComplete}
    >
      <div className={classes.root}>
        {props.xpChanges.map(({ skill, xpGained }) => (
          <div key={skill} className={classes.gain}>
            <SkillIcon skill={skill} />
            <RunescapeTypography colour="black">
              &nbsp;{xpGained}
            </RunescapeTypography>
          </div>
        ))}
      </div>
    </Slide>
  );
};

export default XpNotification;
