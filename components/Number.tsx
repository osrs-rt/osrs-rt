import React, { FunctionComponent } from 'react';
import { formatNumber } from '../utils/stringFormatters';

interface Props {
  value?: number;
  className?: string;
  showZero?: boolean;
}

const Number: FunctionComponent<Props> = ({ value, className, showZero }) => {
  return (<span className={className}>{formatNumber(value, showZero)}</span>);
};

export default Number;
