import React from 'react';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/styles';
import { Theme } from '../utils/muiTheme';
import { Hidden } from '@material-ui/core';
import { Favorite } from '@material-ui/icons';
import { red } from '@material-ui/core/colors';

const useStyles = makeStyles<Theme>(
  theme => {
    return {
      root: {
        padding: '2em',
        backgroundColor: theme.palette.primary.light,
        display: 'flex'
      },
      grower: {
        flexGrow: 1
      },
      madeWithText: {
        display: 'flex',
        alignContent: 'center'
      }
    };
  },
  { classNamePrefix: 'PageFooter' }
);

const PageFooter = () => {
  const classes = useStyles();
  const runeliteDownloadUrl = `${process.env.api_url}/download`;

  return (
    <div className={classes.root}>
      <div className={classes.grower}>
        <Hidden smDown>
          <Typography display="inline">
            Get the latest version of Runelite that works with this site{' '}
            <Link color="secondary" href={runeliteDownloadUrl}>
              here
            </Link>
            .
          </Typography>
        </Hidden>
      </div>

      <Typography className={classes.madeWithText}>
        Made with <Favorite fontSize="small" htmlColor={red['400']} />
      </Typography>
    </div>
  );
};

export default PageFooter;
