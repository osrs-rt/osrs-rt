import React from 'react';
import grey from '@material-ui/core/colors/grey';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { formatNumberWithSuffix } from '../utils/stringFormatters';
import Tooltip from '@material-ui/core/Tooltip';
import RunescapeTypography, {
  RunescapeTypographyProps
} from './RunescapeTypography';
import { isCoins } from '../sharedCode/itemUtils';
import commonColours from '@material-ui/core/colors/common';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles({
  container: {
    position: 'relative',
    width: 36,
    height: 32
  },
  quantity: {
    position: 'absolute',
    top: '-3px',
    left: '0px',
    fontSize: '1em',
    userSelect: 'none'
  },
  tooltip: {
    backgroundColor: fade(grey[600], 1),
    color: '#FF9040',
    // boxShadow: theme.shadows[1],
    fontSize: '1.2em',
    lineHeight: 1,
    fontFamily: 'Runescape Small',
    textShadow: `1px 2px ${commonColours.black}`,
    borderRadius: 0,
    border: `1px solid #5A5245`,
    outline: `1px solid #383023`,
    padding: '2px 1px'
  }
});

type ItemImageSize = 'default' | 'large';

interface ItemImageProps {
  itemId: number;
  itemName: string;
  quantity?: number;
  icon?: string;
  size?: ItemImageSize;
}

const DEFAULT_WIDTH = 36;
const DEFAULT_HEIGHT = 32;

interface Dimensions {
  width: number;
  height: number;
}

const sizeMultiplierMap: Record<ItemImageSize, number> = {
  default: 1,
  large: 1.5
};

function getImageDimensions(size: ItemImageSize): Dimensions {
  const multiplier = sizeMultiplierMap[size] || 1;
  return {
    width: DEFAULT_WIDTH * multiplier,
    height: DEFAULT_HEIGHT * multiplier
  };
}

const ItemImage = (props: ItemImageProps) => {
  const { itemId, itemName, quantity = 1, icon, size = 'default' } = props;
  const classes = useStyles();

  const src = !icon
    ? `https://www.osrsbox.com/osrsbox-db/items-icons/${itemId}.png`
    : `data:image/png;base64,${icon}`;

  const dimensions = getImageDimensions(size);
  let root = <img {...dimensions} src={src} alt={itemName} />;

  if (quantity > 1) {
    let colour: RunescapeTypographyProps['colour'] = 'yellow';
    if (isCoins(itemId)) {
      colour = quantity < 10000000 ? 'white' : 'green';
    }

    root = (
      <div className={classes.container}>
        <RunescapeTypography className={classes.quantity} colour={colour}>
          {formatNumberWithSuffix(quantity)}
        </RunescapeTypography>
        {root}
      </div>
    );
  }

  if (itemName) {
    root = (
      <Tooltip
        title={itemName}
        enterDelay={200}
        classes={{ tooltip: classes.tooltip }}
      >
        {root}
      </Tooltip>
    );
  }

  return root;
};

export default ItemImage;
