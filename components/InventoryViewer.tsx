import React, { FunctionComponent, useEffect } from 'react';
import ItemImage from './ItemImage';
import amber from '@material-ui/core/colors/amber';
import grey from '@material-ui/core/colors/grey';
import cn from 'classnames';
import Number from './Number';
import sumBy from 'lodash/sumBy';
import { calculatePrice } from '../sharedCode/itemUtils';
import RunescapeTypography from './RunescapeTypography';
import { makeStyles } from '@material-ui/styles';
import { Theme } from '../utils/muiTheme';
import { Item } from '../utils/sharedTypes';

const useStyles = makeStyles<Theme>(theme => ({
  // TODO: MAKE CONTAINER A GENERIC COMPONENT
  container: {
    padding: theme.spacing(1),
    display: 'flex',
    flexWrap: 'wrap',
    width: 40 * 4,
    // height: 32 * 8,
    background: '#494034bb',
    boxSizing: 'content-box',
    border: `1px solid ${amber[500]}`,
    outline: `1px solid ${grey[700]}`
  },
  itemPlaceholder: {
    width: 36,
    height: 32,
    display: 'inline-block'
  },
  totalText: {
    fontSize: '1.25em',
    height: 32,
    // lineHeight: '32px',
    width: '100%',
    textAlign: 'center'
  }
}));

interface InventoryViewerProps {
  inventory: Array<InventoryItem>;
  subscribeToInventoryChanges: () => () => void;
  className?: string;
}

export interface InventoryItem extends Item {
  index: number;
}

const InventoryViewer: FunctionComponent<InventoryViewerProps> = ({
  inventory = [],
  subscribeToInventoryChanges,
  className
}) => {
  const classes = useStyles();

  useEffect(() => {
    return subscribeToInventoryChanges();
  }, [subscribeToInventoryChanges]);

  const items = inventory.filter(x => x != null);
  const totalValue = sumBy(items, calculatePrice);

  return (
    <div className={cn(classes.container, className)}>
      <RunescapeTypography className={classes.totalText}>
        Inventory Value:
        <Number value={totalValue} />
      </RunescapeTypography>

      {inventory.map((item, index) => {
        const itemElement = !item ? null : (
          <ItemImage
            itemId={item.id}
            quantity={item.quantity}
            itemName={item.name}
            icon={item.icon}
          />
        );

        return (
          <span
            key={(item && item.index) || index}
            className={classes.itemPlaceholder}
          >
            {itemElement}
          </span>
        );
      })}
    </div>
  );
};

export default InventoryViewer;
