import React, { FunctionComponent } from 'react';
import Typography from '@material-ui/core/Typography';
import yellow from '@material-ui/core/colors/yellow';
import green from '@material-ui/core/colors/green';
import commonColours from '@material-ui/core/colors/common';
import cn from 'classnames';
import makeStyles from '@material-ui/core/styles/makeStyles';

export interface RunescapeTypographyProps {
  colour?: 'yellow' | 'white' | 'green' | 'orange' | 'black';
  className?: string;
}

const useStyles = makeStyles({
  root: {
    fontFamily: 'Runescape Small, monospace',
    lineHeight: 1,
    fontSize: '1em',
    textShadow: `1px 2px ${commonColours.black}`,
  },
  yellow: {
    color: yellow[500],
  },
  orange: {
    color: '#FF9040',
  },
  white: {
    color: commonColours.white
  },
  green: {
    color: green.A400,
    // color: '#00ff80'
  },
  black: {
    color: commonColours.black,
    textShadow: `1px 2px ${commonColours.white}`,
  }

});

const RunescapeTypography: FunctionComponent<RunescapeTypographyProps> = ({ children, colour = 'yellow', className }) => {
  const classes = useStyles();

  return (
    <Typography className={cn(classes.root, classes[colour], className)} display="inline">
      {children}
    </Typography>
  )
};

export default RunescapeTypography;
