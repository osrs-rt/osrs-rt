import React, { useState } from 'react';
import Jumbotron from '../components/Jumbotron';
import Typography from '@material-ui/core/Typography';
import RouterLink from 'next/link';
import Link from '@material-ui/core/Link';
import { MyAppPage } from '../utils/sharedTypes';
import PageHeader from './PageHeader';
import PageContent from './PageContent';
import PageFooter from './PageFooter';
import FeatureBullet from './FeatureBullet';
import AccessTime from '@material-ui/icons/AccessTime';
import MenuBook from '@material-ui/icons/MenuBook';
import TrackChanges from '@material-ui/icons/TrackChanges';
import Money from '@material-ui/icons/Money';
import makeStyles from '@material-ui/styles/makeStyles';
import { darkTheme, Theme } from '../utils/muiTheme';
import jumbotronImage from '../assets/jumbotron.png';
import { ThemeProvider } from '@material-ui/styles';
import { useTheme } from '@material-ui/core';
import { TinyColor } from '@ctrl/tinycolor';

const useStyles = makeStyles<Theme>(
  theme => {
    return {
      featureList: {
        display: 'grid',
        gridTemplateColumns: 'repeat(auto-fill,minmax(250px, 1fr))'
      },
      dark: {
        color: theme.palette.common.white
      }
    };
  },
  { name: 'LoggedOutIndex' }
);

const LoggedOutIndex: MyAppPage = props => {
  const classes = useStyles();
  const theme = useTheme();

  const [headerVisibility, setHeaderVisibility] = useState(
    props.username ? 1 : 0
  );

  function onVisibilityChanged(visibility: number) {
    // Set the visibility of the header to either how far the page has scrolled or how far the jumbotron has scrolled, whichever is bigger
    // NOTE: This can likely be removed if a min-height of 100vh is added to everything under the jumbotron as the page height is guaranteed to be 200vh
    const pageOverScroll =
      document.body.scrollHeight - document.body.clientHeight;
    const currentPageScroll = window.pageYOffset;
    const pageScrollPercent = currentPageScroll / pageOverScroll;

    const jumbotronScrollPercent = 1 - visibility;
    const headerVisibility = Math.max(
      jumbotronScrollPercent,
      pageScrollPercent
    );

    setHeaderVisibility(headerVisibility);
  }

  const darkText = new TinyColor(theme.palette.text.primary);
  const whiteText = new TinyColor(
    darkTheme.palette.getContrastText(theme.palette.text.primary)
  );
  const headerTextColor = whiteText
    .mix(darkText, headerVisibility * 100)
    .toRgbString();

  return (
    <React.Fragment>
      <ThemeProvider theme={darkTheme}>
        <PageHeader
          appBarStyle={{ color: headerTextColor }}
          text={process.env.secret_mode ? '' : 'Old School Real-time Companion'}
          backgroundVisibility={headerVisibility}
        />

        <Jumbotron
          onVisibilityChanged={onVisibilityChanged}
          image={process.env.secret_mode ? undefined : jumbotronImage}
        >
          {!process.env.secret_mode ? (
            <React.Fragment>
              <Typography variant="h2" color="textPrimary">
                Old School real-time companion
              </Typography>
              <Typography variant="h4" component="h3" color="textPrimary">
                Keep track of your Old School game from anywhere in real-time
              </Typography>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Typography variant="h2" color="textPrimary">
                Jumbotron
              </Typography>
              <Typography variant="h4" component="h3" color="textPrimary">
                goes here
              </Typography>
            </React.Fragment>
          )}
        </Jumbotron>
      </ThemeProvider>

      <PageContent>
        <div>
          <Typography variant="h4">Features</Typography>

          <div className={classes.featureList}>
            <FeatureBullet icon={AccessTime} text="Real-time tracking of XP" />
            <FeatureBullet icon={MenuBook} text="Skill calculators" />
            <FeatureBullet
              icon={TrackChanges}
              text="Inventory, equipment and bank tracking"
            />
            <FeatureBullet icon={Money} text="Alchemy guide" />
          </div>

          <Typography>
            Got the{' '}
            <RouterLink href="/profile">
              <Link href="/profile">profile</Link>
            </RouterLink>{' '}
            page and enter an OSRS username to start watching for XP gains and
            other info
          </Typography>
        </div>
      </PageContent>

      <PageFooter />
    </React.Fragment>
  );
};

export default LoggedOutIndex;
