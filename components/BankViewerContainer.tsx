import React, { Component } from 'react';
import query from '../queries/GetBank.graphql';
import subscription from '../queries/SubscribeToBankChanged.graphql';
import { Query } from 'react-apollo';
import BankViewer from './BankViewer';
import { itemDtoToModel } from '../sharedCode/itemUtils';
import { ContainerItemQuery } from '../utils/sharedTypes';

interface BankViewerContainerProps {
  username: string;
  className?: string;
}

class BankViewerContainer extends Component<BankViewerContainerProps> {
  render() {
    const { username, ...containerProps } = this.props;

    if (!username) return null;

    return (
      <Query<{ playerBank: Array<ContainerItemQuery> }> query={query} variables={{ playerId: username }}>
        {({ data: originalData, subscribeToMore, error }) => {
          if (error) {
            console.error(error);
            return <span>An error occurred whilst loading bank</span>;
          }
          if (!originalData) return <span>No data</span>;

          const bank = (originalData.playerBank || []).map(itemDtoToModel);

          const subscribeToBankChanges = () => {
            console.log('Subscribing to bank changes');
            const unsubscribe = subscribeToMore({
              variables: {
                playerId: username
              },
              document: subscription,
              updateQuery(prev, { subscriptionData }: { subscriptionData: any } ) {
                console.log('New bank data received');
                if (
                  !subscriptionData.data ||
                  !subscriptionData.data.bankChanged
                )
                  return prev;
                return {
                  playerBank: subscriptionData.data.bankChanged.newItems
                };
              }
            });

            return () => {
              console.log('Unsubscribing from from bank changes');
              unsubscribe();
            }
          };

          return (
            <BankViewer
              {...containerProps}
              bank={bank}
              subscribeToBankChanges={subscribeToBankChanges}
            />
          );
        }}
      </Query>
    );
  }
}

export default BankViewerContainer;
