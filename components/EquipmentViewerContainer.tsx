import React, { FunctionComponent } from 'react';
import { Query } from 'react-apollo';
import query from '../queries/GetEquipment.graphql';
import subscription from '../queries/SubscribeToEquipmentChanged.graphql';
import EquipmentViewer from './EquipmentViewer';
import { itemDtoToModel } from '../sharedCode/itemUtils';
import { ContainerItemQuery } from '../utils/sharedTypes';

interface EquipmentViewerContainerProps {
  username: string;
  className?: string;
}

const EquipmentViewerContainer: FunctionComponent<EquipmentViewerContainerProps> = ({ username, ...containerProps }) => {
  return (
    <Query<{ playerEquipment: Array<ContainerItemQuery> }> query={query} variables={{ playerId: username }}>
      {({ data: originalData, subscribeToMore, error }) => {
        if (error) return <span>An error occurred whilst loading equipment</span>;
        if (!originalData) return <span>No data</span>;

        const equipment = (originalData.playerEquipment || []).map(itemDtoToModel);
        const subscribe = () => {
          console.log('Subscribing to equipment changes');
          const unsubscribe = subscribeToMore({
            variables: {
              playerId: username
            },
            document: subscription,
            updateQuery(prev, { subscriptionData }: { subscriptionData: any }) {
              console.log('Equipment has been updated');
              if (
                !subscriptionData.data ||
                !subscriptionData.data.equipmentChanged
              )
                return prev;
              return {
                playerEquipment: subscriptionData.data.equipmentChanged.newItems
              };
            }
          });

          return () => {
            console.log('Unsubscribing from from equipment changes');
            unsubscribe();
          }
        };

        return (
          <EquipmentViewer
            equipment={equipment}
            subscribeToEquipmentChanges={subscribe}
            {...containerProps}
          />
        );
      }}
    </Query>
  );
};

export default EquipmentViewerContainer;
