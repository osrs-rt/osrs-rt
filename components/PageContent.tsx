import React, { FunctionComponent } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Theme } from '../utils/muiTheme';

const useStyles = makeStyles<Theme>(theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    flexGrow: 1
  }
}));

type PageContentProps = {
  error?: Error,
  // children: ReactNode
}

const PageContent: FunctionComponent<PageContentProps> = (props) => {
  const classes = useStyles();

  // TOOD: BETTER ERROR DISPLAY
  const content = props.error ? (
    <div>
      An unknown error has occurred
    </div>
  ) : props.children;

  return <div className={classes.root}>{content}</div>;
};

export default PageContent;
