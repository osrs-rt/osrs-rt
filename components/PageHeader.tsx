import React, { FunctionComponent, ReactNode, useState } from 'react';
import XpNotificationArea from './XpNotificationArea';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AccountCircle from '@material-ui/icons/AccountCircle';
import IconButton from '@material-ui/core/IconButton';
import Link from 'next/link';
import Avatar from '@material-ui/core/Avatar';
import TotalExperienceBar from './TotalExperienceBar';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { useRouter } from 'next/router';
import { makeStyles, ThemeProvider } from '@material-ui/styles';
import { Collapse } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import theme, { Theme } from '../utils/muiTheme';
import { adjustAlphaOfShadow, percentToHex } from '../utils/colourUtils';

interface MenuItem {
  text?: string;
  type?: string;
  href?: string;
  children?: MenuItem[];
  selected?: boolean;
  level?: number;
}

const menuItems: Array<MenuItem> = [
  {
    text: 'Home',
    href: '/'
  },
  {
    type: 'parent',
    text: 'Skill Tools',
    children: [
      {
        text: 'Agility',
        href: '/skillguides/agility'
      },
      {
        text: 'Construction',
        href: '/skillguides/construction'
      },
      {
        text: 'Herblore',
        href: '/skillguides/herblore'
      },
      {
        text: 'Magic',
        href: '/skillguides/magic'
      },
      {
        text: 'Mining',
        href: '/skillguides/mining'
      }
    ]
  },
  {
    text: 'Quests',
    href: '/quests'
  },
  {
    text: 'Alch Tools',
    href: '/alch'
  },
  {
    text: 'Skills',
    href: '/skills'
  }
];

// @ts-ignore
const useStyles = makeStyles<Theme>(theme => ({
  header: (props: PropTypes) => {
    const visibility =
      props.backgroundVisibility != null ? props.backgroundVisibility : 1;
    const backgroundColor =
      theme.palette.primary.main + percentToHex(visibility);
    const boxShadow = adjustAlphaOfShadow(theme.shadows[4], visibility);
    return {
      backgroundColor,
      boxShadow,
      transition: 'none'
    };
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  list: {
    width: 250
  },
  listHeader: {
    marginBottom: 10
  }
}));

const useMenuItemStyles = makeStyles((theme: Theme) => ({
  menuItem: (props: MenuItem) => ({
    marginLeft: (props.level || 0) * theme.spacing(2),
    fontSize: 1 - (props.level || 0) * 0.1 + 'rem'
  })
}));

interface PropTypes {
  text: ReactNode;
  username?: string;
  backgroundVisibility?: number;
  appBarStyle?: React.CSSProperties;
}

function MenuItem(item: MenuItem) {
  const classes = useMenuItemStyles(item);
  const [open, setOpen] = React.useState(false);
  const handleClick = (event: any) => {
    event.stopPropagation();
    setOpen(!open);
  };

  return item.type === 'parent' ? (
    <React.Fragment>
      <ListItem button onClick={handleClick}>
        <ListItemText primary={item.text} />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        {item.children &&
          item.children.map(child => (
            <MenuItem
              key={child.text}
              {...child}
              level={(item.level || 0) + 1}
            />
          ))}
      </Collapse>
    </React.Fragment>
  ) : (
    <Link href={item.href || ''} key={item.text}>
      {/*
                // @ts-ignore */}
      <ListItem button component="a" href={item.href} selected={item.selected}>
        <ListItemText
          primary={item.text}
          classes={{ primary: classes.menuItem }}
        />
      </ListItem>
    </Link>
  );
}

const PageHeader: FunctionComponent<PropTypes> = props => {
  const { text, username } = props;
  const router = useRouter();
  const classes = useStyles(props);

  const [menuIsOpen, setMenuIsOpen] = useState(false);
  const openMenu = () => {
    setMenuIsOpen(true);
  };
  const closeMenu = () => {
    setMenuIsOpen(false);
  };

  const drawerHeader = !username ? null : (
    <Link href="/profile" passHref>
      {/*
      // @ts-ignore */}
      <ListItem
        button
        component="a"
        href="/profile"
        className={classes.listHeader}
      >
        <ListItemIcon>
          <Avatar>{username[0].toUpperCase()}</Avatar>
        </ListItemIcon>
        <ListItemText primary={username} />
      </ListItem>
    </Link>
  );

  const pathname = router && router.pathname;

  return (
    <AppBar
      position="sticky"
      className={classes.header}
      style={props.appBarStyle}
      elevation={0}
    >
      <XpNotificationArea />

      <Toolbar>
        <IconButton
          className={classes.menuButton}
          onClick={openMenu}
          color="inherit"
        >
          <MenuIcon />
        </IconButton>
        <Typography
          component="h1"
          variant="h6"
          className={classes.grow}
          color="inherit"
        >
          {text}
        </Typography>

        {username && <TotalExperienceBar />}

        <Link href="/profile">
          <IconButton color="inherit">
            {!username ? (
              <AccountCircle />
            ) : (
              <Avatar>{username[0].toUpperCase()}</Avatar>
            )}
          </IconButton>
        </Link>
      </Toolbar>

      <ThemeProvider theme={theme}>
        <Drawer open={menuIsOpen} onClose={closeMenu}>
          <div
            tabIndex={0}
            role="button"
            onClick={closeMenu}
            onKeyDown={closeMenu}
          >
            <List className={classes.list}>
              {drawerHeader}

              {menuItems.map(item => (
                <MenuItem
                  key={item.text}
                  {...item}
                  selected={item.href === pathname}
                />
              ))}
            </List>
          </div>
        </Drawer>
      </ThemeProvider>
    </AppBar>
  );
};

export default PageHeader;
