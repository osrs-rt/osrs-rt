import dynamic from "next/dynamic";
import React, { CSSProperties, FunctionComponent } from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';
import { MapProps } from './Map';

interface LazyLoadedMapProps extends Partial<MapProps> {
  style?: CSSProperties;
  loading?: boolean;
}

const useStyles = makeStyles({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden'
  }
});

const Loading = () => (
    <Typography>Map is loading...</Typography>
);

const Map = dynamic(() => import('./Map'), {
  ssr: false,
  loading: Loading,

});



const LazyLoadedMap: FunctionComponent<LazyLoadedMapProps> =  ({ style, loading, ...others }) => {
  const classes = useStyles();

  return (
    <div style={style} className={classes.root}>
      {loading && <Loading />}
      {!loading && <Map {...others} />}
    </div>
  );
};

export default LazyLoadedMap;