import { makeStyles } from '@material-ui/styles';
import { useInView } from 'react-intersection-observer';
import React, { useEffect } from 'react';
import cn from 'classnames';

const useStyles = makeStyles(theme => {
  return {
    jumbotron: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      position: 'absolute',
      left: 0,
      top: 0,
      height: '100vh',
      width: '100%',
      background: 'rgba(0,0,0,.9)',
      backgroundImage: (props: JumbotronProps) =>
        props.image && `url("${props.image}")`,
      backgroundAttachment: 'fixed',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: '50%',

      '& + *': {
        marginTop: 'calc(100vh - 64px)'
      }
    }
  };
});

interface JumbotronProps {
  image?: string;
  onVisibilityChanged: (visibility: number) => void;
  children?: React.ReactNode;
  className?: string;
}

const THRESHOLD = Array.from({ length: 101 }, (v, k) => k * 0.01);

export default function Jumbotron(props: JumbotronProps) {
  const classes = useStyles(props);
  // @ts-ignore
  const [ref, inView, entry] = useInView({
    threshold: THRESHOLD
  });

  const visibility: number | undefined = entry && entry.intersectionRatio;

  // Fire the onVisibilityChanged callback based on the visibility value. Only do so when it's value changes
  useEffect(() => {
    if (!props.onVisibilityChanged || visibility == null) return;
    props.onVisibilityChanged(visibility);
  }, [visibility]);

  return (
    <div ref={ref} className={cn(classes.jumbotron, props.className)}>
      {props.children}
    </div>
  );
}
