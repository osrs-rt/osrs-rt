import React, { FunctionComponent, ReactNode } from 'react';
import Paper from '@material-ui/core/Paper';
import makeStyles from '@material-ui/styles/makeStyles';
import { Theme } from '../utils/muiTheme';
import { Typography } from '@material-ui/core';
import { ReactComponentLike } from 'prop-types';

const useStyles = makeStyles<Theme>(theme => {
  return {
    root: {
      textAlign: 'center',
      padding: theme.spacing(2),
      margin: theme.spacing(2),
    },
    icon: {
      fontSize: '2.5rem'
    }
  }
}, { name: 'FeatureBullet' });

interface FeatureBulletProps {
  icon: ReactComponentLike;
  text: ReactNode;
}

const FeatureBullet: FunctionComponent<FeatureBulletProps> = (props) => {
  const classes = useStyles();
  const Icon = props.icon;

  return (
    <Paper className={classes.root}>
      <div className={classes.icon}><Icon fontSize="inherit" /></div>

      <Typography variant="body1">{props.text}</Typography>
    </Paper>
  )
};

export default FeatureBullet
