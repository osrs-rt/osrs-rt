import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import Statistic from './Statistic';
import {
  GrandExchangeOffer,
  OnlineStatus
} from '../server/graphql/dataSources/PlayerInformationDataSource';
import { Grid, GridProps, styled } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { red, green } from '@material-ui/core/colors';
import { endOfDay, startOfDay } from 'date-fns';

const query = gql`
  query GetValue($playerId: String!, $startDate: Float!, $endDate: Float!) {
    player(playerId: $playerId) {
      value {
        total
      }

      onlineStatus

      level {
        overall
      }

      grandExchangeOffers {
        state
        transferredQuantity
        totalQuantity
        itemName
      }

      xpDelta(start: $startDate, end: $endDate) {
        overall
      }
    }
  }
`;

interface QueryResult {
  player: {
    onlineStatus: OnlineStatus;
    value: {
      total: number;
    };
    level: {
      overall: number;
    };
    grandExchangeOffers: Array<
      Pick<
        GrandExchangeOffer,
        'state' | 'transferredQuantity' | 'totalQuantity' | 'itemName'
      >
    >;
    xpDelta: {
      overall: number;
    };
  };
}

interface DashboardStatisticContainerProps {
  playerId: string;
}

const ContentArea = styled(({ onlineStatus, ...props }) => (
  <CardContent {...props} />
))({
  minHeight: 103,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  color: (props: { onlineStatus: OnlineStatus }) =>
    props.onlineStatus === 'ONLINE'
      ? green[500]
      : props.onlineStatus === 'OFFLINE'
      ? red[500]
      : undefined
});

const DashboardStatisticContainer = ({
  playerId
}: DashboardStatisticContainerProps) => {
  const startDate = startOfDay(new Date());
  const endDate = endOfDay(new Date());
  const { data } = useQuery<QueryResult>(query, {
    variables: {
      playerId,
      startDate: startDate.valueOf(),
      endDate: endDate.valueOf()
    }
  });

  // TODO: HANDLE NOT LOADED YET
  if (!data) return null;

  const xpGained = data.player.xpDelta.overall;

  const geSellOffers = data.player.grandExchangeOffers.filter(o =>
    ['SOLD', 'SELLING'].includes(o.state)
  );
  const geBuyOffers = data.player.grandExchangeOffers.filter(o =>
    ['BUYING', 'BOUGHT'].includes(o.state)
  );

  const gridProps: GridProps = {
    item: true,
    xs: 12,
    sm: 6,
    md: 3
  };

  return (
    <Grid container spacing={2}>
      <Grid {...gridProps}>
        <Card>
          <ContentArea onlineStatus={data.player.onlineStatus}>
            <Typography variant="h3" align="center">
              {data.player.onlineStatus}
            </Typography>
          </ContentArea>
        </Card>
      </Grid>

      <Grid {...gridProps}>
        <Statistic
          text="Total Player Level"
          value={data.player.level.overall}
        />
      </Grid>
      <Grid {...gridProps}>
        <Statistic text="XP Gained Today" value={xpGained} unit="XP" />
      </Grid>
      <Grid {...gridProps} md={4}>
        <Statistic
          text="Total Player Value"
          value={data.player.value.total}
          unit="GP"
        />
      </Grid>
      <Grid {...gridProps} xs={6} sm={3}>
        <Statistic text="GE Buy Offers" value={geBuyOffers.length} />
      </Grid>
      <Grid {...gridProps} xs={6} sm={3}>
        <Statistic text="GE Sell Offers" value={geSellOffers.length} />
      </Grid>
    </Grid>
  );
};

export default DashboardStatisticContainer;
