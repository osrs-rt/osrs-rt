import React, { FunctionComponent } from 'react';
import SkillIcon from './SkillIcon';
import Number from './Number';
import { usePlayerXp } from './PlayerXpContext';
import RunescapeTypography from './RunescapeTypography';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Hidden } from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    position: 'relative',
    border: '1px solid #5A5245',
    outline: '1px solid #383023',
    padding: '0.25em',
    fontSize: '1.25em',
    display: 'flex',
    alignItems: 'center',
    background: 'rgba(90, 82, 69, 0.25)',
    minWidth: '6em'
  },

  number: {
    flexGrow: 1,
    textAlign: 'right'
  }
});

const TotalExperienceBar: FunctionComponent = () => {
  const classes = useStyles();
  const skillsXp = usePlayerXp();
  if (!skillsXp) {
    return null;
  }

  return (
    <Hidden xsDown>
      <div className={classes.root}>
        <SkillIcon skill="overall" />
        <RunescapeTypography className={classes.number} colour="white">
          <Number value={skillsXp.overall} />
        </RunescapeTypography>
      </div>
    </Hidden>
  );
};

export default TotalExperienceBar;
