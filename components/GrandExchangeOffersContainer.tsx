import React, { FunctionComponent } from 'react';
import { Query } from 'react-apollo';
import query from '../queries/GetGrandExchangeOffers.graphql';
import subscription from '../queries/SubscribeToGrangeExchangeOffersChanged.graphql';
import GrandExchangeOffers from './GrandExchangeOffers';
import { ContainerItemQuery } from '../utils/sharedTypes';

type GrandExchangeOffersContainerProps = {
  username: string;
};

const GrandExchangeOffersContainer: FunctionComponent<
  GrandExchangeOffersContainerProps
> = props => {
  return (
    <Query<{ playerGrandExchangeOffers: Array<ContainerItemQuery> }> query={query} variables={{ playerId: props.username }}>
      {({ data, error, subscribeToMore }) => {
        if (error) {
          console.error(error);
          return <div>Error loading GE offers</div>;
        }
        if (!data) return <span>No data</span>;

        const subscribeToGEOfferChanges = () => {
          console.log('Subscribing to GE offers');
          const unsubscribe = subscribeToMore({
            variables: {
              playerId: props.username
            },
            document: subscription,
            updateQuery(prev, { subscriptionData }: { subscriptionData: any }) {
              console.log('New GE offer data received');

              if (
                !subscriptionData.data ||
                !subscriptionData.data.grandExchangeOffersChanged
              )
                return prev;
              return {
                playerGrandExchangeOffers:
                  subscriptionData.data.grandExchangeOffersChanged.offers
              };
            }
          });

          return () => {
            console.log('Unsubscribing from from GE Offer changes');
            unsubscribe();
          };
        };

        return (
          <GrandExchangeOffers
            offers={data.playerGrandExchangeOffers || []}
            onMount={subscribeToGEOfferChanges}
          />
        );
      }}
    </Query>
  );
};

export default GrandExchangeOffersContainer;
