import React, { useEffect, FunctionComponent } from 'react';
import ItemImage from './ItemImage';
import amber from '@material-ui/core/colors/amber';
import grey from '@material-ui/core/colors/grey';
import cn from 'classnames';
import sumBy from 'lodash/sumBy';
import { calculatePrice } from '../sharedCode/itemUtils';
import RunescapeTypography from './RunescapeTypography';
import Number from './Number';
import makeStyles from '@material-ui/styles/makeStyles';
import { Theme } from '../utils/muiTheme';
import { Item } from '../utils/sharedTypes';

const useStyles = makeStyles<Theme>(theme => ({
  container: {
    padding: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
    width: 44 * 5,
    height: 50 * 7,
    background: '#494034bb',
    boxSizing: 'content-box',
    border: `1px solid ${amber[500]}`,
    outline: `1px solid ${grey[700]}`,
    justifyContent: 'space-evenly'
  },
  itemContainer: {
    background: 'rgba(128,128,128,1)',
    width: 44,
    height: 44,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    border: `1px solid ${amber[500]}`,
    outline: `1px solid ${grey[700]}`,
    boxSizing: 'content-box'
  },
  itemRow: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-around'
  },
  largeRow: {
    justifyContent: 'space-between'
  },
  totalText: {
    fontSize: '1.25em',
    height: 32,
    // lineHeight: '32px',
    width: '100%',
    textAlign: 'center'
  }
}));

interface EquipmentViewerProps {
  equipment: Array<any>; // TODO: REAL TYPE
  subscribeToEquipmentChanges: () => () => void;
  className?: string;
}

const EquipmentViewer: FunctionComponent<EquipmentViewerProps> = ({
  equipment = [],
  subscribeToEquipmentChanges,
  className
}) => {
  const classes = useStyles();

  useEffect(() => {
    return subscribeToEquipmentChanges();
  }, [subscribeToEquipmentChanges]);

  const ItemContainer = ({ item }: { item: Item }) => (
    <div className={classes.itemContainer}>
      {item && (
        <ItemImage
          itemId={item.id}
          quantity={item.quantity}
          itemName={item.name}
          icon={item.icon}
        />
      )}
    </div>
  );

  const items = equipment.filter(x => x != null);
  const totalValue = sumBy(items, calculatePrice);

  const headItem = equipment.find(item => item.index === 0);
  const capeItem = equipment.find(item => item.index === 1);
  const neckItem = equipment.find(item => item.index === 2);
  const ammoItem = equipment.find(item => item.index === 13);
  const weaponItem = equipment.find(item => item.index === 3);
  const bodyItem = equipment.find(item => item.index === 4);
  const shieldItem = equipment.find(item => item.index === 5);
  const legItem = equipment.find(item => item.index === 7);
  const handItem = equipment.find(item => item.index === 9);
  const feetItem = equipment.find(item => item.index === 10);
  const fingerItem = equipment.find(item => item.index === 12);

  return (
    <div className={cn(classes.container, className)}>
      <RunescapeTypography className={classes.totalText}>
        Equipment Value:
        <Number value={totalValue} />
      </RunescapeTypography>

      <div className={classes.itemRow}>
        <ItemContainer item={headItem} />
      </div>

      <div className={classes.itemRow}>
        <ItemContainer item={capeItem} />
        <ItemContainer item={neckItem} />
        <ItemContainer item={ammoItem} />
      </div>

      <div className={cn(classes.itemRow, classes.largeRow)}>
        <ItemContainer item={weaponItem} />
        <ItemContainer item={bodyItem} />
        <ItemContainer item={shieldItem} />
      </div>

      <div className={classes.itemRow}>
        <ItemContainer item={legItem} />
      </div>

      <div className={cn(classes.itemRow, classes.largeRow)}>
        <ItemContainer item={handItem} />
        <ItemContainer item={feetItem} />
        <ItemContainer item={fingerItem} />
      </div>
    </div>
  );
};

export default EquipmentViewer;
