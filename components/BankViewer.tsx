import React, { useEffect, FunctionComponent } from 'react';
import ItemImage from './ItemImage';
import amber from '@material-ui/core/colors/amber';
import grey from '@material-ui/core/colors/grey';
import cn from 'classnames';
import sortBy from 'lodash/sortBy';
import sumBy from 'lodash/sumBy';
import Number from './Number';
import { calculatePrice } from '../sharedCode/itemUtils';
import RunescapeTypography from './RunescapeTypography';
import { makeStyles } from '@material-ui/styles';
import { Theme } from '../utils/muiTheme';
import { Item } from '../utils/sharedTypes';

const useStyles = makeStyles<Theme>(theme => ({
  // TODO: MAKE CONTAINER A GENERIC COMPONENT
  container: {
    padding: theme.spacing(1),
    display: 'flex',
    flexWrap: 'wrap',
    background: '#494034bb',
    border: `1px solid ${amber[500]}`,
    outline: `1px solid ${grey[700]}`,
    width: 8 * 45
  },
  totalText: {
    width: '100%',
    textAlign: 'center',
    fontSize: '1.25em'
  },
  itemImage: {
    margin: '2px'
  }
}));

interface BankViewerProps {
  bank: Item[];
  subscribeToBankChanges: () => () => void;
  className?: string;
}

const BankViewer: FunctionComponent<BankViewerProps> = ({
  bank = [],
  subscribeToBankChanges,
  className
}) => {
  const classes = useStyles();

  useEffect(() => {
    return subscribeToBankChanges();
  }, [subscribeToBankChanges]);

  bank = sortBy(bank, 'index');
  const totalValue =  sumBy(bank, calculatePrice);

  return (
    <div className={cn(classes.container, className)}>
      <RunescapeTypography className={classes.totalText} colour="yellow">
        Bank Value:
        <Number value={totalValue} />
      </RunescapeTypography>

      {bank.map((item) => {
        return (
          <div className={classes.itemImage} key={item.id}>
            <ItemImage itemId={item.id} quantity={item.quantity} itemName={item.name} icon={item.icon} />
          </div>
        );
      })}
    </div>
  );
};

export default BankViewer;
