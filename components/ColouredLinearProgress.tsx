import * as React from 'react';
import { makeStyles } from '@material-ui/styles';
import LinearProgress, { LinearProgressProps } from '@material-ui/core/LinearProgress';

interface Props {
  color?: string;
  backgroundColor?: string;
}

const useStyles = makeStyles({
  barColorPrimary: {
    backgroundColor: (props: Props) => props.color
  },
  colorPrimary: {
    backgroundColor: (props: Props) => props.backgroundColor
  }
});


export default function ColouredLinearProgress(props: Props & Omit<LinearProgressProps, keyof Props>) {
  const { color, backgroundColor , ...other } = props;
  const classes = useStyles(props);
  return <LinearProgress classes={classes} {...other} />;
}