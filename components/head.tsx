import React, { FunctionComponent } from 'react';
import NextHead from 'next/head';
import muiTheme from '../utils/muiTheme';

const defaultDescription = '';

const Head: FunctionComponent<PropTypes> = props => {
  const title = [props.title, process.env.site_name]
    .filter(x => !!x)
    .join(' - ');

  return (
    <NextHead>
      <title>{title}</title>
      <meta
        name="description"
        content={props.description || defaultDescription}
      />
      <link rel="icon" type="image/png" href="/static/favicon.png" />
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"
      />

      <meta charSet="utf-8" />
      {/* Use minimum-scale=1 to enable GPU rasterization */}
      <meta
        name="viewport"
        content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
      />
      {/* PWA primary color */}
      <meta name="theme-color" content={muiTheme.palette.primary.main} />
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
      />

      <link
        rel="preload"
        as="font"
        href="/static/fonts/runescape_small/RuneScape-Small.woff2"
        type="font/woff2"
        crossOrigin="anonymous"
      />
      <link
        rel="stylesheet"
        href="/static/fonts/runescape_small/RuneScape-Small.css"
      />

      {/*TODO: ONLY LOAD CSS WHEN REQUIRED https://github.com/zeit/next.js/issues/299#issuecomment-474722259 */}
      <link
        rel="stylesheet"
        href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
        integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
        crossOrigin=""
      />

      <style>{`
            html, body, #__next {
              height: 100vh;
            }
          `}</style>
    </NextHead>
  );
};

type PropTypes = {
  title?: string;
  description?: string;
};

export default Head;
