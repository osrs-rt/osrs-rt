import React, { FunctionComponent, useState } from 'react';
import {
  getExperienceForLevel,
  getLevelForExperience
} from '../sharedCode/experience';
import Card from '@material-ui/core/Card/index';
import CardContent from '@material-ui/core/CardContent/index';
import Typography from '@material-ui/core/Typography/index';
import TextField from '@material-ui/core/TextField/index';
import { formatNumber, stringToNumber } from '../utils/stringFormatters';
import Number from './Number';
import { usePlayerXp } from './PlayerXpContext';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Theme } from '../utils/muiTheme';

const useStyles = makeStyles((theme: Theme) => {
  return {
    container: {
      display: 'flex',
      flexWrap: 'wrap'
    },
    label: {
      margin: 'auto 1em auto 0'
    },
    card: {
      marginBottom: theme.spacing(2),
      overflowX: 'auto'
    }
  };
});

interface XpToLevelReportRenderFunctionProps {
  fromLevel: number;
  toLevel: number;
  fromXp: number;
  toXp: number;
}

export type XpToLevelReportRenderFunction = (
  props: XpToLevelReportRenderFunctionProps
) => {};

interface Props {
  skill: string;
  children: XpToLevelReportRenderFunction | null;
}

const XpToLevelReport: FunctionComponent<Props> = ({ skill, children }) => {
  const [fromLevelOverride, setFromLevelOverride] = useState<
    number | undefined
  >(undefined);
  const [toLevelOverride, setToLevelOverride] = useState<number | undefined>(
    undefined
  );
  const [fromXpOverride, setFromXpOverride] = useState<number | undefined>(
    undefined
  );
  const [toXpOverride, setToXpOverride] = useState<number | undefined>(
    undefined
  );
  const classes = useStyles();
  const skillsXp = usePlayerXp();

  // @ts-ignore
  const currentPlayerXpForSkill = (skillsXp || {})[skill] || 0;

  const currentPlayerLevelFprSkill = getLevelForExperience(
    currentPlayerXpForSkill
  );

  const fromLevel =
    fromLevelOverride ||
    (fromXpOverride
      ? getLevelForExperience(fromXpOverride as number)
      : currentPlayerLevelFprSkill);
  const toLevel =
    toLevelOverride ||
    (toXpOverride
      ? getLevelForExperience(toXpOverride as number)
      : fromLevel + 1);

  const fromXp =
    fromXpOverride ||
    (!fromLevelOverride ? currentPlayerXpForSkill : getExperienceForLevel(fromLevel));
  const toXp = toXpOverride || getExperienceForLevel(toLevel);

  return (
    <div className={''}>
      <Card className={classes.card}>
        <CardContent className={classes.container}>
          <div className={classes.container}>
            <Typography
              className={classes.label}
              color="textSecondary"
              gutterBottom
            >
              Level
            </Typography>

            <TextField
              id="fromLevel"
              label="From"
              className={''}
              value={fromLevel}
              onChange={e => {
                setFromXpOverride(undefined);
                setFromLevelOverride(stringToNumber(e.target.value));
              }}
              margin="normal"
            />

            <TextField
              id="toLevel"
              label="To"
              className={''}
              value={toLevel}
              onChange={e => {
                setToXpOverride(undefined);
                setToLevelOverride(stringToNumber(e.target.value));
              }}
              margin="normal"
            />
          </div>

          <div className={classes.container}>
            <Typography
              className={classes.label}
              color="textSecondary"
              gutterBottom
            >
              XP
            </Typography>

            <TextField
              id="fromXp"
              label="From"
              className={''}
              value={formatNumber(fromXp, true)}
              onChange={e => {
                setFromLevelOverride(undefined);
                setFromXpOverride(stringToNumber(e.target.value));
              }}
              margin="normal"
            />

            <TextField
              id="toXp"
              label="To"
              className={''}
              value={formatNumber(toXp, true)}
              onChange={e => {
                setToLevelOverride(undefined);
                setToXpOverride(stringToNumber(e.target.value));
              }}
              margin="normal"
            />
          </div>
        </CardContent>
      </Card>

      <Card className={classes.card}>
        <CardContent>
          <Typography color="textSecondary" gutterBottom>
            XP Required
          </Typography>
          <Typography variant="body2">
            <Number value={toXp - fromXp} showZero />
          </Typography>
        </CardContent>
      </Card>

      {children && children({ fromLevel, toLevel, fromXp, toXp })}
    </div>
  );
};

export default XpToLevelReport;
