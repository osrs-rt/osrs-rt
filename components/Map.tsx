import React, { Component } from 'react';
import leaflet, {
  Icon as LeafletIcon,
  LatLngBoundsExpression,
  LatLngExpression,
  LatLngTuple,
  Map as LeafletMap
} from 'leaflet';
import Position from '../utils/map/Position';

export interface Location {
  x: number;
  y: number;
  z: 0 | 1 | 2;
}

export interface MapProps {
  playerLocation?: Location;
  subscribeToLocationChanges?: Function;
}

interface Icon {
  filename: string;
  name: string;
  width: number;
  height: number;
  "wiki-link": string;
  category: string;
}

interface IconWithMarker extends Icon {
  markerIcon: LeafletIcon;
}

interface IconList<T extends Icon = Icon> {
  [index: string]: T;
}

interface MapFeatureList {
  features: Array<MapFeature>;
}

interface MapFeature {
  geometry: {
    type: string;
    coordinates: [number, number, number]
  },
  properties: {
    icon: string
  }
}


class Map extends Component<MapProps> {
  // @ts-ignore
  map: LeafletMap;
  // @ts-ignore
  playerIndicator: leaflet.Marker;
  unsubscribeFromLocationChanges: Function | null = null;

  private static readonly DEFAULT_LOCATION = { x: 3218, y: 3218, z: 0 };

  async componentDidMount () {
    const location = new Position(this.props.playerLocation || Map.DEFAULT_LOCATION);
    console.log('Constructing map at location:', location);

    const iconSet: IconList<IconWithMarker> = {};


    // TODO: THIS CAN BE DONE ON THE SERVER AND CACHED
    const getConfig = fetch('https://maps.runescape.wiki/osrs/data/config.json').then(res => res.json());
    const getData = fetch('https://maps.runescape.wiki/osrs/data/dataloader.json').then(res => res.json());

    const [config, data] = await Promise.all([getConfig, getData]);

    const getIconLocationsList: Promise<MapFeatureList> = fetch(data.overlayMaps[0].dataSource).then(res => res.json());
    const getIconList = fetch(data.datasources[2].dataproviders.iconlist).then(res => res.json())
      .then((iconList: IconList) => {
        Object.entries(iconList.icons).forEach(([ key, icon ]) => {
          icon.markerIcon = leaflet.icon({
            iconUrl: iconList.folder + icon.filename,
            iconSize: [icon.width, icon.height]
          });
          iconSet[key] = icon;
        });
      });

    const tileUrl = (config.baseTileURL + config.tileURLFormat)
      .replace('{mapID}', data.baseMaps[0].mapId)
      .replace('{cacheVersion}', data.baseMaps[0].cacheVersion)
      .replace('{p}', (this.props.playerLocation && this.props.playerLocation.z) || 0);

    leaflet.CRS.Simple.infinite = false;
    // @ts-ignore
    leaflet.CRS.Simple.projection.bounds = new leaflet.Bounds([[0, 0], [12800, 12800]]);

    this.map = leaflet.map('mapid', {
      renderer: leaflet.canvas(),
      maxZoom: data.baseMaps[0].zoomLimits[1],
      minZoom: data.baseMaps[0].zoomLimits[0],
      crs: leaflet.CRS.Simple,
      maxBounds: Map.translateBounds(data.baseMaps[0].bounds),
      maxBoundsViscosity: 0.5,

      attributionControl: false,
      zoomControl: false
    });


    const playerIndicatorUrl = '/static/player_location_indicator.gif';
    const playerIcon = leaflet.icon({
      iconUrl: playerIndicatorUrl,
      iconSize: [45, 45]
    });


    // TODO: ICON LIST NEEDS UPDATING ON PLANE CHANGE
    Promise.all([getIconLocationsList, getIconList]).then(([iconLocations]) => {
      iconLocations
        .features
        .filter(icon => icon.geometry.coordinates[2] === location.z) // Only show icons for the current plane
        .forEach((icon) => {
        const { geometry, properties } = icon;
        if (geometry.type !== 'Point') {
          console.warn('Got a map icon location that is not a point.', icon);
          return;
        }

        // Coordinates are flipped
        const marker = leaflet.marker([geometry.coordinates[1], geometry.coordinates[0]], {
          icon: iconSet[properties.icon].markerIcon,
          // interactive: false,
          title: iconSet[properties.icon].name
        });
        marker.addTo(this.map);
      });
    });

    const map = this.map;
    map.setView(location.toLatLng(), 3);

    leaflet.tileLayer(tileUrl, {
      bounds: data.baseMaps[0].bounds
      // tms: true
    }).addTo(map);


    this.playerIndicator = leaflet.marker(location.toLatLng(), { icon: playerIcon }).addTo(map);

    if (this.props.subscribeToLocationChanges) {
      this.unsubscribeFromLocationChanges = this.props.subscribeToLocationChanges();
    }
  }

  static translateBounds (bounds: Array<Array<number>>): LatLngBoundsExpression {
    const newBounds = [[0, 0], [12000, 12000]];
    if (Array.isArray(bounds) && bounds.length === 2) {
      // South-West
      if (Array.isArray(bounds[0]) && bounds[0].length === 2) {
        newBounds[0][0] = bounds[0][1];
        newBounds[0][1] = bounds[0][0];
      }
    // North-East
      if (Array.isArray(bounds[1]) && bounds[1].length === 2) {
        newBounds[1][0] = bounds[1][1];
        newBounds[1][1] = bounds[1][0];
      }
    }
    return new leaflet.LatLngBounds(newBounds[0] as LatLngExpression, newBounds[1] as LatLngExpression);
  }

  componentDidUpdate (prevProps: MapProps) {
    const oldPosition = new Position(prevProps.playerLocation || Map.DEFAULT_LOCATION);
    const newPosition = new Position(this.props.playerLocation || Map.DEFAULT_LOCATION);
    if (oldPosition.equals(newPosition)) return;

    console.log('Moving map to location', newPosition);

    const latLong: LatLngTuple = [newPosition.y, newPosition.x];
    this.playerIndicator.setLatLng(latLong);
    this.map.panTo(latLong);
  }

  componentWillUnmount () {
    if (this.unsubscribeFromLocationChanges) {
      this.unsubscribeFromLocationChanges();
    }
  }

  render () {
    let { playerLocation, subscribeToLocationChanges, ...props } = this.props;

    console.log('Rendering map');

    return (
      <div id="mapid" style={{ height: 250, width: 250 }} {...props} />
    );
  }
}

export default Map;