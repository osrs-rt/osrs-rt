import React, { FunctionComponent, useRef, useState, useEffect } from 'react';
// import XpNotification from './XpNotification';
import uniqueId from 'lodash/uniqueId';
// import subscription from '../queries/SubscribeToXpChanged.graphql';
// import { Subscription } from 'react-apollo';
import {
  AllSkillNames,
  XpChangeEvent,
  XpDictionary
} from '../utils/sharedTypes';
import { usePlayerXp } from './PlayerXpContext';
import { makeStyles } from '@material-ui/core';
import XpNotification from './XpNotification';
// import { XpChangedSubscriptionEvent } from './PlayerXpContext';

const useStyles = makeStyles({
  root: {
    position: 'fixed',
    top: 70,
    right: 70,
    fontSize: '20px',
    zIndex: 1,
  }
});

export interface XpNotificationAreaProps {}

interface XpGainEvent {
  id: string;
  gains: Array<XpChangeEvent>;
}

function getXpDifference(
  prevXp: XpDictionary,
  currentXp: XpDictionary
): Array<XpChangeEvent> {
  return Object.entries(currentXp)
    .filter(([skill]) => skill !== 'overall')
    .reduce((gains, [skill, currentXp]) => {
      const previousXp = prevXp[skill as AllSkillNames];
      if (!previousXp || !currentXp || currentXp === previousXp) return gains;

      gains.push({
        skill: skill as AllSkillNames,
        xpGained: currentXp - previousXp,
        xpTotal: currentXp
      });
      return gains;
    }, [] as Array<XpChangeEvent>);
}

const XpNotificationArea: FunctionComponent<XpNotificationAreaProps> = () => {
  const [xpChangeEvents, setXpChangeEvents] = useState<Array<XpGainEvent>>([]);
  const previousXp = useRef<XpDictionary>({});
  const currentXp = usePlayerXp();
  const classes = useStyles();

  const onAnimationFinished = (id: string) => {
    setXpChangeEvents(curState => {
      const index = curState.findIndex(xp => xp.id === id);
      if (index < 0) return curState;

      const newXpGains = [...curState];
      newXpGains.splice(index, 1);
      return newXpGains;
    });
  };

  useEffect(() => {
    const diffs = getXpDifference(previousXp.current, currentXp);
    previousXp.current = currentXp;
    if (diffs.length === 0) return;

    const event: XpGainEvent = {
      id: uniqueId('xp_gain_'),
      gains: diffs
    };
    setXpChangeEvents(state => [...state, event]);
  }, [currentXp]);

  return (
    <div className={classes.root}>
      {xpChangeEvents.map(({ id, gains }) => {
        return (
          <XpNotification
            key={id}
            xpChanges={gains}
            onAnimationComplete={() => {
              onAnimationFinished(id);
            }}
          />
        );
      })}
    </div>
  );
};

export default XpNotificationArea;
