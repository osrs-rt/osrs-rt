import React, { Component, CSSProperties } from 'react';
import query from '../queries/GetLocation.graphql';
import subscription from '../queries/SubscribeToLocationChanged.graphql';
import { Query } from 'react-apollo';
import Map from './LazyLoadedMap';
import { Location } from './Map';

interface PlayerLocation {
  playerLocation: Location
}

interface PlayerLocationChanged {
  data: { playerLocationChanged: { location: Location }};
}

interface MapContainerProps {
  username: string;
  style?: CSSProperties
}

class MapContainer extends Component<MapContainerProps> {
  render() {
    const { username, ...containerProps } = this.props;

    if (!username) return null;

    return (
      <Query<PlayerLocation> query={query} variables={{ playerId: username }}>
        {({ data, subscribeToMore, loading }) => {
          const subscribeToLocationChanges = () => {
            console.log('Subscribing to player location changes');

            const unsubscribe = subscribeToMore({
              variables: {
                playerId: username
              },
              document: subscription,
              updateQuery(prev, { subscriptionData }: { subscriptionData: PlayerLocationChanged }) {
                console.log('New location data received', subscriptionData);
                if (
                  !subscriptionData.data ||
                  !subscriptionData.data.playerLocationChanged
                )
                  return prev;
                return { playerLocation: subscriptionData.data.playerLocationChanged.location };
              }
            });

            return () => {
              console.log('Unsubscribing from from player location changes');
              unsubscribe();
            }
          };

          console.log('Map data is ', data);
          return (
            <Map
              loading={loading}
              playerLocation={data && (data as PlayerLocation).playerLocation}
              subscribeToLocationChanges={subscribeToLocationChanges}
              {...containerProps}
            />
          );
        }}
      </Query>
    );
  }
}

export default MapContainer;