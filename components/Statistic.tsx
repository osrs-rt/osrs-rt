import React, { ReactNode, FunctionComponent } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Number from './Number';
import { styled } from '@material-ui/core';

type PropTypes = {
  text: ReactNode;
  value: number | ReactNode;
  unit?: ReactNode;
};

const Unit = styled(Typography)({
  marginLeft: '0.25em'
});

const Statistic: FunctionComponent<PropTypes> = props => {
  const value =
    typeof props.value === 'number' ? (
      <Number value={props.value} showZero={true} />
    ) : (
      props.value
    );

  return (
    <Card>
      <CardContent>
        <Typography color="textSecondary" gutterBottom>
          {props.text}
        </Typography>

        <Typography variant="h4" display="inline">
          {value}
        </Typography>
        {props.unit && (
          <Unit variant="h5" color="textSecondary" display="inline">
            {props.unit}
          </Unit>
        )}
      </CardContent>
    </Card>
  );
};

export default Statistic;
