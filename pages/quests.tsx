import React, { Fragment } from 'react';
import Statistic from '../components/Statistic';
import { Query, QueryResult } from 'react-apollo';
import getQuests from '../queries/GetQuests.graphql';
import sumBy from 'lodash/sumBy';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Grid from '@material-ui/core/Grid';
import withStyles from '@material-ui/core/styles/withStyles';
import grey from '@material-ui/core/colors/grey';
import yellow from '@material-ui/core/colors/yellow';
import ExternalLink from '../components/ExternalLink';
import { makeStyles, Paper } from '@material-ui/core';
import { Theme } from '../utils/muiTheme';
import PageHeader from '../components/PageHeader';
import PageContent from '../components/PageContent';
import PageFooter from '../components/PageFooter';

interface PropTypes {
  username: string;
  error?: Error;
}

type QuestQueryResult = QueryResult<{ quests: Array<Quest> }>;

interface Quest {
  id: string;
  name: string;
  number: number;
  status: string; // TODO: SHARE WITH SERVER = REAL VALUES
  questPoints: number;
  url: string;
  subQuest?: boolean;
}

const useStyles = makeStyles((theme: Theme) => ({
  'row-not_started': {
    // backgroundColor: 'red'
  },
  'row-finished': {
    backgroundColor: grey[200],
    textDecoration: 'line-through',
    color: grey[500]
  },
  'row-in_progress': {
    backgroundColor: yellow[200]
  },
  table: {
    marginTop: theme.spacing(2)
  }
}));

const CustomTableCell = withStyles({
  body: {
    color: 'inherit'
  }
})(TableCell);

function QuestsPage(props: PropTypes) {
  // TODO: HANDLE NOT LOGGED IN
  const { username } = props;
  const classes = useStyles();

  return (
    <Fragment>
      <PageHeader text={QuestsPage.title || ''} username={username} />

      <PageContent error={props.error}>
        <Query query={getQuests} variables={{ playerId: props.username }}>
          {(props: QuestQueryResult) => {
            // TODO: HIDE STATS WHEN NOT LOGGED IN
            const quests = (props.data && props.data.quests) || [];
            const mainQuests = quests.filter(q => !q.subQuest);
            const finishedQuests = mainQuests.filter(
              q => q.status === 'FINISHED'
            );
            const questPointsCompleted = sumBy(
              finishedQuests,
              q => q.questPoints
            );
            const totalQuests = mainQuests.length;

            return (
              <React.Fragment>
                <Grid container spacing={2}>
                  <Grid item xs={6} md={6}>
                    <Statistic
                      text="Quest Points"
                      value={questPointsCompleted}
                    />
                  </Grid>
                  <Grid item xs={6} md={6}>
                    <Statistic
                      text="Completed Quests"
                      value={`${finishedQuests.length} / ${totalQuests}`}
                    />
                  </Grid>
                </Grid>

                <Paper className={classes.table}>
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell align="center">#</TableCell>
                        <TableCell>Quest</TableCell>
                        <TableCell>Quest Points</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {mainQuests.map(quest => {
                        // prettier-ignore
                        // @ts-ignore
                        const className = quest.status && classes[`row-${quest.status.toLowerCase()}`];
                        return (
                          <TableRow key={quest.id} className={className}>
                            <CustomTableCell align="center">
                              {quest.number}
                            </CustomTableCell>
                            <CustomTableCell>
                              <ExternalLink href={quest.url}>
                                {quest.name}
                              </ExternalLink>
                            </CustomTableCell>
                            <CustomTableCell>
                              {quest.questPoints}
                            </CustomTableCell>
                          </TableRow>
                        );
                      })}
                    </TableBody>
                  </Table>
                </Paper>
              </React.Fragment>
            );
          }}
        </Query>
      </PageContent>

      <PageFooter />
    </Fragment>
  );
}

QuestsPage.title = 'Quests';

export default QuestsPage;
