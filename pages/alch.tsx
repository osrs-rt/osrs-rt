import React, { Fragment, useState } from 'react';
import getAlchables from '../queries/AlchableItems.graphql';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import sortBy from 'lodash/sortBy';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Paper } from '@material-ui/core';
import Number from '../components/Number';
import Grid from '@material-ui/core/Grid';
import Statistic from '../components/Statistic';
import { Theme } from '../utils/muiTheme';
import { makeStyles } from '@material-ui/styles';
import HelpIcon from '@material-ui/icons/Help';
import Tooltip from '@material-ui/core/Tooltip';
import PageHeader from '../components/PageHeader';
import PageContent from '../components/PageContent';
import PageFooter from '../components/PageFooter';
import green from '@material-ui/core/colors/green';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

const useStyles = makeStyles((theme: Theme) => {
  return {
    stats: {
      marginBottom: theme.spacing(2)
    },
    profit: {
      fontWeight: theme.typography.fontWeightMedium,
      color: green['300']
    }
  };
});

type PropTypes = {
  username?: string;
  error?: Error;
};

const AlchPage = (props: PropTypes) => {
  const styles = useStyles();
  const [f2pOnly, setF2pOnly] = useState<boolean>(false);
  const { loading, error, data } = useQuery<{ items: Array<any> }>(
    getAlchables,
    {
      variables: {
        members: f2pOnly ? false : null // Only limit to non-members if we only want f2p items. Show both if not.
      }
    }
  );
  const natureResult = useQuery<{ item: any }>(gql`
    query {
      item(id: 561) {
        price {
          runelite
        }
      }
    }
  `);

  function getContent() {
    if (loading || natureResult.loading) return <span>Loading</span>;
    if (error || natureResult.error) return <span>Error</span>;
    if (!data || !data.items || !natureResult.data || !natureResult.data.item)
      return <span>No Data</span>;

    const profitableItems = data.items
      .filter(x => x.price.runelite > 0)
      .map(x => {
        const profit =
          x.price.highalch -
          x.price.runelite -
          natureResult!.data!.item!.price.runelite;

        const maxAlchedIn4Hours = Math.min(x.buy_limit, 1200 * 4);
        const upfrontCapital =
          maxAlchedIn4Hours * x.price.runelite +
          maxAlchedIn4Hours * natureResult!.data!.item!.price!.runelite;

        return {
          ...x,
          profit,
          profitPer4Hours: profit * maxAlchedIn4Hours,
          capital: upfrontCapital
        };
      })
      .filter(i => i.profit >= 0);
    const sortedItems = sortBy(
      profitableItems,
      x => x.profitPer4Hours
    ).reverse();

    return (
      <Fragment>
        <Grid container spacing={2} className={styles.stats}>
          <Grid item xs={6} md={9}>
            <Statistic
              text="Cost of nature rune"
              value={natureResult.data.item.price.runelite}
            />
          </Grid>
          <Grid item xs={6} md={3}>
            <Statistic
              text="Options"
              value={
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={f2pOnly}
                        onChange={() => {
                          setF2pOnly(current => !current);
                        }}
                      />
                    }
                    label="F2P only"
                  />
                </FormGroup>
              }
            />
          </Grid>
        </Grid>

        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Item</TableCell>
                <TableCell>GE Price</TableCell>
                <TableCell>High Alch Value</TableCell>
                <TableCell>Buy Limit</TableCell>
                <TableCell>Profit</TableCell>
                <TableCell>
                  Profit Per 4 Hours
                  <Tooltip title="Maximum number of alchs per 4 hours (min of either 4800 or the buy limit) x profit">
                    <HelpIcon />
                  </Tooltip>
                </TableCell>

                <TableCell>
                  Upfront Capital
                  <Tooltip title="Maximum number of alchs per 4 hours (min of either 4800 or the buy limit) x (item cost + nature rune cost)">
                    <HelpIcon />
                  </Tooltip>
                </TableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {sortedItems.map(i => {
                return (
                  <TableRow key={i.id}>
                    <TableCell>{i.name}</TableCell>
                    <TableCell>
                      <Number value={i.price.runelite} />
                    </TableCell>
                    <TableCell>
                      <Number value={i.price.highalch} />
                    </TableCell>
                    <TableCell>
                      <Number value={i.buy_limit} />
                    </TableCell>
                    <TableCell className={styles.profit}>
                      <Number value={i.profit} />
                    </TableCell>
                    <TableCell>
                      <Number value={i.profitPer4Hours} />
                    </TableCell>
                    <TableCell>
                      <Number value={i.capital} />
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Paper>
      </Fragment>
    );
  }

  return (
    <Fragment>
      <PageHeader text={AlchPage.title || ''} username={props.username} />

      <PageContent error={props.error}>{getContent()}</PageContent>

      <PageFooter />
    </Fragment>
  );
};

AlchPage.title = 'Alchemy';

export default AlchPage;
