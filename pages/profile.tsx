import React, { ChangeEvent, Fragment, useState } from 'react';
import Button from '@material-ui/core/Button';
import { Input } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import PageHeader from '../components/PageHeader';
import PageContent from '../components/PageContent';
import PageFooter from '../components/PageFooter';
import { useRouter } from 'next/router';
import { MyAppPage } from '../utils/sharedTypes';

const Profile2: MyAppPage = props => {
  const [usernameInput, setUsernameInput] = useState(props.username || '');
  const router = useRouter();

  const updateUsername = (e: ChangeEvent<HTMLInputElement>) => {
    setUsernameInput(e.target.value);
  };

  const onSetClick = () => {
    props.pageContext.setUsername(usernameInput);
    router.push('/dashboard');
  };

  return (
    <Fragment>
      <PageHeader text={Profile2.title} username={props.username} />

      <PageContent error={props.error}>
        <FormControl>
          <InputLabel shrink={true} htmlFor="osrs-username">
            OSRS Username
          </InputLabel>
          <Input
            id="osrs-username"
            value={usernameInput}
            onChange={updateUsername}
            endAdornment={<Button onClick={onSetClick}>Set</Button>}
          />
        </FormControl>
      </PageContent>

      <PageFooter />
    </Fragment>
  );
};

Profile2.title = 'Profile';

export default Profile2;
