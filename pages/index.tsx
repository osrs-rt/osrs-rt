import React, { useEffect } from 'react';
import LoggedOutIndex from '../components/LoggedOutIndex';
import { MyAppPage } from '../utils/sharedTypes';
import { useRouter } from 'next/router';

const Home: MyAppPage = ({ username, pageContext, error }) => {
  const router = useRouter();

  useEffect(() => {
    if (username) {
      router.push('/dashboard');
    }
  });

  if (username) {
    return null;
  }

  return (
    <LoggedOutIndex
      pageContext={pageContext}
      username={username}
      error={error}
    />
  );
};

Home.title = process.env.secret_mode ? '' : 'Old School Real-time Companion';

export default Home;
