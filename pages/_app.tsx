import App, { AppProps, AppContext } from 'next/app';
import React  from 'react';
import withApolloClient, { ApolloClientProps } from '../utils/withApolloClient';
import { ApolloProvider } from 'react-apollo';
import { ThemeProvider } from '@material-ui/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import getPageContext from '../utils/getPageContext';
import Head from '../components/head';
import cookies from 'next-cookies';
import { XpProvider } from '../components/PlayerXpContext';
import Cookies from 'js-cookie';
import muiTheme from '../utils/muiTheme';
import { NextComponentType } from 'next';

export interface PageContext {
  setUsername: (username: string) => void;
}

interface MyAppState {
  username?: string;
}

interface MyAppProps extends AppProps, ApolloClientProps {
  pageContext?: PageContext;
  username?: string;
  error?: Error;
  Component: NextComponentType & { title?: string };
}

class MyApp extends App<MyAppProps> {
  private readonly pageContext: PageContext;

  constructor(props: MyAppProps) {
    super(props);

    this.pageContext = getPageContext();
    // TODO: THESE SHOULD USE THE CONTEXT?
    this.pageContext.setUsername = this._setUsername;
    this.state = {
      username: props.pageProps.username
    };
  }

  static async getInitialProps({ Component, router, ctx }: AppContext) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    const cookie = cookies(ctx);
    const username = router.query.username || cookie.username;

    const isSsr = ctx.req != null;
    return {
      pageProps,
      username,
      isSsr
    };
  }

  _setUsername = (username: string) => {
    Cookies.set('username', username, { expires: 365 });

    this.setState({
      username
    });
  };

  componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  render() {
    const { Component, pageProps, apolloClient } = this.props;
    const { username }: MyAppState = this.state;

    return (
      <ApolloProvider client={apolloClient}>
        <ThemeProvider theme={muiTheme}>
          <CssBaseline />

          <Head title={Component.title || ''} />

          <div
            style={{
              minHeight: '100vh',
              display: 'flex',
              flexDirection: 'column'
            }}
          >
            <XpProvider username={username}>
              <Component
                pageContext={this.pageContext}
                {...pageProps}
                username={username}
              />
            </XpProvider>
          </div>
        </ThemeProvider>
      </ApolloProvider>
    );
  }
}

export default withApolloClient(MyApp);
