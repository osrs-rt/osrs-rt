import React, { useMemo } from 'react';
import PageHeader from '../components/PageHeader';
import PageContent from '../components/PageContent';
import PageFooter from '../components/PageFooter';
import { useQuery } from '@apollo/react-hooks';
import bankQuery from '../queries/GetBank.graphql';
import { ContainerItemQuery } from '../utils/sharedTypes';
import {
  DataGrid,
  ColDef,
  CellParams,
  AutoSizer
} from '@material-ui/data-grid';
import Number from '../components/Number';
import ItemImage from '../components/ItemImage';
import {
  Card,
  Hidden,
  makeStyles,
  Typography,
  useTheme
} from '@material-ui/core';
import { FixedSizeList } from 'react-window';

const NumberCell = (props: CellParams) => {
  return <Number value={props.value as number} />;
};

const useStyles = makeStyles(theme => {
  return {
    cardWrapper: {
      padding: theme.spacing(1, 0)
    },
    cardRoot: {
      display: 'flex'
      // margin: theme.spacing(1, 0)
    },
    iconWrapper: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      padding: theme.spacing(1)
    },
    cardContent: {
      display: 'flex',
      alignItems: 'center',
      flexGrow: 1
    },
    cardItemName: {
      flexGrow: 1,
      fontWeight: theme.typography.fontWeightMedium
    },
    cardItemValue: {
      color: theme.palette.primary.contrastText,
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(1, 2, 1, 4),
      position: 'relative',
      marginLeft: 20,
      height: '100%',

      '& > span': {
        zIndex: 1
      },

      '&::before': {
        background: theme.palette.primary.light,
        clipPath: 'polygon(20px 0px, 100% 0px, 100% 100%, 0 100%)',
        content: '""',
        height: '100%',
        width: '100%',
        position: 'absolute',
        zIndex: 0,
        left: 0
      },

      '&::after': {
        content: '"TOTAL GP"',
        fontSize: '0.5rem',
        transform: 'rotate(-72deg)',
        position: 'absolute',
        left: -20
      }
    }
  };
});

const columns: ColDef[] = [
  { field: 'index', hide: true, sortDirection: 'asc' },
  {
    field: 'name',
    headerName: 'Item',
    width: 250,
    renderCell: cell => {
      // TODO: USE IMAGE FROM SERVER INSETAD OF EXTERNAL
      return (
        <>
          <ItemImage
            itemId={cell.row.id as number}
            itemName={cell.value as string}
          />
          {cell.value}
        </>
      );
    }
  },
  {
    field: 'quantity',
    headerName: 'Quantity',
    renderCell: NumberCell
  },
  {
    headerName: 'Value',
    field: 'value',
    renderCell: NumberCell
    // sortDirection: 'desc'
  }
];

const BankPage = (props: { username: string; error?: Error }) => {
  const styles = useStyles();
  const { username } = props;
  const { data, loading } = useQuery<{ playerBank: Array<ContainerItemQuery> }>(
    bankQuery,
    {
      variables: {
        playerId: username
      }
    }
  );
  const items = useMemo(() => {
    return (data?.playerBank || []).map(({ quantity, item, index }) => {
      return {
        id: item.id,
        name: item.name,
        index,
        quantity,
        value: item?.price?.runelite
          ? (quantity || 1) * item?.price?.runelite
          : undefined
      };
    });
  }, [data]);

  const theme = useTheme();

  return (
    <>
      <PageHeader text={BankPage.title || ''} username={username} />

      <PageContent error={props.error}>
        <Hidden smDown>
          <DataGrid columns={columns} rows={items} loading={loading} />
        </Hidden>
        <Hidden mdUp>
          <AutoSizer>
            {({ height, width }) => (
              <FixedSizeList
                height={height}
                itemCount={items.length}
                itemSize={64 + theme.spacing(2)}
                width={width}
                overscanCount={5}
              >
                {({ index, style }) => {
                  const { id, name, value } = items[index];
                  return (
                    <div className={styles.cardWrapper} style={style}>
                      <Card key={id} className={styles.cardRoot}>
                        <div className={styles.iconWrapper}>
                          <ItemImage
                            itemId={id}
                            itemName={name}
                            size={'large'}
                          />
                        </div>
                        <div className={styles.cardContent}>
                          <Typography
                            variant="body1"
                            className={styles.cardItemName}
                          >
                            {name}
                          </Typography>
                          <Typography
                            variant="h6"
                            className={styles.cardItemValue}
                          >
                            <Number value={value} />
                          </Typography>
                        </div>
                      </Card>
                    </div>
                  );
                }}
              </FixedSizeList>
            )}
          </AutoSizer>
        </Hidden>
      </PageContent>

      <PageFooter />
    </>
  );
};
BankPage.title = 'Bank';

export default BankPage;
