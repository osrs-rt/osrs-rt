import React, { useState } from 'react';
import { XpConsumer } from '../components/PlayerXpContext';
import { constants } from 'osrs-api';
import {
  getExperienceForLevel,
  getLevelForExperience
} from '../sharedCode/experience';
import { Box, Typography } from '@material-ui/core';
import startCase from 'lodash/startCase';
import ColouredLinearProgress from '../components/ColouredLinearProgress';
import * as skillColours from '../utils/skillColours';
import SkillIcon from '../components/SkillIcon';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import {
  usePopupState,
  bindTrigger,
  bindMenu
  // @ts-ignore
} from 'material-ui-popup-state/hooks';
import PageHeader from '../components/PageHeader';
import { MyAppPage } from '../utils/sharedTypes';
import PageContent from '../components/PageContent';
import { makeStyles } from '@material-ui/core/styles';
import PageFooter from '../components/PageFooter';

enum VisibleProgressType {
  NextLevel = 'NextLevel',
  MaxLevel = 'MaxLevel',
  MaxXp = 'MaxXp'
}

function calculatePercent(
  currentXp: number,
  progressType: VisibleProgressType
) {
  const currentLevel = getLevelForExperience(currentXp);

  switch (progressType) {
    case VisibleProgressType.NextLevel:
      if (currentLevel === 99) return 100;
      const currentLevelStartXp = getExperienceForLevel(currentLevel);
      const nextXp = getExperienceForLevel(currentLevel + 1);
      return (
        ((currentXp - currentLevelStartXp) * 100) /
        (nextXp - currentLevelStartXp)
      );
    case VisibleProgressType.MaxLevel:
      if (currentLevel === 99) return 100;
      const xpFor99 = getExperienceForLevel(99);
      return (currentXp / xpFor99) * 100;
    case VisibleProgressType.MaxXp:
      return (currentXp / 200000000) * 100;
  }
}

const useStyles = makeStyles(theme => ({
  skillRoot: {
    flex: '0 0 100%',
    margin: theme.spacing(0.5, 0),

    [theme.breakpoints.up('sm')]: {
      flexBasis: '50%',
      padding: theme.spacing(0, 2),
    },

    [theme.breakpoints.up('md')]: {
      flexBasis: '33%'
    }
  },

  skillsListContainer: {
    display: 'flex',
    flexWrap: 'wrap',
  }
}));

const SkillsPage: MyAppPage = props => {
  const [progressTo, setProgressTo] = useState<VisibleProgressType>(
    VisibleProgressType.NextLevel
  );

  const popupState = usePopupState({
    variant: 'popover',
    popupId: 'targetPopup'
  });

  const styles = useStyles();

  return (
    <React.Fragment>
      <PageHeader text={SkillsPage.title} username={props.username} />

      <PageContent error={props.error}>
        <XpConsumer>
          {skillsXp => {
            if (!skillsXp) return '';

            // TODO: ONLY IN THAT ORDER IN A 3 COLUMN LAYOUT...
            // FOR A 1-COLUMN LAYOUT IT'S PROBABLY BETTER TO GOWN DOWN THE COLUMNS FIRST? (i.e COMBAT SKILLS ARE SEQUENTIAL)
            const allFields = constants.ingameSkillOrder
              .filter(skill => skill !== 'overall')
              .map((skill, index) => {
                const currentXp = skillsXp[skill] || 0;
                const percent = calculatePercent(currentXp, progressTo);
                // @ts-ignore
                const colour: string = skillColours[skill.toUpperCase()];

                const currentLevel = getLevelForExperience(currentXp);
                const nextLevel = Math.min(currentLevel + 1, 99);

                const targetText =
                  progressTo === VisibleProgressType.NextLevel
                    ? `${currentLevel} to ${nextLevel}`
                    : progressTo === VisibleProgressType.MaxLevel
                    ? 'to lvl 99'
                    : 'to 200m xp';

                return (
                  <div key={skill} className={styles.skillRoot}>
                    <Typography variant="subtitle1">
                      <Box display="flex" alignItems="center">
                        <SkillIcon skill={skill} />
                        <span>{startCase(skill)}</span>
                      </Box>
                    </Typography>
                    <ColouredLinearProgress
                      color={colour}
                      backgroundColor="rgba(0,0,0,0.15)"
                      variant="determinate"
                      value={percent}
                    />
                    <Box display="flex" justifyContent="space-between">
                      <Typography variant="caption" color="textSecondary">
                        {percent.toPrecision(3)}%
                      </Typography>

                      <Typography variant="caption" color="textSecondary">
                        {targetText}
                      </Typography>
                    </Box>
                  </div>
                );
              });

            const targetTypeTexts = {
              [VisibleProgressType.NextLevel]: 'THE NEXT LEVEL',
              [VisibleProgressType.MaxLevel]: 'LEVEL 99',
              [VisibleProgressType.MaxXp]: '200M XP'
            };
            const targetTypeText = targetTypeTexts[progressTo];

            return (
              <React.Fragment>
                <div>
                  Show progress to{' '}
                  <Button {...bindTrigger(popupState)}>{targetTypeText}</Button>
                </div>
                <Menu {...bindMenu(popupState)}>
                  {Object.entries(targetTypeTexts).map(([type, text]) => {
                    return (
                      <MenuItem
                        key={type}
                        selected={progressTo === type}
                        onClick={() => {
                          popupState.close();
                          setProgressTo(type as VisibleProgressType);
                        }}
                      >
                        {text}
                      </MenuItem>
                    );
                  })}
                </Menu>

                <div className={styles.skillsListContainer}>{allFields}</div>
              </React.Fragment>
            );
          }}
        </XpConsumer>
      </PageContent>

      <PageFooter />
    </React.Fragment>
  );
};

SkillsPage.title = 'Hiscores';

export default SkillsPage;
