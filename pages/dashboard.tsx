import React, { Fragment, useEffect } from 'react';
import PageHeader from '../components/PageHeader';
import PageContent from '../components/PageContent';
import PageFooter from '../components/PageFooter';
import { MyAppPage } from '../utils/sharedTypes';
import { useRouter } from 'next/router';
import DashboardStatisticContainer from '../components/DashboardStatisticContainer';

const Dashboard: MyAppPage = ({ username, error }) => {
  const router = useRouter();

  useEffect(() => {
    if (!username) {
      router.push('/');
    }
  }, [username]);

  if (!username) return null;

  return (
    <Fragment>
      <PageHeader text={Dashboard.title} username={username} />
      <PageContent error={error}>
        <DashboardStatisticContainer playerId={username} />

        {/*<MapContainer*/}
        {/*  username={username}*/}
        {/*  style={{*/}
        {/*    height: 250,*/}
        {/*    width: 250,*/}
        {/*    borderRadius: '50%',*/}
        {/*    border: '1px solid #ffc107',*/}
        {/*    boxShadow: '0 0 0 1px #616161'*/}
        {/*  }}*/}
        {/*/>*/}
      </PageContent>

      <PageFooter />
    </Fragment>
  )
};

Dashboard.title = 'Dashboard';

export default Dashboard