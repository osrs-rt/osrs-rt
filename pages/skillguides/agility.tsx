import React from 'react';
import Number from '../../components/Number';
import Card from '@material-ui/core/Card/index';
import CardContent from '@material-ui/core/CardContent/index';
import Typography from '@material-ui/core/Typography/index';
import { formatTime } from '../../utils/stringFormatters';
import { makeStyles } from '@material-ui/styles';
import { agility } from '../../utils/skillDatabase';
import Table from '@material-ui/core/Table/index';
import TableRow from '@material-ui/core/TableRow/index';
import TableCell from '@material-ui/core/TableCell/index';
import TableHead from '@material-ui/core/TableHead/index';
import TableBody from '@material-ui/core/TableBody/index';
import XpToLevelReport from '../../components/XpToLevelReport';
import { Theme } from '../../utils/muiTheme';
import PageHeader from '../../components/PageHeader';
import { MyAppPage } from '../../utils/sharedTypes';
import PageContent from '../../components/PageContent';
import PageFooter from '../../components/PageFooter';

const useStyles = makeStyles<Theme>(theme => {
  return {
    container: {
      display: 'flex',
      flexWrap: 'wrap'
    },
    label: {
      margin: 'auto 1em auto 0'
    },
    card: {
      '& + &': {
        marginTop: theme.spacing(2)
      },
      overflowX: 'auto'
    }
  };
});

const AgilityPage: MyAppPage = props => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <PageHeader text={AgilityPage.title} username={props.username} />

      <PageContent error={props.error}>
        <XpToLevelReport skill={'agility'}>
          {({ fromXp, toXp, fromLevel }) => {
            return (
              <div className={''}>
                <Card className={classes.card}>
                  <CardContent>
                    <Typography color="textSecondary" gutterBottom>
                      Laps Required
                    </Typography>

                    <Table>
                      <TableHead>
                        <TableRow>
                          <TableCell>Course</TableCell>
                          <TableCell>Required Level</TableCell>
                          <TableCell>Laps</TableCell>
                          <TableCell>Approx Time</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {agility.map(course => {
                          const lapsRequired = Math.ceil(
                            (toXp - fromXp) / course.xpPerLap
                          );
                          const timeRequired =
                            (toXp - fromXp) / course.approxXpPerHour;
                          const canUseCourse =
                            fromLevel >= course.requiredLevel;
                          return (
                            <TableRow key={course.name}>
                              <TableCell>
                                <Typography
                                  color={
                                    canUseCourse ? 'inherit' : 'textSecondary'
                                  }
                                >
                                  {course.name}
                                </Typography>
                              </TableCell>
                              <TableCell>
                                <Typography
                                  color={
                                    canUseCourse ? 'inherit' : 'textSecondary'
                                  }
                                >
                                  {course.requiredLevel}
                                </Typography>
                              </TableCell>
                              <TableCell>
                                <Typography
                                  color={
                                    canUseCourse ? 'inherit' : 'textSecondary'
                                  }
                                >
                                  <Number value={lapsRequired} />
                                </Typography>
                              </TableCell>
                              <TableCell>
                                <Typography
                                  color={
                                    canUseCourse ? 'inherit' : 'textSecondary'
                                  }
                                >
                                  {formatTime(timeRequired * 3600)}
                                </Typography>
                              </TableCell>
                            </TableRow>
                          );
                        })}
                      </TableBody>
                    </Table>
                  </CardContent>
                </Card>
              </div>
            );
          }}
        </XpToLevelReport>
      </PageContent>

      <PageFooter />
    </React.Fragment>
  );
};

AgilityPage.title = 'Agility';

export default AgilityPage;
