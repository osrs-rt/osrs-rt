import React from 'react';
import Number from '../../components/Number';
import Card from '@material-ui/core/Card/index';
import CardContent from '@material-ui/core/CardContent/index';
import Typography from '@material-ui/core/Typography/index';
import magicDB from '../../databases/magic.json';
import Table from '@material-ui/core/Table/index';
import TableRow from '@material-ui/core/TableRow/index';
import TableCell from '@material-ui/core/TableCell/index';
import TableHead from '@material-ui/core/TableHead/index';
import TableBody from '@material-ui/core/TableBody/index';
import XpToLevelReport from '../../components/XpToLevelReport';
import { makeStyles } from '@material-ui/styles';
import { Theme } from '../../utils/muiTheme';
import { MyAppPage } from '../../utils/sharedTypes';
import PageHeader from '../../components/PageHeader';
import PageContent from '../../components/PageContent';
import PageFooter from '../../components/PageFooter';

const useStyles = makeStyles<Theme>(theme => {
  return {
    container: {
      display: 'flex',
      flexWrap: 'wrap'
    },
    label: {
      margin: 'auto 1em auto 0'
    },
    card: {
      '& + &': {
        marginTop: theme.spacing(2)
      },
      overflowX: 'auto'
    }
  };
});

const MagicPage: MyAppPage = (props) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <PageHeader text={MagicPage.title} username={props.username} />

      <PageContent error={props.error}>
        <XpToLevelReport skill={'magic'}>
          {({ fromXp, toXp, fromLevel }) => {
            return (
              <div className={''}>
                <Card className={classes.card}>
                  <CardContent>
                    <Typography color="textSecondary" gutterBottom>
                      Items
                    </Typography>

                    <Table>
                      <TableHead>
                        <TableRow>
                          <TableCell>Spell</TableCell>
                          <TableCell>Required level</TableCell>
                          <TableCell>XP</TableCell>
                          <TableCell>Casts required</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {magicDB.map(spell => {
                          const canUseMethod =
                            fromLevel >= spell.requiredLevel;
                          const actionsRequired =
                            spell.xp === 0
                              ? null
                              : Math.ceil((toXp - fromXp) / spell.xp);

                          return (
                            <TableRow key={spell.spell}>
                              <TableCell>
                                <Typography
                                  color={
                                    canUseMethod ? 'inherit' : 'textSecondary'
                                  }
                                >
                                  {spell.spell}
                                </Typography>
                              </TableCell>
                              <TableCell>
                                <Typography
                                  color={
                                    canUseMethod ? 'inherit' : 'textSecondary'
                                  }
                                >
                                  {spell.requiredLevel}
                                </Typography>
                              </TableCell>
                              <TableCell>
                                <Typography
                                  color={
                                    canUseMethod ? 'inherit' : 'textSecondary'
                                  }
                                >
                                  <Number value={spell.xp} />
                                </Typography>
                              </TableCell>
                              <TableCell>
                                <Typography
                                  color={
                                    canUseMethod ? 'inherit' : 'textSecondary'
                                  }
                                >
                                  {actionsRequired == null ? (
                                    'N/A'
                                  ) : (
                                    <Number value={actionsRequired} />
                                  )}
                                </Typography>
                              </TableCell>
                            </TableRow>
                          );
                        })}
                      </TableBody>
                    </Table>
                  </CardContent>
                </Card>
              </div>
            );
          }}
        </XpToLevelReport>
      </PageContent>

      <PageFooter />
    </React.Fragment>
  );
};

MagicPage.title = 'Magic';

export default MagicPage;
