import React from 'react';
import Number from '../../components/Number';
import Card from '@material-ui/core/Card/index';
import CardContent from '@material-ui/core/CardContent/index';
import Typography from '@material-ui/core/Typography/index';
import construction from '../../databases/construction.json';
import Table from '@material-ui/core/Table/index';
import TableRow from '@material-ui/core/TableRow/index';
import TableCell from '@material-ui/core/TableCell/index';
import TableHead from '@material-ui/core/TableHead/index';
import TableBody from '@material-ui/core/TableBody/index';
import XpToLevelReport from '../../components/XpToLevelReport';
import { makeStyles } from '@material-ui/styles';
import { Theme } from '../../utils/muiTheme';
import { MyAppPage } from '../../utils/sharedTypes';
import PageHeader from '../../components/PageHeader';
import PageFooter from '../../components/PageFooter';
import PageContent from '../../components/PageContent';

const useStyles = makeStyles<Theme>(theme => {
  return {
    container: {
      display: 'flex',
      flexWrap: 'wrap'
    },
    label: {
      margin: 'auto 1em auto 0'
    },
    card: {
      '& + &': {
        marginTop: theme.spacing(2)
      },
      overflowX: 'auto'
    }
  };
});

// TODO: Make a SkillPage component and pass name and database
const ConstructionPage: MyAppPage = props => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <PageHeader text={ConstructionPage.title} username={props.username} />

      <PageContent error={props.error}>
        <XpToLevelReport skill={'construction'}>
          {({ fromXp, toXp, fromLevel }) => {
            return (
              <div className={''}>
                <Card className={classes.card}>
                  <CardContent>
                    <Typography color="textSecondary" gutterBottom>
                      Items
                    </Typography>

                    <Table>
                      <TableHead>
                        <TableRow>
                          <TableCell>Item</TableCell>
                          <TableCell>Required level</TableCell>
                          <TableCell>XP</TableCell>
                          <TableCell>Actions required</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {construction.map(method => {
                          const canUseMethod =
                            fromLevel >= method.requiredLevel;
                          const actionsRequired =
                            method.xp === 0
                              ? null
                              : Math.ceil((toXp - fromXp) / method.xp);

                          return (
                            <TableRow key={method.item}>
                              <TableCell>
                                <Typography
                                  color={
                                    canUseMethod ? 'inherit' : 'textSecondary'
                                  }
                                >
                                  {method.item}
                                </Typography>
                              </TableCell>
                              <TableCell>
                                <Typography
                                  color={
                                    canUseMethod ? 'inherit' : 'textSecondary'
                                  }
                                >
                                  {method.requiredLevel}
                                </Typography>
                              </TableCell>
                              <TableCell>
                                <Typography
                                  color={
                                    canUseMethod ? 'inherit' : 'textSecondary'
                                  }
                                >
                                  <Number value={method.xp} />
                                </Typography>
                              </TableCell>
                              <TableCell>
                                <Typography
                                  color={
                                    canUseMethod ? 'inherit' : 'textSecondary'
                                  }
                                >
                                  {actionsRequired == null ? (
                                    'N/A'
                                  ) : (
                                    <Number value={actionsRequired} />
                                  )}
                                </Typography>
                              </TableCell>
                            </TableRow>
                          );
                        })}
                      </TableBody>
                    </Table>
                  </CardContent>
                </Card>
              </div>
            );
          }}
        </XpToLevelReport>
      </PageContent>

      <PageFooter />
    </React.Fragment>
  );
};

ConstructionPage.title = 'Construction';

export default ConstructionPage;
