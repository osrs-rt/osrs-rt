import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import getQuests from '../queries/GetQuests.graphql';
// @ts-ignore
import GraphViz from 'react-graph-vis';
import NoSsr from '@material-ui/core/NoSsr';
import Head from 'next/head';

export default function() {
  const { loading, error, data } = useQuery(getQuests, {
    variables: {
      includeLinks: true
    }
  });
  if (error) {
    return <div>an error!</div>;
  }
  if (loading) return <div>loading</div>;
  // console.log(data);

  // TODO: SCALE BY NUMBER OF DEPENDANTS. MORE DEPENDANTS = MORE IMPORTANT - NEEDS INVERSE LOOKUP
  const graph = {
    nodes: data.quests.map((quest: any) => {
      return {
        id: quest.id,
        label: quest.name,
        shape: 'box',
      };
    }),
    edges: data.quests.reduce(
      (edges: Array<{ from: any; to: any }>, quest: any) => {
        // console.log(edges);
        quest.requirements.quests.forEach((innerQuest: string) => {
          edges.push({ to: quest.id, from: innerQuest });
        });
        return edges;
      },
      []
    )
  };

  console.log(graph);

  const options = {
    layout: {
      improvedLayout: true,
      hierarchical: {
        enabled: true,
        levelSeparation: 100,
        nodeSpacing: 50,
        direction: 'LR'
      }
    },
    edges: {
      color: '#000000'
    },
    // configure: true,
    physics: {
      barnesHut: {
        avoidOverlap: 0.8,
        damping: 0.5,
        springLength: 500,
        springConstant: 0.5
      },
      stabilization: true
    }
  };

  const events = {
    select: function(event: any) {
      console.log(event);
      // var { nodes, edges } = event;
    }
  };

  return (
    <NoSsr>
      <Head>
        <script src="https://unpkg.com/vis-network@5.2.3/dist/vis-network.min.js"/>
        <link href="https://unpkg.com/vis-network@5.2.3/dist/vis-network.min.css" rel="stylesheet" type="text/css" />
      </Head>
      <GraphViz
        graph={graph}
        options={options}
        events={events}
        style={{ height: '640px' }}
      />
    </NoSsr>
  );
}
