import fetch from 'cross-fetch';
import createLogger from './logger';

const logger = createLogger('osrs-tools:graphql_server:rsbuddyApiClient');

export interface RSBuddyItemsResponse {
  [itemId: string]: RSBuddyItem
}

export interface RSBuddyItem {
  id: number,
  name: string,
  members: boolean,
  sp: number, //store price
  buy_average: number,
  buy_quantity: number,
  sell_average: number,
  sell_quantity: number,
  overall_average: number,
  overall_quantity: number
}

export async function getItems(): Promise<RSBuddyItemsResponse> {
  logger.debug('Getting lit of all items from RSBuddy API');
  const response = await fetch('https://rsbuddy.com/exchange/summary.json');

  if (!response.ok) {
    const message = `Failed to get list of items from the RSBuddy. ${response.status} - ${response.statusText}`;
    logger.warn(message);
    throw new Error(message);
  }

  return response.json();
}