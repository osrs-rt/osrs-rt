import loadedEnvVars from './environment';
import RSBuddyApiDataSource from './graphql/dataSources/RSBuddyApiDataSource';
import {
  ApolloError,
  ApolloServer,
  ApolloServerExpressConfig
} from 'apollo-server-express';
import typeDefs from './graphql/schema.graphqls';
import OsrsApiDataSource from './graphql/dataSources/OsrsApiDataSource';
import CrystalMathLabsDataSource from './graphql/dataSources/CrystalMathLabsDataSource';
import express, {
  ErrorRequestHandler,
  Request,
  RequestHandler,
  Response
} from 'express';
import ApolloSentryPlugin from './ApolloSentryPlugin';
import resolvers from './graphql/resolvers';
import PlayerInformationDataSource from './graphql/dataSources/PlayerInformationDataSource';
import createLogger from './logger';
import * as Sentry from '@sentry/node';
const Tracing = require('@sentry/tracing');
import { createServer } from 'http';
import { redisCache } from './graphql/redis';
import { DataSource } from 'apollo-datasource';
import { KeyValueCache } from 'apollo-server-caching';
import {
  NOT_FOUND,
  NOT_MODIFIED,
  INTERNAL_SERVER_ERROR,
  BAD_REQUEST
} from 'http-status-codes';
import {
  getBlobItemDownload,
  getLatestRuneliteBlob
} from './rest/BlobStorageClient';
import getMetricData from './rest/metrics';
import getMetricData2 from './rest/metrics2';
import ApolloLinkTimeout from 'apollo-link-timeout';
import { createHttpLink } from 'apollo-link-http';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-boost';
import { getItem } from './rest/Item';

const logger = createLogger('osrs-tools:graphql_server:index');

logger.trace('Loaded environment variables:', loadedEnvVars);

const app = express();

const sentryInitOptions: Sentry.NodeOptions = {
  // environment: process.env.APP_ENV,
  // see why we use APP_NAME here: https://github.com/getsentry/sentry-cli/issues/482
  // release: `${process.env.APP_NAME}-${process.env.APP_REVISION}` || '0.0.1',
  dsn: process.env.sentry_dsn,
  // integrations: [
  // used for rewriting SourceMaps from js to ts
  // check that sourcemaps are enabled in tsconfig.js
  // read the docs https://docs.sentry.io/platforms/node/typescript/
  // new RewriteFrames({
  //   root: process.cwd(),
  // }) as any,
  // Output sended data by Sentry to console.log()
  // new Debug({ stringify: true }),
  // ],

  integrations: [
    // enable HTTP calls tracing
    new Sentry.Integrations.Http({ tracing: true }),
    // enable Express.js middleware tracing
    new Tracing.Integrations.Express({
      // to trace all requests to the default router
      app
      // alternatively, you can specify the routes you want to trace:
      // router: someRouter,
    })
  ],

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
};
logger.debug('Creating sentry instance with options:', sentryInitOptions);
Sentry.init(sentryInitOptions);

function getDataSources() {
  return {
    osrsApi: new OsrsApiDataSource(),
    crystalMethLabsApi: new CrystalMathLabsDataSource(),
    player: new PlayerInformationDataSource(),
    rsbuddyApi: new RSBuddyApiDataSource()
  };
}

function initDataSourcesForSubscriptions<TContext>(
  dataSources: { [dataSourceName: string]: DataSource },
  context: TContext,
  cache: KeyValueCache
) {
  Object.values(dataSources).forEach(dataSource => {
    if (!dataSource || !dataSource.initialize) return;
    dataSource.initialize({ context: context, cache: cache });
  });
}

let clientsConnected = 0;

const apolloServerOptions: ApolloServerExpressConfig = {
  typeDefs,
  resolvers,
  dataSources: getDataSources,
  cache: redisCache,
  introspection: true,
  subscriptions: {
    // path: '/subscriptions',
    onConnect() {
      logger.info(
        `Socket connected - ${++clientsConnected} sockets now connected`
      );
    },
    onDisconnect() {
      logger.info(
        `Socket disconnected - ${--clientsConnected} sockets left connected`
      );
    }
  },
  uploads: false,
  plugins: [new ApolloSentryPlugin()],
  context: ({ req, connection }) => {
    // Manually put dataSources on the context for WebSocket based connections
    // https://github.com/apollographql/apollo-server/issues/1526
    if (connection) {
      const dataSources = getDataSources();
      initDataSourcesForSubscriptions(
        dataSources,
        connection.context,
        redisCache
      );
      return {
        dataSources
      };
    }
  }
};

const port = parseInt(process.env.PORT || '', 10) || 3001;
const apolloServer = new ApolloServer(apolloServerOptions);

const timeoutLink = new ApolloLinkTimeout(30000); // 30 second timeout
const httpLink = createHttpLink({ uri: process.env.graphql_url });
const timeoutHttpLink = timeoutLink.concat(httpLink);

// TODO: REDIS FOR CACHE?
const apolloClient = new ApolloClient({
  link: timeoutHttpLink,
  cache: new InMemoryCache()
});
logger.info(
  `Created apollo client for internal queries with URL '${process.env.graphql_url}'`
);

app.use(Sentry.Handlers.requestHandler() as RequestHandler);
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());

app.get('/download', async (req: Request, res: Response) => {
  logger.info('Providing download of latest custom runelite client');

  try {
    const runeliteBlob = await getLatestRuneliteBlob();
    if (!runeliteBlob) {
      res.sendStatus(NOT_FOUND);
      return;
    }

    // const latestVersionMatch = runeliteBlob.name.match(/\d+\.\d+\.\d+-\d+/);
    // const latestVersion = latestVersionMatch && latestVersionMatch[0];
    const latestVersion = runeliteBlob.properties.etag;

    const current = (req.header('If-None-Match') || '').replace(/(^"|"$)/g, '');
    // console.log(current);
    if (current && latestVersion && current === latestVersion) {
      res.sendStatus(NOT_MODIFIED);
      return;
    }

    const downloadStream =
      runeliteBlob && (await getBlobItemDownload(runeliteBlob));

    if (!downloadStream || !downloadStream.stream) {
      res.sendStatus(NOT_FOUND);
      return;
    }

    if (downloadStream.contentLength) {
      res.set('Content-Length', downloadStream.contentLength.toString());
    }
    if (latestVersion) {
      res.set('ETag', `"${latestVersion}"`);
    }
    res.attachment(downloadStream.filename);
    downloadStream.stream.pipe(res);
  } catch (e) {
    logger.error(e);
    res.sendStatus(INTERNAL_SERVER_ERROR);
  }
});

app.get('/item/:id', async (req: Request, res: Response) => {
  if (!req.params.id) {
    res.status(BAD_REQUEST).send('An item ID must be specified');
  }
  const item = await getItem(apolloClient, req.params.id);

  res.json(item);
});

app.post('/player/:playerId/skills', async (req: Request, res: Response) => {
  const playerId = req.params.playerId;
  logger.info('Updating player ' + playerId);

  const dataSources = getDataSources();
  initDataSourcesForSubscriptions(dataSources, {}, redisCache);

  const highscore = dataSources.osrsApi.getHighscore(playerId);
  await dataSources.player.addXpDataPoint(playerId, highscore);
});

app.get('/metrics', async (req: Request, res: Response) => {
  const playerId = req.query.playerId;
  logger.info('Getting metrics for player  ' + playerId);

  if (!playerId) {
    res
      .status(BAD_REQUEST)
      .send(
        'No player context. Metrics are retrieved per player by appending `?playerId=player_name` to the URL.'
      );
    return;
  }

  const requestedMetrics = {
    // onlineStatus: req.query.onlineStatus,
    // xp: req.query.xp,
    // level: req.query.level,
    value: req.query.includeValue
  };

  const metricFn = req.query.useNewMetrics ? getMetricData2 : getMetricData;

  try {
    const { metrics, contentType } = await metricFn(
      apolloClient,
      playerId,
      requestedMetrics
    );
    res.contentType(contentType);
    res.send(metrics);
  } catch (e) {
    logger.error(e);
    const apolloError = e as ApolloError;
    const status =
      apolloError?.networkError?.statusCode || INTERNAL_SERVER_ERROR;
    res.sendStatus(status);
  }
});

// app.use((req, res, next) => {
//   logger.trace('EXPRESS: request url is ' + req.url);
//   next();
// });

apolloServer.applyMiddleware({ app });
app.use(
  Sentry.Handlers.errorHandler({
    shouldHandleError(error) {
      return Number(error.status) >= 400;
    }
  }) as ErrorRequestHandler
);

const httpServer = createServer(app);
apolloServer.installSubscriptionHandlers(httpServer);

httpServer.listen(port, () => {
  logger.info(`> 🚀 GraphQL available at ${apolloServer.graphqlPath}`);
  logger.info(
    `> 🚀 GraphQL Subscriptions ready at ws://localhost:${port}${apolloServer.subscriptionsPath}`
  );
});
