import { ApolloServerPlugin, GraphQLRequestContext } from 'apollo-server-plugin-base';
import * as Sentry from '@sentry/node';
import { Severity } from '@sentry/node';

export default class ApolloSentryPlugin implements ApolloServerPlugin {
  // For plugin definition see the docs: https://www.apollographql.com/docs/apollo-server/integrations/plugins/
  requestDidStart() {
    return {
      didEncounterErrors(requestContext: GraphQLRequestContext) {
        requestContext.errors?.forEach((error) => {
          console.error(error);
          if (error.path || error.name !== 'GraphQLError') {
            // scope.setExtras({
            //   path: error.path,
            // });
            Sentry.captureException(error);
          } else {
            // scope.setExtras({});
            Sentry.captureMessage(`GraphQLWrongQuery: ${error.message}`, Severity.Error);
          }
        });

        // Sentry.withScope((scope) => {
        //   scope.addEventProcessor((event) =>
        //     Sentry.Handlers.parseRequest(event, (requestContext.context as any).req)
        //   );
        //
        //   // public user email
        //   // const userEmail = (requestContext.context as any).req?.session?.userId;
        //   // if (userEmail) {
        //   //   scope.setUser({
        //   //     // id?: string;
        //   //     ip_address: (requestContext.context as any).req?.ip,
        //   //     email: userEmail,
        //   //   });p
        //   // }
        //
        //   scope.setTags({
        //     graphql: requestContext.operation?.operation || 'parse_err',
        //     graphqlName: (requestContext.operationName as any) || (requestContext.request.operationName as any),
        //   });
        //
        //   requestContext.errors?.forEach((error) => {
        //     console.error(error);
        //     if (error.path || error.name !== 'GraphQLError') {
        //       scope.setExtras({
        //         path: error.path,
        //       });
        //       Sentry.captureException(error);
        //     } else {
        //       scope.setExtras({});
        //       Sentry.captureMessage(`GraphQLWrongQuery: ${error.message}`);
        //     }
        //   });
        // });
      },
    };
  }
};
