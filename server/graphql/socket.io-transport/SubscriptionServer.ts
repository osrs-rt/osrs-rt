import socketIO, { Socket } from 'socket.io';
import {
  GraphQLSchema,
} from 'graphql';
import SubscriptionSocket from './SubscriptionSocket';
import { DataSource } from 'apollo-datasource';
import { IResolvers, makeExecutableSchema } from 'graphql-tools';
// @ts-ignore
import typeDefs from '../schema.graphqls';
import resolvers from '../resolvers';

export type SubscriptionServerOptions = {
  typeDefs?: any,
  resolvers?: IResolvers | Array<IResolvers>,
  cache?: any,
  dataSources?: Function,
  onConnect?: Function,
  onDisconnect?: Function
};

type SubscriptionServerServerOptions = {
  server: any,
  path?: string
};

export default class SubscriptionServer {
  socket: SubscriptionSocket<{ dataSources?: { [key: string]: DataSource } }> | null = null;
  options: SubscriptionServerOptions;
  context: { dataSources?: { [key: string]: DataSource } };
  schema: GraphQLSchema;

  static create(
    options: SubscriptionServerOptions,
    serverOptions: SubscriptionServerServerOptions
  ) {
    return new SubscriptionServer(options, serverOptions);
  }

  constructor(
    options: SubscriptionServerOptions,
    serverOptions: SubscriptionServerServerOptions
  ) {
    const { server, ...otherOptions } = serverOptions;
    const io = socketIO(server, otherOptions);
    this.options = options;

    this.context = {};

    this.schema = makeExecutableSchema({
      typeDefs,
      resolvers
    });

    this.initialiseContext();

    io.on('connect', this.onConnect);
  }

  initialiseContext = () => {
    this.context = {};
    if (this.options.dataSources) {
      this.context.dataSources = this.options.dataSources();
      if (!this.context.dataSources) return;
      Object.values(this.context.dataSources).forEach(
        (dataSource: DataSource) => {
          if (!dataSource || !dataSource.initialize) return;
          dataSource.initialize({
            cache: this.options.cache,
            context: undefined
          });
        }
      );
    }
  };

  onConnect = (socket: Socket) => {
    this.socket = new SubscriptionSocket(
      socket,
      this.options,
      this.schema,
      this.context
    );
    if (this.options.onConnect) {
      this.options.onConnect();
    }

    socket.on('disconnect', this.onDisconnect);
  };

  onDisconnect = () => {
    if (this.options.onDisconnect) {
      this.options.onDisconnect();
    }
  };
}
