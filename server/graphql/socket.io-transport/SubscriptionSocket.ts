import createLogger from '../../logger';
import { Socket } from 'socket.io';
import { SubscriptionServerOptions } from './SubscriptionServer';
import {
  parse,
  execute,
  subscribe,
  GraphQLSchema,
  DocumentNode,
  validate,
  getOperationAST
} from 'graphql';
import { createAsyncIterator, forAwaitEach, isAsyncIterable } from 'iterall';

const logger = createLogger('osrs-tools:graphql_server:SubscriptionSocket');

type ExecutionParams = {
  id: number | string,
  query: string,
  variables?: { [index: string]: any }
};

export default class SubscriptionSocket<TContext extends Object> {
  socket: Socket;
  _subscriptionOptions: SubscriptionServerOptions;
  _context: TContext;
  _schema: GraphQLSchema;

  constructor(
    socket: Socket,
    subscriptionOptions: SubscriptionServerOptions,
    schema: GraphQLSchema,
    context: TContext
  ) {
    this.socket = socket;
    this._subscriptionOptions = subscriptionOptions;
    this._context = context || {};
    this._schema = schema;

    this.socket.on('connection_init', this._onConnectionInit);
    this.socket.on('start', this._onStart);
  }

  _onConnectionInit = () => {
    logger.debug(
      'Received connection_init message. Sending connection_ack in response'
    );
    this.socket.emit('connection_ack');
  };

  _onStart = async (
    executionParamsOrString: ExecutionParams | string,
    id: string | number
  ): Promise<void> => {
    const executionParams = typeof executionParamsOrString === 'string' ? JSON.parse(executionParamsOrString) : executionParamsOrString;

    id = id || executionParams.id; // Id can either be on first arg or specified as a second argumentp
    logger.debug(`Received start message with id ${id}`);

    if (!id) {
      this.socket.emit('data', { errors: [
        'no id specified'
        ] });
      return;
    }

    const query =
      typeof executionParams.query === 'string'
        ? parse(executionParams.query)
        : executionParams.query;
    const validationErrors = validate(this._schema, query);

    if (validationErrors.length) {
      this.socket.emit('data', { errors: validationErrors });
      return;
    }

    const executor = SubscriptionSocket._isSubscription(query)
      ? subscribe
      : execute;

    // @ts-ignore
    const result = await executor(
      this._schema,
      query,
      null,
      this._context,
      executionParams.variables
    );
    // TODO: ERROR HANDLING IN RESULT? SEEMS TO HAVE A MESSAGE PROPERTY WHEN THROWN

    const collection = isAsyncIterable(result)
      ? result
      : createAsyncIterator([result]);
    // @ts-ignore
    await forAwaitEach(collection, resultValue => {
      logger.debug(`Sending data to socket with id ${id}`);
      this.socket.emit('data', resultValue, id);
    });

    this.socket.emit('complete');
  };

  static _isSubscription(query: DocumentNode): boolean {
    const ast = getOperationAST(query, undefined);
    return !!ast && ast.operation === 'subscription';
  }
}
