type Query {
  item(id: ID!): Item
  items(
    tradeable: Boolean
    members: Boolean
    id: [ID]
    alchable: Boolean
    name: String
    highalch: IntSearchField
    lowalch: IntSearchField
    price: IntSearchField
    buy_limit: IntSearchField
  ): [Item]
  hiscore(playerId: String!): Hiscore
  #  getLikelyAlchChoices(members: Boolean): AlchReport
  xpTrackerReport(
    playerId: String!
    durationInSeconds: Int
    durationString: String
  ): XpTrackReport

  player(playerId: String!): Player!

  # TODO: DEPRECATE THE FOLLOWING
  playerInventory(playerId: String!): [ContainerItem]!
  playerEquipment(playerId: String!): [ContainerItem]!
  playerBank(playerId: String!): [ContainerItem]!
  playerGrandExchangeOffers(playerId: String!): [GrandExchangeOffer]!
  playerLocation(playerId: String!): Location

  quests: [Quest]
  TEST_ERROR: Boolean
}

type Mutation {
  playerXp(
    playerId: String!
    xpChanges: [XpChangeInput]!
  ): StandardMutationResponse!

  playerOnlineStatus(
    playerId: String!
    onlineStatus: OnlineStatus!
  ): StandardMutationResponse!

  inventoryChanged(
    playerId: String!
    items: [ContainerItemInput]!
  ): StandardMutationResponse!

  equipmentChanged(
    playerId: String!
    items: [ContainerItemInput]!
  ): StandardMutationResponse!

  bankChanged(
    playerId: String!
    items: [ContainerItemInput]!
  ): StandardMutationResponse!

  seedVaultChanged(
    playerId: String!
    items: [ContainerItemInput]!
  ): StandardMutationResponse!

  grandExchangeOffersChanged(
    playerId: String!
    offers: [GrandExchangeOfferInput]!
  ): StandardMutationResponse!

  playerLocationChanged(
    playerId: String!
    location: LocationInput
  ): StandardMutationResponse!

  questsStatusChanged(
    playerId: String!
    quests: [QuestStatusInput]
  ): StandardMutationResponse!

  xpUpdate(playerId: String!): StandardMutationResponse!

  storageServiceChanged(
    playerId: String!
    storageService: String!
    type: StorageServiceType!
    items: [ContainerItemInput]!
  ): StandardMutationResponse!
}

type Subscription {
  #  TODO: DEPRECATE IN FAVOUR OF xpChanged
  xpGained(playerId: String!): XpGainEvent
  xpChanged(playerId: String!): XpChangeEvent
  inventoryChanged(playerId: String!): ItemContainerChangeEvent
  equipmentChanged(playerId: String!): ItemContainerChangeEvent
  bankChanged(playerId: String!): ItemContainerChangeEvent
  seedVaultChanged(playerId: String!): ItemContainerChangeEvent
  grandExchangeOffersChanged(playerId: String!): GrandExchangeOfferChangeEvent
  playerLocationChanged(playerId: String!): PlayerLocationChangeEvent
}

type Player {
  onlineStatus: OnlineStatus!
  bank: [ContainerItem]!
  seedVault: [ContainerItem]!
  inventory: [ContainerItem]!
  equipment: [ContainerItem]!
  grandExchangeOffers: [GrandExchangeOffer]!
  location: Location
  xp: SkillNumberMap
  level: SkillNumberMap
  value(source: PriceSource): PlayerValue
  storageServices: [StorageService]!
  xpHistory(start: Float, end: Float): SkillHistoryMap
  xpDelta(start: Float, end: Float): SkillNumberMap
}

enum StorageServiceType {
  COFFER
}

type StorageService {
  name: String!
  type: StorageServiceType!
  items: [ContainerItem]!
}

enum PriceSource {
  HIGH_ALCH
  RUNELITE
  RSBUDDY
}

type PlayerValue {
  """
  The estimated value of the players bank.
  """
  bank: Int

  """
  The estimated value of all seeds and saplings in the players seed vault.
  """
  seedVault: Int

  """
  The estimated value of the players worn equipment.
  """
  equipment: Int

  """
  The estimated value of the players current inventory.
  """
  inventory: Int

  """
  The estimated value of the players grand exchange buy/sell offers.
  This is calculated by summing any current buy offers with the current market value of any sell offers rather than the asking price of the sale item.
  This is to stop values being skewed by over priced sale offers.
  """
  grandExchange: Int

  """
  Any other forms of wealth.
  This could include STASH units, Kingdom of Miscellania and the various coffers around Gelinor if client support is ever added.
  No client support is currently added so this value is always 0.
  """
  other: Int

  """
  The sum of all forms of estimated wealth.
  """
  total: Int
}

input XpChangeInput {
  skill: String! # TODO: DEFINE ENUM OF SKILLS
  xpGained: Int
  xpTotal: Int
}

input LocationInput {
  x: Int!
  y: Int!
  z: Int!
}

type Location {
  x: Int!
  y: Int!
  z: Int!
}

type PlayerLocationChangeEvent {
  playerId: String!
  location: Location!
}

enum QuestStatus {
  NOT_STARTED
  IN_PROGRESS
  FINISHED
}

enum OnlineStatus {
  ONLINE
  OFFLINE
  UNKNOWN
}

type SkillNumberMap {
  overall: Int
  attack: Int
  defence: Int
  strength: Int
  hitpoints: Int
  ranged: Int
  prayer: Int
  magic: Int
  cooking: Int
  woodcutting: Int
  fletching: Int
  fishing: Int
  firemaking: Int
  crafting: Int
  smithing: Int
  mining: Int
  herblore: Int
  agility: Int
  thieving: Int
  slayer: Int
  farming: Int
  runecrafting: Int
  hunter: Int
  construction: Int
}

type HistoryDataPoint {
  date: Float
  value: Int
}

type SkillHistoryMap {
  overall: [HistoryDataPoint]
  attack: [HistoryDataPoint]
  defence: [HistoryDataPoint]
  strength: [HistoryDataPoint]
  hitpoints: [HistoryDataPoint]
  ranged: [HistoryDataPoint]
  prayer: [HistoryDataPoint]
  magic: [HistoryDataPoint]
  cooking: [HistoryDataPoint]
  woodcutting: [HistoryDataPoint]
  fletching: [HistoryDataPoint]
  fishing: [HistoryDataPoint]
  firemaking: [HistoryDataPoint]
  crafting: [HistoryDataPoint]
  smithing: [HistoryDataPoint]
  mining: [HistoryDataPoint]
  herblore: [HistoryDataPoint]
  agility: [HistoryDataPoint]
  thieving: [HistoryDataPoint]
  slayer: [HistoryDataPoint]
  farming: [HistoryDataPoint]
  runecrafting: [HistoryDataPoint]
  hunter: [HistoryDataPoint]
  construction: [HistoryDataPoint]
}

type QuestRequirements {
  quests: [String!]
  skills: SkillNumberMap
}

type Quest {
  id: String!
  name: String!
  status(playerId: String): QuestStatus
  questPoints: Int
  number: Float
  url: String
  requirements: QuestRequirements
  subQuest: Boolean
}

input QuestStatusInput {
  id: String!
  status: QuestStatus!
}

type GrandExchangeOffer {
  itemId: Int!
  itemName: String
  itemIcon: String
  pricePerItem: Int!
  totalQuantity: Int!
  transferredQuantity: Int!
  wealthTransferred: Int!
  index: Int
  state: GrandExchangeOfferState
}

enum GrandExchangeOfferState {
  BOUGHT
  BUYING
  CANCELLED_BUY
  CANCELLED_SELL
  EMPTY
  SELLING
  SOLD
}

input GrandExchangeOfferInput {
  itemId: Int!
  pricePerItem: Int!
  totalQuantity: Int!
  transferredQuantity: Int!
  wealthTransferred: Int!
  index: Int
  state: GrandExchangeOfferState!
}

type GrandExchangeOfferChangeEvent {
  playerId: String!
  offers: [GrandExchangeOffer]
}

type ItemContainerChangeEvent {
  playerId: String!
  newItems: [ContainerItem]!
}

input IntSearchField {
  eq: Int
  neq: Int
  gt: Int
  gte: Int
  lt: Int
  lte: Int
}

input ContainerItemInput {
  id: Int!
  quantity: Int
  index: Int
}

enum ContainerTypes {
  inventory
  bank
  equipment
  grand_exchange
  seed_vault
}

type ContainerItem {
  item: Item!
  quantity: Int
  index: Int
  container: ContainerTypes
}

#type BankItem implements ContainerItem {
#  item: Item!
#  quantity: Int
#  index: Int
#  container: ContainerTypes
#}
#
#type SeedVaultItem implements ContainerItem {
#  item: Item!
#  quantity: Int
#  index: Int
#  container: ContainerTypes
#}
#
#type StorageServiceItem implements ContainerItem {
#  item: Item!
#  quantity: Int
#  index: Int
#  container: ContainerTypes
#}
#
#type InventoryItem implements ContainerItem {
#  item: Item!
#  quantity: Int
#  index: Int
#  container: ContainerTypes
#}
#
#type EquipmentItem implements ContainerItem {
#  item: Item!
#  quantity: Int
#  index: Int
#  container: ContainerTypes
#}

type Item {
  id: Int!
  name: String!
  price: Price!
  buy_limit: Int!
  members: Boolean!
  icon: String
}

type Price @cacheControl(maxAge: 1800) {
  rsbuddy: Int
  runelite: Int
  highalch: Int
  lowalch: Int
  #  osrs: Int
}

type StandardMutationResponse {
  success: Boolean!
  error: String
}

type XpGainEvent {
  playerId: String!
  skill: String!
  xpGained: Int!
}

type XpChange {
  skill: String!
  xpGained: Float!
  xpTotal: Float
}

type XpChangeEvent {
  playerId: String!
  xpChanges: [XpChange]!
}

type XpTrackReport {
  overall: XpGained
  attack: XpGained
  defence: XpGained
  strength: XpGained
  hitpoints: XpGained
  ranged: XpGained
  prayer: XpGained
  magic: XpGained
  cooking: XpGained
  woodcutting: XpGained
  fletching: XpGained
  fishing: XpGained
  firemaking: XpGained
  crafting: XpGained
  smithing: XpGained
  mining: XpGained
  herblore: XpGained
  agility: XpGained
  thieving: XpGained
  slayer: XpGained
  farming: XpGained
  runecrafting: XpGained
  hunter: XpGained
  construction: XpGained
}

type XpGained {
  xp: Int
  rank: Int
  xpGained: Int
  ranksGained: Int
  levelsGained: Int
  ehpGained: Float
}

type AlchReport {
  mostProfitable: [AlchableItem]
}

enum PlayerType {
  normal
  ironman
  ultimate
  hardcore_ironman
  deadman
  seasonal
}

type SkillScore {
  rank: Int
  level: Int
  experience: Int
}

type MinigameScore {
  rank: Int
  score: Int
}

type Hiscore {
  name: String!
  type: PlayerType
  overall: SkillScore
  attack: SkillScore
  defence: SkillScore
  strength: SkillScore
  hitpoints: SkillScore
  ranged: SkillScore
  prayer: SkillScore
  magic: SkillScore
  cooking: SkillScore
  woodcutting: SkillScore
  fletching: SkillScore
  fishing: SkillScore
  firemaking: SkillScore
  crafting: SkillScore
  smithing: SkillScore
  mining: SkillScore
  herblore: SkillScore
  agility: SkillScore
  thieving: SkillScore
  slayer: SkillScore
  farming: SkillScore
  runecrafting: SkillScore
  hunter: SkillScore
  construction: SkillScore

  easyClueScrolls: MinigameScore
  mediumClueScrolls: MinigameScore
  clueScrolls: MinigameScore
  bountyHunter: MinigameScore
  bountyHunterRogues: MinigameScore
  hardClueScrolls: MinigameScore
  lastManStanding: MinigameScore
  eliteClueScrolls: MinigameScore
  masterClueScrolls: MinigameScore
}

scalar StringOrInt

type AlchableItem {
  id: ID!
  name: String!
  members: Boolean!
  highalch: Int
  buy_limit: Int
  price: Price
  profit: Int
}

directive @cacheControl(
  maxAge: Int
  scope: CacheControlScope
) on OBJECT | FIELD_DEFINITION

enum CacheControlScope {
  PUBLIC
  PRIVATE
}
