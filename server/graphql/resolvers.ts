// A map of functions which return data for the schema.
import {
  getItemById,
  ItemQueryOptions,
  searchForItems
} from './itemRepository';
import { getCurrentPrice } from './rsBuddyRepository';
import { sortBy, mapValues, values, sum } from 'lodash';
import { GraphQLScalarType, Kind } from 'graphql';
import { PubSub, withFilter } from 'apollo-server-express';
import questsDB from '../../databases/quests.json';
import createLogger from '../logger';
import {
  IPlayerQuestStatus,
  ItemContainer,
  OnlineStatus,
  QuestStatus,
  StorageService,
  StorageServiceType,
  SkillNumberMap
} from './dataSources/PlayerInformationDataSource';
import {
  AllSkillNames,
  Item,
  XpChangeSubscription
} from '../../utils/sharedTypes';
import { IResolvers } from 'graphql-tools';
import { constants } from 'osrs-api';
import { Context } from './resolvers/context';
import * as PlayerValue from './resolvers/PlayerValue';
import { PriceSource } from './resolvers/PlayerValue';
import { getPrice } from './runeliteRepository';
import { getLevelForExperience } from '../../sharedCode/experience';
import { getHistoricalXp, getXpDelta } from './resolvers/XpHistory';

const logger = createLogger('osrs-tools:graphql_server:resolvers');

const pubsub = new PubSub();

enum ContainerType {
  inventory = 'inventory',
  bank = 'bank',
  equipment = 'equipment',
  grand_exchange = 'grand_exchange',
  seed_vault = 'seed_vault',
  storage_service = 'storage_service'
}

// type ContainerItem = {
//   item: Item;
//   quantity?: number;
//   index?: number;
//   container: ContainerType;
// };

function dtoMapperGenerator(container: ContainerType) {
  return (item: any) => {
    return {
      item: {
        name: item.name,
        id: item.id
      },
      index: item.index,
      quantity: item.quantity,
      container: container
    };
  };
}

const inventoryMapper = dtoMapperGenerator(ContainerType.inventory);
const bankMapper = dtoMapperGenerator(ContainerType.bank);
const seedVaultMapper = dtoMapperGenerator(ContainerType.seed_vault);
const equipmentMapper = dtoMapperGenerator(ContainerType.equipment);
const storageServiceMapper = dtoMapperGenerator(ContainerType.storage_service);

type PlayerQueryOptions = {
  playerId: string;
};

async function getXpResolver(
  parent: { player: string },
  args: any,
  { dataSources }: Context
): Promise<SkillNumberMap> {
  // TODO: GET FROM API IF NOT LOGGED IN
  // TODO: GET FROM MAX OF API VS CACHE IF STATUS IS UNKNOWN

  const [cacheXp, status] = await Promise.all([
    dataSources.player.getCurrentXp(parent.player),
    dataSources.player.getOnlineStatus(parent.player)
  ]);

  if (status === 'ONLINE') {
    return cacheXp;
  }

  const hiscore = await dataSources.osrsApi.getHighscore(parent.player);
  return Object.keys(constants.skills).reduce((xpRecord, skillValue) => {
    const skill = skillValue as AllSkillNames;
    const cacheValue = cacheXp[skill] || 0;
    const apiValue = hiscore[skill].experience || 0;
    xpRecord[skill] = Math.max(cacheValue, apiValue);
    return xpRecord;
  }, {} as SkillNumberMap);
}

const resolvers: IResolvers<any, Context> = {
  Query: {
    TEST_ERROR() {
      throw new Error(
        'This is a test error to ensure error reporting is working correctly'
      );
    },

    item(parent: any, args: { id: number }) {
      return getItemById(args.id);
    },
    items(parent: any, args: ItemQueryOptions) {
      return searchForItems(args);
    },
    hiscore(parent: any, args: PlayerQueryOptions, context: Context) {
      return context.dataSources.osrsApi.getHighscore(args.playerId);
    },

    xpTrackerReport(
      parent: any,
      args: PlayerQueryOptions & {
        durationInSeconds?: number;
        durationString?: string;
      },
      { dataSources }: Context
    ) {
      const duration =
        args.durationInSeconds || args.durationString || 60 * 60 * 24;
      return dataSources.crystalMethLabsApi.getTrackedData(
        args.playerId,
        duration
      );
    },

    player(parent: any, args: PlayerQueryOptions) {
      return {
        player: args.playerId
      };
    },

    async playerInventory(
      parent: any,
      args: PlayerQueryOptions,
      { dataSources }: Context
    ) {
      const playerId = args.playerId;
      const items = await dataSources.player.getInventory(playerId);
      return items.filter(x => !!x).map(inventoryMapper);
    },

    async playerEquipment(
      parent: any,
      args: PlayerQueryOptions,
      { dataSources }: Context
    ) {
      const playerId = args.playerId;
      const items = await dataSources.player.getEquipment(playerId);
      return items.filter(x => !!x).map(equipmentMapper);
    },

    async playerBank(
      parent: any,
      args: PlayerQueryOptions,
      { dataSources }: Context
    ) {
      const playerId = args.playerId;
      const items = await dataSources.player.getBank(playerId);
      return items.filter(x => !!x).map(bankMapper);
    },

    async playerGrandExchangeOffers(
      parent: any,
      args: PlayerQueryOptions,
      { dataSources }: Context
    ) {
      const playerId = args.playerId;
      return dataSources.player.getGrandExchangeOffers(playerId);
    },

    async playerLocation(
      parent: any,
      args: PlayerQueryOptions,
      { dataSources }: Context
    ) {
      const playerId = args.playerId;
      return dataSources.player.getLocation(playerId);
    },

    async quests() {
      const quests = Object.entries(questsDB).map(([id, quest]) => ({
        ...quest,
        id
      }));

      return sortBy(quests, q => q.number);
    }
  },

  Subscription: {
    xpGained: {
      subscribe: withFilter(
        () => pubsub.asyncIterator('xp_gain'),
        (payload, variables) => {
          return payload.xpGained.playerId === variables.playerId;
        }
      )
    },
    xpChanged: {
      subscribe: withFilter(
        () => pubsub.asyncIterator('xp_changed'),
        (payload, variables) => {
          return payload.xpChanged.playerId === variables.playerId;
        }
      )
    },
    inventoryChanged: {
      subscribe: withFilter(
        () => pubsub.asyncIterator('inventory_changed'),
        (payload, variables) => {
          return payload.inventoryChanged.playerId === variables.playerId;
        }
      )
    },
    equipmentChanged: {
      subscribe: withFilter(
        () => pubsub.asyncIterator('equipment_changed'),
        (payload, variables) => {
          return payload.equipmentChanged.playerId === variables.playerId;
        }
      )
    },
    bankChanged: {
      subscribe: withFilter(
        () => pubsub.asyncIterator('bank_changed'),
        (payload, variables) => {
          return payload.bankChanged.playerId === variables.playerId;
        }
      )
    },
    seedVaultChanged: {
      subscribe: withFilter(
        () => pubsub.asyncIterator('seed_vault_changed'),
        (payload, variables) => {
          return payload.seedVaultChanged.playerId === variables.playerId;
        }
      )
    },
    grandExchangeOffersChanged: {
      subscribe: withFilter(
        () => pubsub.asyncIterator('ge_offers_changed'),
        (payload, variables) => {
          return (
            payload.grandExchangeOffersChanged.playerId === variables.playerId
          );
        }
      )
    },
    playerLocationChanged: {
      subscribe: withFilter(
        () => pubsub.asyncIterator('location_changed'),
        (payload, variables) => {
          return payload.playerLocationChanged.playerId === variables.playerId;
        }
      )
    }
  },

  Mutation: {
    async playerOnlineStatus(
      parent: any,
      args: PlayerQueryOptions & { onlineStatus: OnlineStatus },
      { dataSources }
    ) {
      logger.debug(
        `Setting player ${args.playerId} status to ${args.onlineStatus}`
      );
      try {
        await dataSources.player.setOnlineStatus(
          args.playerId,
          args.onlineStatus
        );
        return {
          success: true
        };
      } catch (e) {
        return {
          success: false,
          error: e.message
        };
      }
    },

    async xpUpdate(parent: any, args: PlayerQueryOptions, { dataSources }) {
      logger.debug('Add data point');
      try {
        const highscore = dataSources.osrsApi.getHighscore(args.playerId);
        dataSources.player.addXpDataPoint(args.playerId, highscore);
        return {
          success: true
        };
      } catch (e) {
        return {
          success: false,
          error: e.message
        };
      }
    },

    async playerXp(
      parent: any,
      args: PlayerQueryOptions & XpChangeSubscription,
      { dataSources }
    ) {
      logger.debug('XP Changed Mutation', args);
      try {
        const changesPromises = args.xpChanges.map(change =>
          dataSources.player.setCurrentXp(
            args.playerId,
            change.skill,
            change.xpTotal
          )
        );
        await Promise.all(changesPromises);

        // TOOD: REMOVE ONCE BELOW IS HOOKED UP
        args.xpChanges.forEach(change => {
          pubsub.publish('xp_gain', {
            xpGained: {
              playerId: args.playerId,
              skill: change.skill,
              xpGained: change.xpGained || 0
            }
          });
        });

        await pubsub.publish('xp_changed', {
          xpChanged: args
        });

        return {
          success: true
        };
      } catch (e) {
        return {
          success: false,
          error: e.message
        };
      }
    },

    async inventoryChanged(
      parent: any,
      args: PlayerQueryOptions & { items: any },
      { dataSources }: Context
    ) {
      logger.info('Inventory changed mutation', args);
      let error;
      try {
        await dataSources.player.setInventory(args.playerId, args.items);
        await pubsub.publish('inventory_changed', {
          inventoryChanged: {
            newItems: args.items.map(inventoryMapper),
            playerId: args.playerId
          }
        });
      } catch (e) {
        logger.error(e);
        error = e;
      }
      return {
        success: error == null,
        error: error && error.message
      };
    },

    async equipmentChanged(
      parent: any,
      args: PlayerQueryOptions & { items: any },
      { dataSources }: Context
    ) {
      logger.info('Equipment changed mutation', args);
      let error;
      try {
        await dataSources.player.setEquipment(args.playerId, args.items);
        await pubsub.publish('equipment_changed', {
          equipmentChanged: {
            newItems: args.items.map(equipmentMapper),
            playerId: args.playerId
          }
        });
      } catch (e) {
        logger.error(e);
        error = e;
      }
      return {
        success: error == null,
        error: error && error.message
      };
    },

    async bankChanged(
      parent: any,
      args: PlayerQueryOptions & { items: any },
      { dataSources }: Context
    ) {
      logger.info('Bank changed mutation', args);
      let error;
      try {
        await dataSources.player.setBank(args.playerId, args.items);
        await pubsub.publish('bank_changed', {
          bankChanged: {
            newItems: args.items.map(bankMapper),
            playerId: args.playerId
          }
        });
      } catch (e) {
        logger.error(e);
        error = e;
      }
      return {
        success: error == null,
        error: error && error.message
      };
    },

    async seedVaultChanged(
      parent: any,
      args: PlayerQueryOptions & { items: any },
      { dataSources }: Context
    ) {
      logger.info('Seed vault changed mutation', args);
      let error;
      try {
        await dataSources.player.setSeedVault(args.playerId, args.items);
        await pubsub.publish('seed_vault_changed', {
          seedVaultChanged: {
            newItems: args.items.map(seedVaultMapper),
            playerId: args.playerId
          }
        });
      } catch (e) {
        logger.error(e);
        error = e;
      }
      return {
        success: error == null,
        error: error && error.message
      };
    },

    async grandExchangeOffersChanged(
      parent: any,
      args: PlayerQueryOptions & { offers: any },
      { dataSources }: Context
    ) {
      logger.info('GE offers changed mutation', args);
      let error;
      try {
        await dataSources.player.setGrandExchangeOffers(
          args.playerId,
          args.offers
        );
        await pubsub.publish('ge_offers_changed', {
          grandExchangeOffersChanged: {
            offers: args.offers,
            playerId: args.playerId
          }
        });
      } catch (e) {
        logger.error(e);
        error = e;
      }
      return {
        success: error == null,
        error: error && error.message
      };
    },

    async playerLocationChanged(
      parent: any,
      args: PlayerQueryOptions & { location: any },
      { dataSources }: Context
    ) {
      logger.info('Player Location changed mutation', args);
      let error;
      try {
        await dataSources.player.setLocation(args.playerId, args.location);
        await pubsub.publish('location_changed', {
          playerLocationChanged: args
        });
      } catch (e) {
        logger.error(e);
        error = e;
      }
      return {
        success: error == null,
        error: error && error.message
      };
    },

    async storageServiceChanged(
      parent: any,
      args: PlayerQueryOptions & {
        storageService: string;
        type: StorageServiceType;
        items: ItemContainer;
      },
      { dataSources }: Context
    ) {
      logger.info('Storage service changed mutation', args);
      let error;
      try {
        await dataSources.player.setStorageService(
          args.playerId,
          args.storageService,
          args.type,
          args.items
        );
        // await pubsub.publish('equipment_changed', {
        //   equipmentChanged: {
        //     newItems: args.items.map(equipmentMapper),
        //     playerId: args.playerId
        //   }
        // });
      } catch (e) {
        logger.error(e);
        error = e;
      }
      return {
        success: error == null,
        error: error && error.message
      };
    },

    async questsStatusChanged(
      parent: any,
      args: PlayerQueryOptions & { quests: IPlayerQuestStatus[] },
      { dataSources }: Context
    ) {
      logger.info('Quests status changed mutation');
      let error;
      try {
        await dataSources.player.setQuestStatuses(args.playerId, args.quests);
        await pubsub.publish('quests_changed', {
          questsChanged: {
            quests: args.quests,
            playerId: args.playerId
          }
        });
      } catch (e) {
        logger.error(e);
        error = e;
      }
      return {
        success: error == null,
        error: error && error.message
      };
    }
  },

  GrandExchangeOffer: {
    async itemName(parent: { itemId: number }) {
      const item = await getItemById(parent.itemId);
      if (!item) return null;

      return item.name;
    },
    async itemIcon(parent: { itemId: number }) {
      const item = await getItemById(parent.itemId);
      if (!item) return null;

      return item.icon;
    }
  },

  Quest: {
    async status(
      parent: any,
      args: PlayerQueryOptions,
      { dataSources }: Context
    ) {
      logger.debug('Getting status of Quest', args);
      if (!args || !args.playerId) return null;

      const statusForQuest = await dataSources.player.getQuestStatus(
        args.playerId,
        parent.id
      );
      return statusForQuest ? statusForQuest.status : QuestStatus.NOT_STARTED;
    }
  },

  Item: {
    async name(parent: Item) {
      const item = await getItemById(parent.id);
      if (!item) return null;

      return item.name;
    },
    price(parent: Item) {
      // logger.debug('price of parent', parent);
      // return getCurrentPrice(parent.id);
      return {
        id: parent.id,
        highalch: parent.highalch,
        lowalch: parent.lowalch
      };
    }
  },

  ContainerItem: {
    item(parent: any) {
      return getItemById(parent.item.id);
    }
    // __resolveType(item: ContainerItem) {
    //   switch (item.container) {
    //     case ContainerType.inventory:
    //       return 'InventoryItem';
    //     case ContainerType.equipment:
    //       return 'EquipmentItem';
    //     case ContainerType.bank:
    //       return 'BankItem';
    //     case ContainerType.seed_vault:
    //       return 'SeedVaultItem';
    //     case ContainerType.storage_service:
    //       return 'StorageServiceItem';
    //   }
    // }
  },

  Price: {
    // async osrs(parent, args, { dataSources }) {
    //   try {
    //     return await dataSources.osrsApi.getItem(parent.id);
    //   } catch (error) {
    //     logger.error(error);
    //     return null;
    //   }
    // },
    async rsbuddy(parent: Item, args: any, { dataSources }: Context) {
      const price = await getCurrentPrice(parent.id, dataSources.rsbuddyApi);
      return price || 0;
    },
    async runelite(parent: Item) {
      return getPrice(parent.id);
    }
  },

  Player: {
    async onlineStatus(
      parent: { player: string },
      args: any,
      { dataSources }: Context
    ) {
      // TODO: DETECT DISCONNECTS. NEEDS A LOOPING PROCESS AND NEEDS THE CLIENT TO SEND KEEP ALIVE PACKETS. LOOPING PROCESS WILL CHECK DATES OF LAST REPORT IN
      return dataSources.player.getOnlineStatus(parent.player);
    },
    async bank(
      parent: { player: string },
      args: any,
      { dataSources }: Context
    ) {
      const items = await dataSources.player.getBank(parent.player);
      return items.filter(x => !!x).map(bankMapper);
    },
    async seedVault(
      parent: { player: string },
      args: any,
      { dataSources }: Context
    ) {
      const items = await dataSources.player.getSeedVault(parent.player);
      return items.filter(x => !!x).map(seedVaultMapper);
    },
    async storageServices(
      parent: { player: string },
      args: any,
      { dataSources }: Context
    ) {
      const items = await dataSources.player.getStorageServices(parent.player);
      return items
        .filter(x => !!x)
        .map((item: StorageService) => {
          return {
            ...item,
            items: item.items.map(storageServiceMapper)
          };
        });
    },
    async inventory(
      parent: { player: string },
      args: any,
      { dataSources }: Context
    ) {
      const items = await dataSources.player.getInventory(parent.player);
      return items.filter(x => !!x).map(inventoryMapper);
    },
    async equipment(
      parent: { player: string },
      args: any,
      { dataSources }: Context
    ) {
      const items = await dataSources.player.getEquipment(parent.player);
      return items.filter(x => !!x).map(equipmentMapper);
    },
    grandExchangeOffers(
      parent: { player: string },
      args: any,
      { dataSources }: Context
    ) {
      return dataSources.player.getGrandExchangeOffers(parent.player);
    },
    location(parent: { player: string }, args: any, { dataSources }: Context) {
      return dataSources.player.getLocation(parent.player);
    },

    xp: getXpResolver,

    xpHistory: (
      parent: { player: string },
      { start, end }: { start: number; end: number }
    ) => {
      const step = (end - start) / 1000;
      return getHistoricalXp(
        parent.player,
        new Date(start),
        new Date(end),
        step
      );
    },

    xpDelta(
      parent: { player: string },
      { start, end }: { start: number; end: number }
    ) {
      return getXpDelta(parent.player, new Date(start), new Date(end));
    },

    async level(
      parent: { player: string },
      args: any,
      context: Context
    ): Promise<SkillNumberMap> {
      const xpMap = await getXpResolver(parent, args, context);
      const levels = mapValues(xpMap, xp => getLevelForExperience(xp || 0));
      levels.overall = 0;
      levels.overall = sum(values(levels));
      return levels;
    },

    value(parent: { player: string }, args: { source: PriceSource }) {
      return {
        ...parent,
        source: args.source
      };
    }
  },

  PlayerValue,

  StringOrInt: new GraphQLScalarType({
    name: 'StringOrInt',
    description: 'A String or an Int union type',
    serialize(value) {
      if (typeof value !== 'string' && typeof value !== 'number') {
        throw new Error('Value must be either a String or an Int');
      }

      if (typeof value === 'number' && !Number.isInteger(value)) {
        throw new Error('Number value must be an Int');
      }

      return value;
    },
    parseValue(value) {
      if (typeof value !== 'string' && typeof value !== 'number') {
        throw new Error('Value must be either a String or an Int');
      }

      if (typeof value === 'number' && !Number.isInteger(value)) {
        throw new Error('Number value must be an Int');
      }

      return value;
    },
    parseLiteral(ast) {
      // Kinds: http://facebook.github.io/graphql/June2018/#sec-Type-Kinds
      // ast.value is always a string
      switch (ast.kind) {
        case Kind.INT:
          return parseInt(ast.value, 10);
        case Kind.STRING:
          return ast.value;
        default:
          throw new Error('Value must be either a String or an Int');
      }
    }
  })
};

export default resolvers;
