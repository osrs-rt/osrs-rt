import RSBuddyApiDataSource from './RSBuddyApiDataSource';
import * as rsbuddyApi from '../../rsbuddyApiClient';

const mockApi = rsbuddyApi as jest.Mocked<typeof rsbuddyApi>;

jest.mock('../../rsbuddyApiClient', () => ({
  getItems: jest.fn()
}));

const fakeItemList = {
  1: {
    id: 1,
    name: 'item 1',
    members: true,
    sp: 1, //store price
    buy_average: 1,
    buy_quantity: 1,
    sell_average: 1,
    sell_quantity: 1,
    overall_average: 1,
    overall_quantity: 1
  },
  2: {
    id: 2,
    name: 'item 2',
    members: true,
    sp: 2, //store price
    buy_average: 2,
    buy_quantity: 2,
    sell_average: 2,
    sell_quantity: 2,
    overall_average: 2,
    overall_quantity: 2
  }
};

beforeEach(() => {
  jest.clearAllMocks();

  mockApi.getItems.mockResolvedValue(fakeItemList);
});

describe('getItem', () => {
  it('should only return null if data source has not been correctly initialised', async () => {
    const dataSource = new RSBuddyApiDataSource();

    const item1 = await dataSource.getItem(1);

    expect(item1).toBeFalsy();
  });
});
