import { DataSource, DataSourceConfig } from 'apollo-datasource';
import Crystalmethlabs from 'crystalmethlabs';
import createLogger from '../../logger';
import { KeyValueCache } from 'apollo-server-caching';

const logger = createLogger(
  'osrs-tools:graphql_server:CrystalMathLabsDataSource'
);

class CrystalMathLabsDataSource extends DataSource {
  private _cache: KeyValueCache | null = null;
  private _api: Crystalmethlabs;
  constructor() {
    super();
    this._api = new Crystalmethlabs('osrs');
  }

  initialize(config: DataSourceConfig<any>): void {
    this._cache = config.cache;
  }

  async getTrackedData(player: string, durationInSeconds: number | string) {
    this.updateSideEffect(player);

    const { err, stats } = await this._api.track(player, durationInSeconds);
    if (err) {
      logger.error(err);
      throw err;
    }
    return stats;
  }

  async getRecords(player: string) {
    this.updateSideEffect(player);

    const { err, records } = await this._api.recordsOfPlayer(player);
    if (err) {
      logger.error(err);
      throw err;
    }
    return records;
  }

  updateSideEffect = (player: string) => {
    this.updateIfStale(player).catch(e => {
      console.error(e);
      // Do nothing else. We ignore the error and certainly don't stop the callers from executing
    });
  };

  update = async (player: string) => {
    const { err = undefined } = (await this._api.update(player)) || {};
    if (err) {
      logger.error(err);
      throw err;
    }
  };

  lastUpdate = async (player: string): Promise<number> => {
    const { err, sec } = await this._api.lastchange(player);
    if (err) {
      logger.error(err);
      throw err;
    }
    return sec || -1;
  };

  updateIfStale = async (player: string, ttl: number = 3600) => {
    const cacheKey = `crystal_meth_labs.${player}.last_updated`;
    const timeLastUpdated = !this._cache ? NaN : Number(await this._cache.get(cacheKey));

    const now = global.Date.now();

    // The use of the cache assumes that we are the only source of updates for this user.
    const secondsSinceLastUpdate = (now - timeLastUpdated) / 1000; //  : await this.lastUpdate(player);

    if (!timeLastUpdated || secondsSinceLastUpdate > ttl) {
      await this.update(player);
      this._cache && await this._cache.set(cacheKey, Date.now().toString());
    }
  };
}

export default CrystalMathLabsDataSource;
