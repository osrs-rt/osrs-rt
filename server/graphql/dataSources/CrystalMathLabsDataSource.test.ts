import CrystalMathLabsDataSource from './CrystalMathLabsDataSource'
import Crystalmethlabs from 'crystalmethlabs';
import { install as installClock, InstalledClock } from 'lolex';
jest.mock('crystalmethlabs');


const crystalmethlabs: jest.Mock<jest.Mocked<Crystalmethlabs>> = Crystalmethlabs as any;

let clock: InstalledClock;

const emptyStatGain = {
  xp: 1,
  rank: 0,
  xpGained: 0,
  ranksGained: 0,
  levelsGained: 0,
  ehpGained: 0
};
const emptyStatsObject = {
  lastchanged: 0,
  overall: emptyStatGain,
  attack: emptyStatGain,
  defence: emptyStatGain,
  strength: emptyStatGain,
  hitpoints: emptyStatGain,
  ranged:emptyStatGain,
  prayer: emptyStatGain,
  magic:emptyStatGain,
  cooking: emptyStatGain,
  woodcutting:emptyStatGain,
  fletching:emptyStatGain,
  fishing:emptyStatGain,
  firemaking:emptyStatGain,
  crafting:emptyStatGain,
  smithing:emptyStatGain,
  mining:emptyStatGain,
  herblore:emptyStatGain,
  agility:emptyStatGain,
  thieving:emptyStatGain,
  slayer:emptyStatGain,
  farming:emptyStatGain,
  runecrafting:emptyStatGain,
  hunter:emptyStatGain,
  construction:emptyStatGain,
  ehp: { hours: 0, ranksGained: 0, ehpGained: 0 } }

beforeEach(() => {
  // Clear all instances and calls to constructor and all methods:
  crystalmethlabs.mockClear();

  clock = installClock();
});

afterEach(() => {
  clock.uninstall();
});

describe('getTrackedData', () => {
  it('should return the stats retrieved from CML if no errors were returned', async function() {
    const dataSource = new CrystalMathLabsDataSource();
    const mockCML = crystalmethlabs.mock.instances[0];

    const stats = emptyStatsObject;
    mockCML.track.mockResolvedValueOnce({ stats, err: undefined });
    const data = await dataSource.getTrackedData('player1', 1);

    expect(data).toBe(stats);
  });

  it('should throw error if errors returned from CML', async function() {
    const dataSource = new CrystalMathLabsDataSource();
    const mockCML = crystalmethlabs.mock.instances[0];

    mockCML.track.mockResolvedValueOnce({ err: 'There was an error' });
    const promise = dataSource.getTrackedData('player1', 1);

    await expect(promise).rejects.toEqual('There was an error');
  });
});

describe('getRecords', () => {
  it('should return the stats retrieved from CML if no errors were returned', async function() {
    const dataSource = new CrystalMathLabsDataSource();
    const mockCML = crystalmethlabs.mock.instances[0];

    const records = {};
    // @ts-ignore
    mockCML.recordsOfPlayer.mockResolvedValueOnce({ records, err: undefined });
    const data = await dataSource.getRecords('player1');

    expect(data).toBe(records);
  });

  it('should throw error if errors returned from CML', async function() {
    const dataSource = new CrystalMathLabsDataSource();
    const mockCML = crystalmethlabs.mock.instances[0];

    mockCML.recordsOfPlayer.mockResolvedValueOnce({ err: 'There was an error' });
    const promise = dataSource.getRecords('player1');

    await expect(promise).rejects.toEqual('There was an error');
  });
});

describe('updateIfStale', () => {
  it('should update CML if the cache does not contain a time since last update for the given player', async function() {
    const dataSource = new CrystalMathLabsDataSource();
    const mockCML = crystalmethlabs.mock.instances[0];
    mockCML.update.mockImplementationOnce(() => ({}));
    const mockCache = { get: jest.fn(), set: jest.fn(), delete: jest.fn() };
    clock.setSystemTime(1000);

    dataSource.initialize({ cache: mockCache, context: null });
    await dataSource.updateIfStale('player1');

    expect(mockCML.update).toHaveBeenCalled();
  });

  it('should update CML if the cache contains a time since last update for the given player that is older than the validity threshold', async function() {
    const dataSource = new CrystalMathLabsDataSource();
    const mockCML = crystalmethlabs.mock.instances[0];
    mockCML.update.mockImplementationOnce(() => ({}));
    const mockCache = { get: jest.fn(() => Promise.resolve('1000')), set: jest.fn(), delete: jest.fn() }
    clock.setSystemTime(15000);

    dataSource.initialize({ cache: mockCache, context: null });
    await dataSource.updateIfStale('player1', 10);

    expect(mockCML.update).toHaveBeenCalled();
  });

  it('should not update CML if the cache contains a time since last update for the given player that is less than the validity threshold', async function() {
    const dataSource = new CrystalMathLabsDataSource();
    const mockCML = crystalmethlabs.mock.instances[0];
    mockCML.update.mockImplementationOnce(() => ({}));
    const mockCache = { get: jest.fn(() => Promise.resolve('10000')), set: jest.fn(), delete: jest.fn() };
    clock.setSystemTime(15000);

    dataSource.initialize({ cache: mockCache, context: null });
    await dataSource.updateIfStale('player1', 10);

    expect(mockCML.update).not.toHaveBeenCalled();
  });
});

describe('update', () => {
  it('should throw an error if the CML API returns an error', async function() {
    const dataSource = new CrystalMathLabsDataSource();
    const mockCML = crystalmethlabs.mock.instances[0];

    mockCML.update.mockImplementationOnce(() => ({ err: 'There was an error' }))
    const promise = dataSource.update('player1');

    await expect(promise).rejects.toEqual('There was an error');
  });
});

describe('lastUpdate', () => {
  it('should return the seconds retrieved from CML if no errors were returned', async function() {
    const dataSource = new CrystalMathLabsDataSource();
    const mockCML = crystalmethlabs.mock.instances[0];

    const seconds = 10;
    mockCML.lastchange.mockResolvedValueOnce({ sec: seconds, err: undefined });
    const data = await dataSource.lastUpdate('player1');

    expect(data).toBe(seconds);
  });

  it('should throw error if errors returned from CML', async function() {
    const dataSource = new CrystalMathLabsDataSource();
    const mockCML = crystalmethlabs.mock.instances[0];

    mockCML.lastchange.mockResolvedValueOnce({ err: 'There was an error' });
    const promise = dataSource.lastUpdate('player1');

    await expect(promise).rejects.toEqual('There was an error');
  });
});