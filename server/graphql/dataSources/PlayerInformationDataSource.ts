import createLogger from '../../logger';
import { DataSource } from 'apollo-datasource';
import { generateCacheKey } from '../cacheUtils';
import redis from '../redis';
import { isToday } from 'date-fns';
import _ from 'lodash';
import {
  HiscoreMinigameResult,
  HiscoreResult,
  HiscoreSkillResult,
  valueof,
  Skill
} from 'osrs-api';
import { AllSkillNames } from '../../../utils/sharedTypes';

function mapHighscore(
  hiscore: HiscoreResult
): Record<keyof HiscoreResult, number> {
  return Object.entries(hiscore).reduce((cur, [key, value]) => {
    const hiscoreValue = value as valueof<HiscoreSkillResult>;
    const minigameValue = value as valueof<HiscoreMinigameResult>;
    cur[key as Skill] =
      (hiscoreValue && hiscoreValue.experience) ||
      (minigameValue && minigameValue.score) ||
      (value as number);
    return cur;
  }, {} as Record<keyof HiscoreResult, number>);
}

const logger = createLogger(
  'osrs-tools:graphql_server:PlayerInformationDataSource'
);

interface CacheItem<T> {
  date?: number;
  item: T;
}

type CacheItemWithLegacy<T> = CacheItem<T> | T;

export type Item = {
  id: number;
  quantity?: number;
};

export type ItemContainer = Item[];

export enum StorageServiceType {
  coffer
}

export interface StorageService {
  name: string;
  type: StorageServiceType;
  items: ItemContainer;
}

export type GrandExchangeOffer = {
  itemId: number;
  itemName: string;
  itemIcon: string;
  pricePerItem: number;
  totalQuantity: number;
  transferredQuantity: number;
  wealthTransferred: number;
  index: number;
  state:
    | 'BOUGHT'
    | 'BUYING'
    | 'CANCELLED_BUY'
    | 'CANCELLED_SELL'
    | 'EMPTY'
    | 'SELLING'
    | 'SOLD';
};

export type Location = {
  x: number;
  y: number;
  z: number;
};

export enum QuestStatus {
  'NOT_STARTED' = 'NOT_STARTED',
  'IN_PROGRESS' = 'IN_PROGRESS',
  'FINISHED' = 'FINISHED'
}

export type OnlineStatus = 'ONLINE' | 'OFFLINE' | 'UNKNOWN';

export type SkillNumberMap = Partial<Record<AllSkillNames, number>>;

export interface IPlayerQuestStatus {
  id: string;
  status: QuestStatus;
  startedDate?: number;
  finishedDate?: number;
}

class PlayerQuestStatus implements IPlayerQuestStatus {
  constructor(questId: string) {
    this.id = questId;
  }

  finishedDate?: number;
  id: string;
  startedDate?: number;

  get status(): QuestStatus {
    if (this.finishedDate != null) return QuestStatus.FINISHED;
    if (this.startedDate != null) return QuestStatus.IN_PROGRESS;
    return QuestStatus.NOT_STARTED;
  }
}

const EQUIPMENT = 'equipment';
const INVENTORY = 'inventory';
const BANK = 'bank';
const GRAND_EXCHANGE = 'grand_exchange';
const LOCATION = 'location';
const QUESTS = 'quests';
const HIGHSCORE_HISTORY = 'hiscore_history';
const XP = 'xp';
const ONLINE_STATUS = 'online_status';
const SEED_VAULT = 'seed_vault';
const STORAGE_SERVICE = 'storage_service';

class PlayerInformationDataSource extends DataSource {
  getLocation = async (playerId: string): Promise<Location | null> => {
    logger.debug(`Getting location for player with id ${playerId}`);
    return this._getDataFromCache<Location | null>(
      PlayerInformationDataSource._createCacheKey(playerId, LOCATION),
      null
    );
  };

  setLocation = (playerId: string, location: Location) => {
    logger.debug(`Setting location for player with id ${playerId}`);
    this._updateActivity(playerId);
    const cacheKey = PlayerInformationDataSource._createCacheKey(
      playerId,
      LOCATION
    );
    // TODO: NO HISTORY
    return this._setDataInCacheWithHistory(cacheKey, location);
  };

  getOnlineStatus = async (playerId: string): Promise<OnlineStatus> => {
    const cacheKey = PlayerInformationDataSource._createCacheKey(
      playerId,
      ONLINE_STATUS
    );
    const status = (await redis.get(cacheKey)) as OnlineStatus;
    return status || 'UNKNOWN';
  };

  setOnlineStatus = async (
    playerId: string,
    status: OnlineStatus
  ): Promise<void> => {
    const cacheKey = PlayerInformationDataSource._createCacheKey(
      playerId,
      ONLINE_STATUS
    );
    if (status === 'UNKNOWN') {
      await redis.del(cacheKey);
    } else if (status === 'ONLINE') {
      // Set a TTL of 15 minutes for the online status. This should reset on every other set in this class which denotes activity
      await redis.set(cacheKey, status, 'EX', 15 * 60);
    } else {
      await redis.set(cacheKey, status);
    }
  };

  getInventory = async (playerId: string): Promise<ItemContainer> => {
    logger.debug(`Getting inventory for player with id ${playerId}`);
    return this._getDataFromCache<ItemContainer>(
      PlayerInformationDataSource._createCacheKey(playerId, INVENTORY),
      []
    );
  };

  setInventory = (playerId: string, inventory: ItemContainer) => {
    logger.debug(`Setting inventory for player with id ${playerId}`);
    this._updateActivity(playerId);
    const cacheKey = PlayerInformationDataSource._createCacheKey(
      playerId,
      INVENTORY
    );
    return this._setDataInCacheWithHistory(cacheKey, inventory);
  };

  getEquipment = async (playerId: string): Promise<ItemContainer> => {
    logger.debug(`Getting equipment for player with id ${playerId}`);
    return this._getDataFromCache<ItemContainer>(
      PlayerInformationDataSource._createCacheKey(playerId, EQUIPMENT),
      []
    );
  };

  setEquipment = async (
    playerId: string,
    equipment: ItemContainer
  ): Promise<void> => {
    logger.debug(`Settings equipment for player with id ${playerId}`);
    this._updateActivity(playerId);
    const cacheKey = PlayerInformationDataSource._createCacheKey(
      playerId,
      EQUIPMENT
    );
    return this._setDataInCacheWithHistory(cacheKey, equipment);
  };

  getBank = async (playerId: string): Promise<ItemContainer> => {
    logger.debug(`Getting bank for player with id ${playerId}`);
    return this._getDataFromCache<ItemContainer>(
      PlayerInformationDataSource._createCacheKey(playerId, BANK),
      []
    );
  };

  setBank = async (playerId: string, bank: ItemContainer): Promise<void> => {
    logger.debug(`Setting bank for player with id ${playerId}`);
    this._updateActivity(playerId);
    const cacheKey = PlayerInformationDataSource._createCacheKey(
      playerId,
      BANK
    );
    return this._setDataInCacheWithHistory(cacheKey, bank);
  };

  getSeedVault = async (playerId: string): Promise<ItemContainer> => {
    logger.debug(`Getting seed vault for player with id ${playerId}`);
    return this._getDataFromCache<ItemContainer>(
      PlayerInformationDataSource._createCacheKey(playerId, SEED_VAULT),
      []
    );
  };

  setSeedVault = async (
    playerId: string,
    vault: ItemContainer
  ): Promise<void> => {
    logger.debug(`Setting seed vault for player with id ${playerId}`);
    this._updateActivity(playerId);
    const cacheKey = PlayerInformationDataSource._createCacheKey(
      playerId,
      SEED_VAULT
    );
    return this._setDataInCacheWithHistory(cacheKey, vault);
  };

  getStorageServices = async (playerId: string): Promise<StorageService[]> => {
    logger.debug(`Getting storage services for player with id ${playerId}`);
    const cacheKey = PlayerInformationDataSource._createCacheKey(
      playerId,
      STORAGE_SERVICE
    );
    const rawData = await redis.hgetall(cacheKey);
    if (!rawData) return [];
    return Object.values(rawData).map(data => {
      return <StorageService>JSON.parse(data);
    });
  };

  setStorageService = async (
    playerId: string,
    storageService: string,
    type: StorageServiceType,
    items: ItemContainer
  ): Promise<void> => {
    logger.debug(
      `Setting storage service ${storageService} for player with id ${playerId}`
    );
    this._updateActivity(playerId);
    const cacheKey = PlayerInformationDataSource._createCacheKey(
      playerId,
      STORAGE_SERVICE
    );

    await redis.hset(
      cacheKey,
      storageService,
      JSON.stringify({
        type,
        items,
        name: storageService
      })
    );
  };

  getGrandExchangeOffers = async (
    playerId: string
  ): Promise<GrandExchangeOffer[]> => {
    logger.debug(`Getting GE offers for player with id ${playerId}`);
    return this._getDataFromCache<GrandExchangeOffer[]>(
      PlayerInformationDataSource._createCacheKey(playerId, GRAND_EXCHANGE),
      []
    );
  };

  setGrandExchangeOffers = async (
    playerId: string,
    geOffers: GrandExchangeOffer[]
  ): Promise<void> => {
    logger.debug(`Setting GE offers for player with id ${playerId}`);

    this._updateActivity(playerId);
    const cacheKey = PlayerInformationDataSource._createCacheKey(
      playerId,
      GRAND_EXCHANGE
    );
    return this._setDataInCacheWithHistory(cacheKey, geOffers);
  };

  getQuestStatus = async (
    playerId: string,
    questId: string
  ): Promise<IPlayerQuestStatus | null> => {
    logger.debug(`Getting quest statuses for player with id ${playerId}`);

    const legacyKey = PlayerInformationDataSource._createCacheKey(
      playerId,
      QUESTS
    );
    const newKey = legacyKey + `/${questId}`;

    const element = await redis.hgetall(newKey);
    if (!_.isEmpty(element)) {
      const status = new PlayerQuestStatus(questId);
      status.startedDate = Number(element.startedDate) || undefined;
      status.finishedDate = Number(element.finishedDate) || undefined;
      return status;
    } else {
      // Legacy handling - saves status, has no dates for started/finished
      const questsString = await redis.get(legacyKey);
      if (!questsString) return null;
      const legacyQuests = <IPlayerQuestStatus[]>JSON.parse(questsString);
      return legacyQuests.find(q => q.id === questId) || null;
    }
  };

  setQuestStatuses = async (
    playerId: string,
    questStatuses: IPlayerQuestStatus[]
  ): Promise<void> => {
    logger.debug(`Setting quest statuses for player with id ${playerId}`);
    this._updateActivity(playerId);

    const cacheKey = PlayerInformationDataSource._createCacheKey(
      playerId,
      QUESTS
    );

    await Promise.all(
      questStatuses.map(async quest => {
        const key = cacheKey + `/${quest.id}`;

        // If the status flips back to not started for whatever reason, delete the value
        if (quest.status === QuestStatus.NOT_STARTED) {
          return redis.del(key);
        }
        if (quest.status === QuestStatus.IN_PROGRESS) {
          return redis.hsetnx(key, 'startedDate', Date.now());
        }
        if (quest.status === QuestStatus.FINISHED) {
          return redis.hsetnx(key, 'finishedDate', Date.now());
        }
      })
    );

    // return this._setDataInCacheWithHistory(cacheKey, questStatuses);
  };

  addXpDataPoint = async (
    playerId: string,
    highscore: HiscoreResult | Promise<HiscoreResult>
  ): Promise<void> => {
    const key = PlayerInformationDataSource._createCacheKey(
      playerId,
      HIGHSCORE_HISTORY
    );

    const highscorePromise = Promise.resolve(highscore);
    const lastPromise = redis.lindex(key, 0);

    const [highscoreResult, lastXpString] = await Promise.all([
      highscorePromise,
      lastPromise
    ]);
    const lastXp = JSON.parse(lastXpString);

    const hiscoreItem = mapHighscore(highscoreResult);

    if (_.isEqual(lastXp, hiscoreItem)) {
      return;
    }

    await redis.lpush(key, JSON.stringify(hiscoreItem));
  };

  // TODO: WIPE THIS ON LOGOUT, DATA SHOULD BE FETCHED FROM OSRS API INSTEAD
  setCurrentXp = async (
    playerId: string,
    skill: AllSkillNames,
    xp: number
  ): Promise<void> => {
    logger.debug(
      `Setting current "${skill}" xp for player with id ${playerId}`
    );
    this._updateActivity(playerId);
    const cacheKey = PlayerInformationDataSource._createCacheKey(playerId, XP);
    await redis.hmset(cacheKey, skill, xp);
  };

  async getCurrentXp(playerId: string): Promise<SkillNumberMap>;
  async getCurrentXp(
    playerId: string,
    skill?: AllSkillNames
  ): Promise<SkillNumberMap | number | undefined> {
    logger.debug(
      `Getting current ${
        skill ? `${skill} xp` : 'xp for all skills'
      } for player with id ${playerId}`
    );
    const cacheKey = PlayerInformationDataSource._createCacheKey(playerId, XP);
    if (skill) {
      const data = await redis.hget(cacheKey, skill);
      return !data ? undefined : parseInt(data);
    } else {
      const data = await redis.hgetall(cacheKey);
      return Object.entries(data).reduce((current, kvp) => {
        return {
          ...current,
          [kvp[0]]: parseInt(kvp[1])
        };
      }, {});
    }
  }

  _getDataFromCache = async <T>(
    cacheKey: string,
    defaultValue: T
  ): Promise<T> => {
    const legacyCacheItem = await this._getFirstCacheItem<T>(cacheKey);
    if (!legacyCacheItem) return defaultValue;

    const cacheItem = legacyCacheItem as CacheItem<T>;
    const legacyObject = legacyCacheItem as T;
    return (cacheItem && cacheItem.item) || legacyObject || defaultValue;
  };

  _setDataInCacheWithHistory = async <T>(
    cacheKey: string,
    object: T
  ): Promise<void> => {
    const cacheObject: CacheItem<T> = {
      item: object,
      date: Date.now()
    };
    const objectJson = JSON.stringify(cacheObject);

    // TODO: GETTING ERROR IF ATTEMPTING TO OVERWRITE EXISTING LEGACY STRING KEY

    const currentItem = await this._getFirstCacheItem<CacheItemWithLegacy<T>>(
      cacheKey
    );
    const currentCacheItem = currentItem as CacheItem<T>;
    if (
      currentItem &&
      currentCacheItem.date &&
      isToday(currentCacheItem.date)
    ) {
      logger.trace(`Updating current days value at '${cacheKey}'`);
      // TODO: TRIM LIST TO PREVENT STORING TOO MUCH DATA
      await redis.lset(cacheKey, 0, objectJson);
    } else {
      logger.trace(`No data for today at key ${cacheKey}. Adding new value.`);
      await redis.lpush(cacheKey, objectJson);
    }
  };

  async _getFirstCacheItem<T>(
    cacheKey: string
  ): Promise<CacheItemWithLegacy<T> | null> {
    /*
     TODO: ERROR HANDLING?
     TODO: REMOVE LEGACY HANDLING ONCE ALL DATA HAS BEEN MIGRATED. I.E. THIS TYPE CHECK
               POSSIBLY BY CHECKING AT STARTUP THAT ALL KEYS UNDER PLAYER/./. ARE ALL LISTS?
    */
    const keyType = await redis.type(cacheKey);

    let currentItemString;
    let migrationRequired = false;

    switch (keyType) {
      case 'none':
        // Do nothing if there is no key yet
        break;
      case 'string':
        currentItemString = await redis.get(cacheKey);
        migrationRequired = true;
        break;
      case 'list':
        currentItemString = await redis.lindex(cacheKey, 0);
        break;
      default:
        throw new Error(
          `Key at '${cacheKey}' has an unknown type and cannot be read`
        );
    }

    if (!currentItemString) return null;

    const object = <T>JSON.parse(currentItemString);

    if (migrationRequired) {
      await redis.del(cacheKey);
    }

    return object;
  }

  _updateActivity(playerId: string) {
    return this.setOnlineStatus(playerId, 'ONLINE');
  }

  static _createCacheKey(playerId: string, ...dataType: string[]) {
    return generateCacheKey(`player/${playerId}/${dataType.join('/')}`);
  }
}

export default PlayerInformationDataSource;
