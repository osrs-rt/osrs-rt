import { DataSource, DataSourceConfig } from 'apollo-datasource';
import createLogger  from '../../logger';
import { Semaphore } from 'await-semaphore/index';
import osrsApi, { HiscoreResult, Item } from 'osrs-api';
import { generateCacheKey } from '../cacheUtils';
import { KeyValueCache } from 'apollo-server-caching';

const logger = createLogger('osrs-tools:graphql_server:OsrsApiDataSource');

class OsrsApiDataSource extends DataSource {
  private _semaphore: Semaphore;
  private _cache: KeyValueCache | null = null;

  constructor() {
    super();
    this._semaphore = new Semaphore(10);
  }

  initialize(config: DataSourceConfig<any>): void {
    this._cache = config.cache;
  }

  async getItem(id: number) {
    const cacheKey = generateCacheKey(`item/${id}/ge`);
    let itemJson = this._cache && await this._cache.get(cacheKey);
    let item: Item;
    if (itemJson) {
      logger.trace(`Cache hit for item with key ${cacheKey}`);
      return JSON.parse(itemJson);
    }

    logger.trace(`Cache miss for item with key ${cacheKey}`);

    const release = await this._semaphore.acquire();
    try {
      logger.debug(
        `Getting item details from OSRS GE API for item with id ${id}`
      );
      item = (await osrsApi.grandExchange.getItem(id)).item;
      if (!itemJson) {
        throw new Error(
          `Unexpectedly got back undefined from OSRS GE API for item with id ${id}`
        );
      }

      logger.debug(
        `Successfully retrieved data from OSRS GE API for item with id ${id}`
      );
    } catch (error) {
      logger.error(
        `Failed to get item details from OSRS GE API for item with id ${id}: ${
          error.message
        }`
      );

      throw error;
    } finally {
      release();
    }

    this._cache && await this._cache.set(cacheKey, JSON.stringify(item), {
      ttl: 3600
    });
    return item;
  }

  getHighscore(playerId: string): Promise<HiscoreResult> {
    logger.debug(`Getting hiscore from OSRS API with playerId ${playerId}`);
    return osrsApi.hiscores
      .getPlayer({
        name: playerId
      })
      .then((data: HiscoreResult) => {
        if (!data.type) {
          data.type = null;
        }
        return data;
      });
  }
}

export default OsrsApiDataSource;
