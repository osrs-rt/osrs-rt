import { DataSource, DataSourceConfig } from 'apollo-datasource';
import createLogger from '../../logger';
import { getItems, RSBuddyItemsResponse } from '../../rsbuddyApiClient';
import { generateCacheKey } from '../cacheUtils';

const logger = createLogger('osrs-tools:graphql_server:rsbuddyApiDataSource');

class RSBuddyApiDataSource extends DataSource {
  private _memCache: Promise<RSBuddyItemsResponse> | null = null;

  async initialize(config: DataSourceConfig<any>) {
    logger.trace('initialised a new rsbuddy api data source');
    this._memCache = new Promise(async resolve => {
      const key = generateCacheKey('rsbuddy');

      const existingItems = await config.cache.get(key);
      if (existingItems) {
        logger.debug('Got existing items from the cache');
        resolve(JSON.parse(existingItems));
        return;
      }

      const items = await getItems();
      await config.cache.set(key, JSON.stringify(items), { ttl: 30 * 60 });
      resolve(items);
    });
  }

  async getItem(id: number) {
    if (!this._memCache) {
      return null;
    }

    const result = await this._memCache;
    return result[id];
  }
}

export default RSBuddyApiDataSource;
