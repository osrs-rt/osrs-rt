import { AllSkillNames } from "../../../utils/sharedTypes";
import { instantQuery, rangeQuery } from "../../prometheusApiClient";
import { differenceInSeconds } from "date-fns";

/**
 *
 * @param player The player to get data for
 * @param start  The start date to get data for
 * @param end    The end date to get data for
 * @param step  Either a textual representation of a time span (i.e. 1h, 30m or 7d) or a number representing the number of seconds in the time span
 */
export async function getHistoricalXp(
  player: string,
  start: Date,
  end: Date,
  step: string | number = '1h'
): Promise<HistoricalXp> {
  const data = await rangeQuery(
    `osrs_xp{ player="${player}" }`,
    start,
    end,
    step
  );
  return data.data.result.reduce<Partial<HistoricalXp>>((current, next) => {
    const skill = next.metric.skill;

    current[skill.toLowerCase() as AllSkillNames] = next.values.map(value => ({
      date: value[0] * 1000,
      value: parseInt(value[1].toString(), 10)
    }));
    return current;
  }, {}) as HistoricalXp;
}

export type HistoricalXp = Record<
  AllSkillNames,
  Array<{ date: number; value: number }>
>;

export async function getXpDelta(
  player: string,
  startDate: Date,
  endDate: Date
): Promise<Record<AllSkillNames, number>> {
  const timeSpan = differenceInSeconds(endDate, startDate);
  const query = `delta(osrs_xp { player="${player}" }[${timeSpan}s])`;
  const data = await instantQuery(query, endDate);
  return data.data.result.reduce((current, next) => {
    current[next.metric.skill.toLowerCase() as AllSkillNames] = parseInt(next.value[1], 10);
    return current;
  }, {} as Record<AllSkillNames, number>);
}
