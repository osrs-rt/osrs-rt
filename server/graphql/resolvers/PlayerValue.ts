import { getCurrentPrice } from '../rsBuddyRepository';
import sum from 'lodash/sum';
import { Context } from './context';
import createLogger from '../../logger';
import { ItemContainer } from '../dataSources/PlayerInformationDataSource';
import RSBuddyApiDataSource from '../dataSources/RSBuddyApiDataSource';
import { getPrice as getRuneLitePrice } from '../runeliteRepository';
import { getItemById } from '../itemRepository';
import { flatten } from 'lodash';

const logger = createLogger('osrs-tools:graphql_server:resolvers:player_value');

export type PriceSource = 'HIGH_ALCH' | 'RUNELITE' | 'RSBUDDY';
type Parent = {
  player: string;
  source: PriceSource;
};

function getPrice(
  itemId: number,
  source: PriceSource,
  rsBuddyApiDataSource: RSBuddyApiDataSource
): Promise<number | null> {
  switch (source) {
    case 'RSBUDDY':
      return getCurrentPrice(itemId, rsBuddyApiDataSource);

    case 'HIGH_ALCH':
      return getItemById(itemId).then(item => item.highalch || null);

    case 'RUNELITE':
    default:
      return getRuneLitePrice(itemId);
  }
}

async function getSumOfItemPrices(
  items: ItemContainer,
  rsbuddyApi: RSBuddyApiDataSource,
  source: PriceSource
) {
  const pricePromises = items.map(async item => {
    const price = await getPrice(item.id, source, rsbuddyApi);
    return (price || 0) * (item.quantity || 0);
  });
  const prices = await Promise.all(pricePromises);
  return sum(prices);
}

export async function bank(
  parent: Parent,
  args: any,
  { dataSources }: Context
): Promise<number> {
  logger.debug(`Getting bank value for player ${parent.player}`);
  const items = await dataSources.player.getBank(parent.player);
  return getSumOfItemPrices(items, dataSources.rsbuddyApi, parent.source);
}

export async function seedVault(
  parent: Parent,
  args: any,
  { dataSources }: Context
): Promise<number> {
  logger.debug(`Getting seed vault value for player ${parent.player}`);
  const items = await dataSources.player.getSeedVault(parent.player);
  return getSumOfItemPrices(items, dataSources.rsbuddyApi, parent.source);
}

export async function equipment(
  parent: Parent,
  args: any,
  { dataSources }: Context
): Promise<number> {
  logger.debug(`Getting equipment value for player ${parent.player}`);
  const items = await dataSources.player.getEquipment(parent.player);
  return getSumOfItemPrices(items, dataSources.rsbuddyApi, parent.source);
}

export async function inventory(
  parent: Parent,
  args: any,
  { dataSources }: Context
): Promise<number> {
  logger.debug(`Getting inventory value for player ${parent.player}`);
  const items = await dataSources.player.getInventory(parent.player);
  return getSumOfItemPrices(items, dataSources.rsbuddyApi, parent.source);
}

export async function grandExchange(
  parent: Parent,
  args: any,
  { dataSources }: Context
): Promise<number> {
  logger.debug(`Getting GE value for player ${parent.player}`);
  const offers = await dataSources.player.getGrandExchangeOffers(parent.player);
  const offerValuePromises = offers.map(async offer => {
    const quantityLeftToTransfer =
      offer.totalQuantity - offer.transferredQuantity;
    const price: number | null = await (offer.state !== 'SELLING' &&
    offer.state !== 'SOLD'
      ? Promise.resolve(offer.pricePerItem)
      : getPrice(offer.itemId, parent.source, dataSources.rsbuddyApi));

    const wealthLeftToTransfer = (price || 0) * quantityLeftToTransfer;
    return wealthLeftToTransfer + offer.wealthTransferred;
  });

  const offerValues = await Promise.all(offerValuePromises);
  return sum(offerValues);
}

export async function other(
  parent: Parent,
  args: any,
  { dataSources }: Context
): Promise<number> {
  logger.debug(`Getting other value for player ${parent.player}`);
  const storageServices = await dataSources.player.getStorageServices(
    parent.player
  );
  const items = flatten(storageServices.map(s => s.items));
  return getSumOfItemPrices(items, dataSources.rsbuddyApi, parent.source);
}

export async function total(
  parent: Parent,
  args: any,
  context: Context
): Promise<number> {
  logger.debug(`Getting total value for player ${parent.player}`);
  const prices = await Promise.all([
    bank(parent, args, context),
    equipment(parent, args, context),
    inventory(parent, args, context),
    grandExchange(parent, args, context),
    seedVault(parent, args, context),
    other(parent, args, context)
  ]);

  return sum(prices);
}
