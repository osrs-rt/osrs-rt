import OsrsApiDataSource from '../dataSources/OsrsApiDataSource';
import CrystalMathLabsDataSource from '../dataSources/CrystalMathLabsDataSource';
import PlayerInformationDataSource from '../dataSources/PlayerInformationDataSource';
import RSBuddyApiDataSource from '../dataSources/RSBuddyApiDataSource';

export type Context = {
  dataSources: {
    osrsApi: OsrsApiDataSource;
    crystalMethLabsApi: CrystalMathLabsDataSource;
    player: PlayerInformationDataSource;
    rsbuddyApi: RSBuddyApiDataSource;
  };
};
