import { removeUndefinedProperties } from './propertyUtils';

describe('removeUndefinedProperties', () => {
  it('should remove all undefined properties from passed object', () => {
    const input = {
      a: 'one',
      b: undefined
    };

    const actual = removeUndefinedProperties(input);

    expect(actual).not.toHaveProperty('b');
    expect(actual).toEqual({ a: 'one' });
  });
});
