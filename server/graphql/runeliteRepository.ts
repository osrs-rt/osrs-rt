import createLogger from '../logger';
import { InMemoryLRUCache } from 'apollo-server-caching';
import redis from './redis';
import fetch from 'node-fetch';
import prettyBytes from 'pretty-bytes';
import { generateCacheKey } from './cacheUtils';
import { getItemById, Item } from './itemRepository';
import {
  getBaseItemOfVariant,
  getMappedItems,
  itemToItemPriceMap
} from '../itemUtils';
import { COINS_995, PLATINUM_TOKEN } from '../itemIds';
import wornItems from '../wornItems';
import sum from 'lodash/sum';

type PriceMap = {
  [itemId: number]: number;
};

const logger = createLogger('osrs-tools:graphql_server:runeliteRepository');
const cache = new InMemoryLRUCache<PriceMap>();
let getDatabaseLock: Promise<PriceMap> | undefined;

export async function getPrice(itemId: number): Promise<number | null> {
  if (itemId == COINS_995) return 1;
  if (itemId == PLATINUM_TOKEN) return 1000;

  const itemDef: Item = await getItemById(itemId);

  // Use the real item if dealing with a note or placeholder
  if (itemDef.linked_id_item) {
    itemId = itemDef.linked_id_item;
  }
  itemId = wornItems[itemId] || itemId;

  const baseItem = getBaseItemOfVariant(itemId) || itemId;
  const mappedItem = itemToItemPriceMap[baseItem];
  if (mappedItem) {
    const price = await getPrice(mappedItem.item);
    return price == null ? null : price * mappedItem.quantity;
  }

  const mappedIds = getMappedItems(itemId);
  if (mappedIds.length > 1) {
    logger.debug('Found item that is made of multiple prices: ', itemDef.name);
  }

  const allItemPrices = await getItems();
  const prices = mappedIds.map(id => allItemPrices[id] || 0);
  return sum(prices);

  //
  // let price = null;
  // if (isCoins(itemId)) {
  //   price = 1;
  // } else {
  //   price = (await getItems())[itemId];
  // }
  // return price;
}

function downloadDatabase(cacheKey: string): Promise<PriceMap> {
  if (getDatabaseLock) return getDatabaseLock;

  getDatabaseLock = new Promise<PriceMap>(async resolve => {
    let items: PriceMap;

    // get them from the remote cache if they exist
    let itemsString = await redis.get(cacheKey);
    if (itemsString) {
      // TODO: Check ttl and download a fresh copy of the database if nearly expired
      logger.debug('Loaded runelite item price list from remote cache');
      items = JSON.parse(itemsString) as PriceMap;
      if (items) {
        // Save the items to the local cache
        await cache.set(cacheKey, items, { ttl: 7200 });
        resolve(items);
        return;
      }
    }

    logger.trace(
      'Unable to find runelite item prices database in remote cache. Downloading a fresh copy of the database.'
    );

    const runeliteVersion = await getRuneliteVersion();
    const url = `http://api.runelite.net/runelite-${runeliteVersion}/item/prices`;

    const response = await fetch(url);
    items = (await response.json()).reduce(
      (
        cur: PriceMap,
        item: {
          id: number;
          name: string;
          price: number;
          time: { seconds: number; nanos: number };
        }
      ) => {
        cur[item.id] = item.price;
        return cur;
      },
      {}
    );
    if (!items)
      throw new Error('Unable to retrieve price list from runelite api');

    const numItems = Object.keys(items).length;
    const contentLength = parseInt(
      response.headers.get('Content-Length') || '0',
      10
    );
    logger.debug(
      `Downloaded price list from runelite (${prettyBytes(
        contentLength
      )}, ${numItems} items wit prices found)`
    );

    itemsString = JSON.stringify(items);
    await Promise.all([
      redis.setex(cacheKey, 7200, itemsString),
      cache.set(cacheKey, items, { ttl: 7200 })
    ]);
    getDatabaseLock = undefined;
    resolve(items);
  });
  return getDatabaseLock;
}

async function getRuneliteVersion(): Promise<string> {
  logger.trace('Getting version number of latest runelite client');
  const response = await fetch('https://static.runelite.net/bootstrap.json');
  const json = await response.json();
  return json.client.version;
}

async function getItems(): Promise<PriceMap> {
  const cacheKey = generateCacheKey('runelite/prices');

  // get the items from the local cache if they exist
  let items = await cache.get(cacheKey);
  if (items) {
    logger.debug('Loaded item database from local cache');
    return items;
  }

  logger.trace(
    'Unable to find item database in local cache, attempting to load from remote cache.'
  );

  // if not, we will need to grab them from the server and save them to both caches
  return downloadDatabase(cacheKey);
}
