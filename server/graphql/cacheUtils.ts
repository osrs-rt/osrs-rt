export function generateCacheKey(key: string): string {
  if (!key.startsWith('/')) {
    key = '/' + key;
  }

  return 'osrs-tools' + key.toLowerCase();
}
