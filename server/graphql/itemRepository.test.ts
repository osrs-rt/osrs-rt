import mockDb from '../../__mocks__/items.json';
import { searchForItems, getItemById } from './itemRepository';

jest.mock('./redis', () => {
  return {
    get: jest.fn(() => {
      return Promise.resolve(JSON.stringify(mockDb));
    }),
    setex: jest.fn()
  };
});

describe('getItemById', () => {
  it('should return the item from the database with the given id', async () => {
    const actual = await getItemById(1);

    expect(actual).toStrictEqual(mockDb['1']);
    expect(actual).toBeDefined();
  });
});

describe('searchForItems', () => {
  it('should filter out non-tradeable items if passed tradeable option set to true', async () => {
    const items = await searchForItems({
      tradeable: true
    });

    expect(items).toEqual([mockDb['2']]);
  });

  it('should filter out non-member items if passed member option set to true', async () => {
    const items = await searchForItems({
      member: true
    });

    expect(items).toEqual([mockDb['1']]);
  });

  it('should return all items with ids that match a passed array', async () => {
    const items = await searchForItems({
      id: [1, 2]
    });

    expect(items).toEqual([mockDb['1'], mockDb['2']]);
  });

  it('should not return items that have no high alch value when searching for alchable items', async function() {
    const items = await searchForItems({
      alchable: true
    });

    expect(items).toEqual([mockDb['1']]);
  });

  it('should not return items that have do have a high alch value when searching for non-alchable items', async function() {
    const items = await searchForItems({
      alchable: false
    });

    expect(items).toEqual([mockDb['2'], mockDb['3']]);
  });
});
