import { generateCacheKey } from './cacheUtils';
import fetch from 'node-fetch';
import prettyBytes from 'pretty-bytes';
import createLogger from '../logger';
import { removeUndefinedProperties } from './propertyUtils';
import { InMemoryLRUCache } from 'apollo-server-caching';
import redis from './redis';

interface ItemList {
  [id: number]: Item;
}

interface Item2 {
  /** Unique OSRS item ID number. */

  id: number;
  /** The name of the item. */
  name: string;
  /** If the item has incomplete wiki data. */
  incomplete: boolean;
  /** If the item is a members-only. */
  members: boolean;
  /** If the item is tradeable (between players and on the GE). */
  tradeable: boolean;
  /** If the item is tradeable (only on GE). */
  tradeable_on_ge: boolean;
  /** If the item is stackable (in inventory). */
  stackable: boolean;
  /** If the item is stacked, indicated by the stack count. */
  stacked?: number;
  /** If the item is noted. */
  noted: boolean;
  /** If the item is noteable. */
  noteable: boolean;
  /** The linked ID of the actual item (if noted/placeholder). */
  linked_id_item?: number;
  /** The linked ID of an item in noted form. */
  linked_id_noted?: number;
  /** The linked ID of an item in placeholder form. */
  linked_id_placeholder?: number;
  /** If the item is a placeholder. */
  placeholder: boolean;
  /** If the item is equipable (based on right-click menu entry). */
  equipable: boolean;
  /** If the item is equipable in-game by a player. */
  equipable_by_player: boolean;
  /** If the item is an equipable weapon. */
  equipable_weapon: boolean;
  /** The store price of an item. */
  cost: number;
  /** The low alchemy value of the item (cost * 0.4). */
  lowalch?: number;
  /** The high alchemy value of the item (cost * 0.6). */
  highalch?: number;
  /** The weight (in kilograms) of the item. */
  weight?: number;
  /** The Grand Exchange buy limit of the item. */
  buy_limit?: number;
  /** If the item is associated with a quest. */
  quest_item: boolean;
  /** Date the item was released (in ISO8601 format). */
  release_date?: string;
  /** If the item is a duplicate. */
  duplicate: boolean;
  /** The examine text for the item. */
  examine?: string;
  /** The item icon (in base64 encoding). */
  icon: string;
  /** The OSRS Wiki name for the item. */
  wiki_name?: string;
  /** The OSRS Wiki URL (possibly including anchor link). */
  wiki_url?: string;
  /** The OSRS Wiki Exchange URL. */
  wiki_exchnage?: string;
  /** The equipment bonuses of equipable armour/weapons. */
  equipment?: any;
  /** The weapon bonuses including attack speed, type and stance. */
  weapon?: any;
}

type BaseItem = {
  name: string;
  members: boolean;
  tradeable: boolean;
  stackable: boolean;
  noted: boolean;
  noteable: boolean;
  linked_id_item: number;
  equipable: boolean;
  cost: number;

  weight: number;
  buy_limit: number;
  quest_item: string;
  release_date: string;
  examine: string;
  url: string;

  [index: string]: any;
};

export type Item = Item2;

type NumberSearch =
  | number
  | {
      eq: number;
      neq: number;
      gt: number;
      gte: number;
      lt: number;
      lte: number;
    };

export type ItemQueryOptions = Partial<BaseItem> & {
  alchable?: boolean;
  id?: number | number[];
  lowalch?: NumberSearch;
  highalch?: NumberSearch;
};

const logger = createLogger('osrs-tools:graphql_server:itemRepository');
const cache = new InMemoryLRUCache<ItemList>();
let getDatabaseLock: Promise<ItemList> | undefined;

export async function getItemById(id: number): Promise<Item> {
  return (await getItems())[id];
}

function downloadDatabase(cacheKey: string): Promise<ItemList> {
  if (getDatabaseLock) return getDatabaseLock;

  getDatabaseLock = new Promise<ItemList>(async resolve => {
    let items: ItemList;

    // get them from the remote cache if they exist
    let itemsString = await redis.get(cacheKey);
    if (itemsString) {
      // TODO: Check ttl and download a fresh copy of the database if nearly expired
      logger.debug('Loaded item database from remote cache');
      items = JSON.parse(itemsString) as ItemList;
      if (items) {
        // Save the items to the local cache
        await cache.set(cacheKey, items, { ttl: 7200 });
        resolve(items);
        return;
      }
    }

    logger.trace(
      'Unable to find item database in remote cache. Downloading a fresh copy of the database.'
    );

    const response = await fetch(
      'https://raw.githubusercontent.com/osrsbox/osrsbox-db/master/docs/items-complete.json'
    );
    items = (await response.json()) as ItemList;
    if (!items) throw new Error('Unable to retrieve items from OSRSbox');

    const numItems = Object.keys(items).length;
    const contentLength = parseInt(
      response.headers.get('Content-Length') || '0',
      10
    );
    logger.debug(
      `Downloaded item database from OSRSbx (${prettyBytes(
        contentLength
      )}, ${numItems} items definitions found)`
    );

    itemsString = JSON.stringify(items);
    await Promise.all([
      redis.setex(cacheKey, 7200, itemsString),
      cache.set(cacheKey, items, { ttl: 7200 })
    ]);
    getDatabaseLock = undefined;
    resolve(items);
  });
  return getDatabaseLock;
}

async function getItems(): Promise<ItemList> {
  const cacheKey = generateCacheKey('item-definitions');

  // get the items from the local cache if they exist
  let items = await cache.get(cacheKey);
  if (items) {
    logger.debug('Loaded item database from local cache');
    return items;
  }

  logger.trace(
    'Unable to find item database in local cache, attempting to load from remote cache.'
  );

  // if not, we will need to grab them from the server and save them to both caches
  return downloadDatabase(cacheKey);
}

export async function searchForItems(
  searchProps: ItemQueryOptions
): Promise<Array<Item>> {
  searchProps = removeUndefinedProperties(searchProps);

  // TODO: Store each item under it's own key in the cache for getById, store the entire object list as an array
  const itemsByValue = Object.values(await getItems());

  return itemsByValue.filter(item => {
    return Object.entries(searchProps).every(([searchKey, searchValue]) => {
      if (searchProps.alchable != null) {
        if (
          (searchProps.alchable && !item.highalch) ||
          (!searchProps.alchable && item.highalch > 0)
        ) {
          return false;
        }
      }

      if (!item.hasOwnProperty(searchKey) || searchValue == null) return true;

      const itemValue = item[searchKey];

      if (Array.isArray(searchValue)) {
        return searchValue.some(val => itemValue == val);
      }

      if (typeof searchValue === 'object') {
        if (searchValue.eq != null) {
          return itemValue === searchValue.eq;
        }
        if (searchValue.neq != null) {
          return itemValue !== searchValue.neq;
        }
        if (searchValue.gt != null) {
          return itemValue > searchValue.gt;
        }
        if (searchValue.gte != null) {
          return itemValue >= searchValue.gte;
        }
        if (searchValue.lt != null) {
          return itemValue < searchValue.lt;
        }
        if (searchValue.lte != null) {
          return itemValue <= searchValue.lte;
        }
      }

      return itemValue === searchValue;
    });
  });
}
