import { RedisOptions, Redis } from 'ioredis';
import { RedisCache } from 'apollo-server-cache-redis';
import createLogger from '../logger';

const logger = createLogger('osrs-tools:graphql_server:redis');

const redisHost = process.env.redis_host || 'localhost';
const redisPort = Number(process.env.redis_port) || undefined;
const redisPassword = process.env.redis_password || undefined;
const redisDatabase = Number(process.env.redis_database) || undefined;
const useSsl = Boolean(process.env.redis_ssl) || false;
const redisOptions: RedisOptions = {
  host: redisHost,
  port: redisPort,
  password: redisPassword,
  db: redisDatabase,
  tls: useSsl as any
};

const redisCache = new RedisCache(redisOptions);
logger.info(`Starting apollo server with redis cache options:`, redisOptions);

export { redisCache };
export default redisCache.client as Redis;
