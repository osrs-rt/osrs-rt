export function removeUndefinedProperties<T extends { [p: string]: any }>(properties: T): Partial<T> {
  return Object.entries(properties).reduce((cur, [key, value]) => {
    if (value === undefined) return cur;

    return {
      ...cur,
      [key]: value
    };
  }, {});
}