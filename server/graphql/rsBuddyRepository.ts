import createLogger from '../logger';
import { getItemById, Item } from './itemRepository';
import { isCoins } from '../../sharedCode/itemUtils';
import RSBuddyApiDataSource from './dataSources/RSBuddyApiDataSource';

const logger = createLogger('osrs-tools:graphql_server:rsBuddyRepository');

function canItemBeTraded(item: Item): boolean {
  return isCoins(item.id) || (item && item.tradeable);
}

export async function getCurrentPrice(itemId: number, api: RSBuddyApiDataSource): Promise<number | null> {
  logger.debug(`Getting current RSBuddy price for item with id ${itemId}`);

  const itemDef: Item = await getItemById(itemId);

  if (!await canItemBeTraded(itemDef)) {
    logger.debug(`Item with id ${itemId} is not tradeable so has no value`);
    return null;
  }

  // Use the real item if dealing with a note or placeholder
  if (itemDef.linked_id_item) {
    itemId = itemDef.linked_id_item;
  }

  let price = null;
  if (isCoins(itemId)) {
    price = 1;
  } else {
    const item = await api.getItem(itemId);
    if (!item) {
      logger.warn(`Unable to find item with id ${itemId} in rsbuddy database`);
      return null;
    }

    price = item.overall_average;
  }

  logger.debug(`RSBuddy price for item with id ${itemId} is ${price}`);
  return price;
}
