import { AllSkillNames } from '../utils/sharedTypes';
import fetch from 'cross-fetch';

function toUnixTimestamp(date: Date) {
  return Math.round(date.getTime() / 1000);
}

if (!process.env.PROMETHEUS_URL) {
  throw new Error(
    'The PROMETHEUS_URL parameter has not been set. This should point to the Prometheus server used for storing time series data'
  );
}

const BASE_URL = new URL('api/v1/', process.env.PROMETHEUS_URL);

const INSTANT_QUERY_URL = new URL('query', BASE_URL);
const RANGE_QUERY_URL = new URL('query_range', BASE_URL);

type ResultType = 'matrix' | 'vector' | 'scalar' | 'string';

interface PrometheusResponse<TData extends { resultType: ResultType }> {
  status: string;
  data: TData;
}

interface PrometheusInstantData {
  resultType: ResultType;
  result: Array<{
    metric: Metric;
    value: [number, string];
  }>;
}

interface PrometheusRangeData {
  resultType: 'matrix';
  result: Array<{
    metric: Metric;
    values: Array<[number, string]>;
  }>;
}

export interface Metric {
  __name__: string;
  instance: string;
  job: string;
  player: string;
  skill: AllSkillNames;
}

export function instantQuery(
  query: string,
  evaluationTime: Date
): Promise<PrometheusResponse<PrometheusInstantData>> {
  const url = new URL(INSTANT_QUERY_URL.toString());
  url.searchParams.set('query', query);
  url.searchParams.set('time', toUnixTimestamp(evaluationTime).toString());
  const urlString = url.toString();
  return fetch(urlString).then(response => response.json());
}

export function rangeQuery(
  query: string,
  start: Date,
  end: Date,
  step: string | number
): Promise<PrometheusResponse<PrometheusRangeData>> {
  const url = new URL(RANGE_QUERY_URL.toString());
  url.searchParams.set('query', query);
  url.searchParams.set('start', toUnixTimestamp(start).toString());
  url.searchParams.set('end', toUnixTimestamp(end).toString());

  if (step) {
    url.searchParams.set('step', step.toString());
  }
  const urlString = url.toString();
  return fetch(urlString).then(response => response.json());
}
