import ApolloClient from 'apollo-boost';
import query from './metrics.graphql';
import { Gauge, Registry } from 'prom-client';
import { AllSkillNames } from '../../utils/sharedTypes';
import { constants } from 'osrs-api';
import { getLevelForExperience } from '../../sharedCode/experience';
import { OnlineStatus } from '../graphql/dataSources/PlayerInformationDataSource';

export interface MetricData {
  contentType: string;
  metrics: string;
}

interface PlayerQueryResult {
  player: {
    xp: Record<AllSkillNames, number>;
    onlineStatus: OnlineStatus;
    value?: {
      bank: number;
      seedVault: number;
      equipment: number;
      grandExchange: number;
      inventory: number;
      total: number;
    };
  };
}

function addValueMetric(
  name: string,
  registry: Registry,
  helpTextOverride?: string
) {
  new Gauge({
    name: `${name}_value`,
    help: helpTextOverride || `The value of the players ${name}`,
    registers: [registry],
    labelNames: ['player']
  });
  // gauge.remove();
}

function createRegistry(): Registry {
  const registry = new Registry();

  new Gauge({
    name: 'online',
    help: 'Whether the player is currently online or not',
    registers: [registry],
    labelNames: ['player']
  });

  addValueMetric('bank', registry);
  addValueMetric('inventory', registry);
  addValueMetric('equipment', registry);
  addValueMetric('seed_vault', registry);
  addValueMetric('grand_exchange', registry);
  addValueMetric('total', registry, 'The players total value');

  constants.ingameSkillOrder.forEach(skill => {
    new Gauge({
      name: `xp_${skill}`,
      help: `Current XP for ${skill} skill`,
      registers: [registry],
      labelNames: ['player']
    });

    if (skill !== 'overall') {
      new Gauge({
        name: `level_${skill}`,
        help: `Current level for ${skill} skill`,
        registers: [registry],
        labelNames: ['player']
      });
    }
  });

  return registry;
}

export interface RequestedMetrics {
  onlineStatus?: any;
  xp?: any;
  level?: any;
  value?: any;
}

// TODO: CHANGE TO OPT-IN SO ONLY CERTAIN METRICS ARE INCLUDED. Configurable via GET params
export default async function getMetricData(
  client: ApolloClient<any>,
  players: string | Array<string>,
  requestedMetrics: RequestedMetrics
): Promise<MetricData> {
  const playersList: Array<string> =
    typeof players === 'string' ? [players] : players;

  const register = createRegistry();

  await Promise.all(
    playersList.map(async player => {
      const data = await client.query<PlayerQueryResult>({
        query,

        variables: {
          player,
          includeValue: !!requestedMetrics.value
        },

        fetchPolicy: 'no-cache'
      });

      if (requestedMetrics.value && data.data.player.value) {
        (register.getSingleMetric('bank_value') as Gauge<string>).set(
          { player },
          data.data.player.value.bank
        );
        (register.getSingleMetric('seed_vault_value') as Gauge<string>).set(
          { player },
          data.data.player.value.seedVault
        );
        (register.getSingleMetric('equipment_value') as Gauge<string>).set(
          { player },
          data.data.player.value.equipment
        );
        (register.getSingleMetric('inventory_value') as Gauge<string>).set(
          { player },
          data.data.player.value.inventory
        );
        (register.getSingleMetric('grand_exchange_value') as Gauge<string>).set(
          { player },
          data.data.player.value.grandExchange
        );
        (register.getSingleMetric('total_value') as Gauge<string>).set(
          { player },
          data.data.player.value.total
        );
      }

      Object.entries(data.data.player.xp).map(([skill, xp]) => {
        const skillName = skill.toLowerCase();
        const xpGauge = register.getSingleMetric(`xp_${skillName}`) as Gauge<
          string
        >;
        if (xpGauge && xp && xpGauge) {
          xpGauge.set({ player }, xp);
        }

        const levelGauge = register.getSingleMetric(
          `level_${skillName}`
        ) as Gauge<string>;
        if (xp && skill !== 'overall' && levelGauge) {
          const level = getLevelForExperience(xp);
          levelGauge.set({ player }, level);
        }
      });

      const onlineGauge = register.getSingleMetric('online') as Gauge<string>;
      const online = data.data.player.onlineStatus === 'ONLINE' ? 1 : 0;
      onlineGauge.set({ player }, online);
    })
  );

  return {
    contentType: register.contentType,
    metrics: register.metrics()
  };
}
