import query from './getItem.graphql'
import ApolloClient from 'apollo-client';

export async function getItem(client: ApolloClient<any>, itemId: string) {
    const data = await client.query<any>({
      query,

      variables: {
        id: itemId,
      },

      fetchPolicy: 'no-cache'
    });

    if (data.errors) {
      throw new Error(data.errors.join('\n'));
    }

    return data.data;
}
