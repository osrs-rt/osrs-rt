import {
  BlobItem,
  BlobServiceClient,
  StorageSharedKeyCredential
} from '@azure/storage-blob';
import createLogger from '../logger';

const logger = createLogger('osrs-tools:graphql_server:BlobStorageClient');

export interface BlobDownload {
  filename: string;
  stream?: NodeJS.ReadableStream;
  contentLength?: number;
  contentDisposition?: string;
  etag?: string;
}

function createBlobServiceClient() {
  const account = process.env.AZURE_STORAGE_ACCOUNT || '';
  const accountKey = process.env.AZURE_STORAGE_KEY || '';

  logger.debug(
    `Connecting to blob storage using account and key: ${account}, ${accountKey}`
  );
  const sharedKeyCredential = new StorageSharedKeyCredential(
    account,
    accountKey
  );
  return new BlobServiceClient(
    `https://${account}.blob.core.windows.net`,
    sharedKeyCredential
  );
}

export async function getLatestRuneliteBlob(): Promise<
  BlobItem | undefined
> {
  const container = process.env.AZURE_STORAGE_CONTAINER || '';
  const blobService = createBlobServiceClient();
  const containerClient = blobService.getContainerClient(container);

  let latest: BlobItem | undefined;
  const iterator = await containerClient.listBlobsFlat();
  for await (const blob of iterator) {
    if (!latest) {
      latest = blob;
    } else if (blob.properties.lastModified > latest.properties.lastModified) {
      latest = blob;
    }
  }
  return latest;
}

export async function getBlobItemDownload(blob: BlobItem): Promise<BlobDownload> {
  const container = process.env.AZURE_STORAGE_CONTAINER || '';
  const blobService = createBlobServiceClient();
  const containerClient = blobService.getContainerClient(container);
  const blobClient = containerClient.getBlobClient(blob.name);
  const download = await blobClient.download();
  return {
    filename: blob.name,
    stream: download.readableStreamBody,
    contentLength: blob.properties.contentLength,
    etag: blob.properties.etag,
    contentDisposition: blob.properties.contentDisposition,
  };
}