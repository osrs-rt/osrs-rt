import ApolloClient from 'apollo-boost';
import query from './metrics.graphql';
import { Gauge, Registry } from 'prom-client';
import { AllSkillNames } from '../../utils/sharedTypes';
import { constants } from 'osrs-api';
import { getLevelForExperience } from '../../sharedCode/experience';
import { OnlineStatus } from '../graphql/dataSources/PlayerInformationDataSource';
import { startCase } from 'lodash';

export interface MetricData {
  contentType: string;
  metrics: string;
}

interface PlayerQueryResult {
  player: {
    xp: Record<AllSkillNames, number>;
    onlineStatus: OnlineStatus;
    value?: {
      bank: number;
      seedVault: number;
      equipment: number;
      grandExchange: number;
      inventory: number;
      other: number;
      total: number;
    };
  };
}

function createRegistry(): Registry {
  const registry = new Registry();

  // New gauges. Old will be deprecated
  new Gauge({
    name: 'osrs_online',
    help: 'Whether the player is currently online or not',
    registers: [registry],
    labelNames: ['player']
  });

  new Gauge({
    name: `osrs_value`,
    help: `The estimated value of a given container`,
    registers: [registry],
    labelNames: ['player', 'container']
  });

  new Gauge({
    name: `osrs_xp`,
    help: `Current XP for a given skill`,
    registers: [registry],
    labelNames: ['player', 'skill']
  });

  new Gauge({
    name: `osrs_level`,
    help: `Current level for a given skill`,
    registers: [registry],
    labelNames: ['player', 'skill']
  });

  return registry;
}

export interface RequestedMetrics {
  onlineStatus?: any;
  xp?: any;
  level?: any;
  value?: any;
}

// TODO: CHANGE TO OPT-IN SO ONLY CERTAIN METRICS ARE INCLUDED. Configurable via GET params
export default async function getMetricData(
  client: ApolloClient<any>,
  players: string | Array<string>,
  requestedMetrics: RequestedMetrics
): Promise<MetricData> {
  const playersList: Array<string> =
    typeof players === 'string' ? [players] : players;

  const register = createRegistry();

  await Promise.all(
    playersList.map(async player => {
      const data = await client.query<PlayerQueryResult>({
        query,
        variables: {
          player,
          includeValue: !!requestedMetrics.value
        },
        fetchPolicy: 'no-cache'
      });

      if (requestedMetrics.value && data.data.player.value) {
        const valueMetric = register.getSingleMetric('osrs_value') as Gauge<
          string
        >;

        valueMetric.set(
          { player, container: 'Bank' },
          data.data.player.value.bank
        );
        valueMetric.set(
          { player, container: 'Seed Vault' },
          data.data.player.value.seedVault
        );
        valueMetric.set(
          { player, container: 'Equipment' },
          data.data.player.value.equipment
        );
        valueMetric.set(
          { player, container: 'Inventory' },
          data.data.player.value.inventory
        );
        valueMetric.set(
          { player, container: 'Grand Exchange' },
          data.data.player.value.grandExchange
        );
        valueMetric.set(
          { player, container: 'Other' },
          data.data.player.value.other
        );
        valueMetric.set(
          { player, container: 'Total' },
          data.data.player.value.total
        );
      }

      const xpGauge = register.getSingleMetric(`osrs_xp`) as Gauge<string>;
      const levelGauge = register.getSingleMetric(`osrs_level`) as Gauge<
        string
      >;
      Object.keys(constants.skills).map((skill) => {
        // const skillName = skill.toLowerCase();
        const skillName = startCase(skill);
        const xp = data.data.player.xp[skill as AllSkillNames];
        if (xpGauge && xp) {
          xpGauge.set({ player, skill: skillName }, xp);
        }

        if (xp && skill !== 'overall') {
          const level = getLevelForExperience(xp);
          levelGauge.set({ player, skill: skillName }, level);
        }
      });

      const onlineGauge = register.getSingleMetric('osrs_online') as Gauge<string>;
      const online = data.data.player.onlineStatus === 'ONLINE' ? 1 : 0;
      onlineGauge.set({ player }, online);
    })
  );

  return {
    contentType: register.contentType,
    metrics: register.metrics()
  };
}
