const logLevels = {
  error: 0,
  warn: 1,
  info: 2,
  verbose: 3,
  debug: 4,
  silly: 5,
};

export type LogFunction = (...args: any) => void;

export type LogLevels = keyof typeof logLevels;
export type ILogger = {
  error: LogFunction;
  warn: LogFunction;
  info: LogFunction;
  debug: LogFunction;
  trace: LogFunction;
};


const debug = require('debug');

let logLevel = logLevels['info'];

const envLevel = process.env.DEBUG_LEVEL;
if (envLevel) {
  const lower = envLevel.toLowerCase() as LogLevels;
  if (logLevels[lower] != null) {
    logLevel = logLevels[lower];
  }
}


export default function createLogger(namespace: string): ILogger {
  const logger = debug(namespace);

  function generateLogFunc(logFuncLevel: keyof typeof logLevels) {
    return (...args: any) => {
      const levelAsNum = logLevels[logFuncLevel];
      if (levelAsNum > logLevel) {
        return;
      }

      logger(...args)
    }
  }

  return {
    trace: generateLogFunc("silly"),
    debug: generateLogFunc("debug"),
    info: generateLogFunc("info"),
    warn: generateLogFunc("warn"),
    error: generateLogFunc("error")
  }
}
