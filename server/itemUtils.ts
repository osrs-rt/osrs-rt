import * as ItemID from './itemIds';
import VARIATIONS from '../databases/itemVariations.json';
import { MultiDictionary } from 'typescript-collections';

export type IdMap = { [id: number]: number };

const variationMap: IdMap = Object.values(VARIATIONS).reduce(
  (current, [base, ...variations]) => {
    variations.forEach(id => {
      current[id] = base;
    });
    return current;
  },
  {} as IdMap
);
export function getBaseItemOfVariant(itemId: number): number | undefined {
  return variationMap[itemId];
}

export interface UntradableItemMap {
  [id: number]: {
    item: number;
    quantity: number;
  };
}

export const itemToItemPriceMap: UntradableItemMap = {
  [ItemID.MARK_OF_GRACE]: { item: ItemID.AMYLASE_CRYSTAL, quantity: 10 },
  [ItemID.GRACEFUL_HOOD]: { item: ItemID.MARK_OF_GRACE, quantity: 28 },
  [ItemID.GRACEFUL_TOP]: { item: ItemID.MARK_OF_GRACE, quantity: 44 },
  [ItemID.GRACEFUL_LEGS]: { item: ItemID.MARK_OF_GRACE, quantity: 48 },
  [ItemID.GRACEFUL_GLOVES]: { item: ItemID.MARK_OF_GRACE, quantity: 24 },
  [ItemID.GRACEFUL_BOOTS]: { item: ItemID.MARK_OF_GRACE, quantity: 32 },
  [ItemID.GRACEFUL_CAPE]: { item: ItemID.MARK_OF_GRACE, quantity: 32 },

  // 10 golden nuggets = 100 soft clay
  [ItemID.GOLDEN_NUGGET]: { item: ItemID.SOFT_CLAY, quantity: 10 },
  [ItemID.PROSPECTOR_HELMET]: { item: ItemID.GOLDEN_NUGGET, quantity: 32 },
  [ItemID.PROSPECTOR_JACKET]: { item: ItemID.GOLDEN_NUGGET, quantity: 48 },
  [ItemID.PROSPECTOR_LEGS]: { item: ItemID.GOLDEN_NUGGET, quantity: 40 },
  [ItemID.PROSPECTOR_BOOTS]: { item: ItemID.GOLDEN_NUGGET, quantity: 24 },

  [ItemID.CRYSTAL_HELM]: { item: ItemID.CRYSTAL_ARMOUR_SEED, quantity: 1 },
  [ItemID.CRYSTAL_HELM_INACTIVE]: {
    item: ItemID.CRYSTAL_ARMOUR_SEED,
    quantity: 1
  },
  [ItemID.CRYSTAL_LEGS]: { item: ItemID.CRYSTAL_ARMOUR_SEED, quantity: 2 },
  [ItemID.CRYSTAL_LEGS_INACTIVE]: {
    item: ItemID.CRYSTAL_ARMOUR_SEED,
    quantity: 2
  },
  [ItemID.CRYSTAL_BODY]: { item: ItemID.CRYSTAL_ARMOUR_SEED, quantity: 3 },
  [ItemID.CRYSTAL_BODY_INACTIVE]: {
    item: ItemID.CRYSTAL_ARMOUR_SEED,
    quantity: 3
  },

  [ItemID.TATTERED_MOON_PAGE]: { item: ItemID.COINS_995, quantity: 1000 },
  [ItemID.TATTERED_SUN_PAGE]: { item: ItemID.COINS_995, quantity: 1000 },
  [ItemID.TATTERED_TEMPLE_PAGE]: { item: ItemID.COINS_995, quantity: 1000 },

  [ItemID.LONG_BONE]: { item: ItemID.COINS_995, quantity: 1000 },
  [ItemID.CURVED_BONE]: { item: ItemID.COINS_995, quantity: 2000 },
  [ItemID.PERFECT_SHELL]: { item: ItemID.COINS_995, quantity: 600 },
  [ItemID.PERFECT_SNAIL_SHELL]: { item: ItemID.COINS_995, quantity: 600 },
  [ItemID.SNAIL_SHELL]: { item: ItemID.COINS_995, quantity: 600 },
  [ItemID.TORTOISE_SHELL]: { item: ItemID.COINS_995, quantity: 250 }
};

// TODO: ABOVE HAS DUPLICATE DECLARATIONS. IT IS OVERWRITING ORIGINAL. NEED TO USE A MULTIHSAH OR SOMETHING LIKE IN JAVA CODE

const untradeableItemMap = new MultiDictionary<number, Array<number>>();
untradeableItemMap.setValue(ItemID.AHRIMS_HOOD, [
  ItemID.AHRIMS_HOOD_25,
  ItemID.AHRIMS_HOOD_50,
  ItemID.AHRIMS_HOOD_75,
  ItemID.AHRIMS_HOOD_100
]);

type IdMappings = { [id: number]: Array<number> };
untradeableItemMap.setValue(ItemID.AHRIMS_HOOD, [
  ItemID.AHRIMS_HOOD_25,
  ItemID.AHRIMS_HOOD_50,
  ItemID.AHRIMS_HOOD_75,
  ItemID.AHRIMS_HOOD_100
]);
untradeableItemMap.setValue(ItemID.AHRIMS_ROBETOP, [
  ItemID.AHRIMS_ROBETOP_25,
  ItemID.AHRIMS_ROBETOP_50,
  ItemID.AHRIMS_ROBETOP_75,
  ItemID.AHRIMS_ROBETOP_100
]);
untradeableItemMap.setValue(ItemID.AHRIMS_ROBESKIRT, [
  ItemID.AHRIMS_ROBESKIRT_25,
  ItemID.AHRIMS_ROBESKIRT_50,
  ItemID.AHRIMS_ROBESKIRT_75,
  ItemID.AHRIMS_ROBESKIRT_100
]);
untradeableItemMap.setValue(ItemID.AHRIMS_STAFF, [
  ItemID.AHRIMS_STAFF_25,
  ItemID.AHRIMS_STAFF_50,
  ItemID.AHRIMS_STAFF_75,
  ItemID.AHRIMS_STAFF_100
]);
untradeableItemMap.setValue(ItemID.KARILS_COIF, [
  ItemID.KARILS_COIF_25,
  ItemID.KARILS_COIF_50,
  ItemID.KARILS_COIF_75,
  ItemID.KARILS_COIF_100
]);
untradeableItemMap.setValue(ItemID.KARILS_LEATHERTOP, [
  ItemID.KARILS_LEATHERTOP_25,
  ItemID.KARILS_LEATHERTOP_50,
  ItemID.KARILS_LEATHERTOP_75,
  ItemID.KARILS_LEATHERTOP_100
]);
untradeableItemMap.setValue(ItemID.KARILS_LEATHERSKIRT, [
  ItemID.KARILS_LEATHERSKIRT_25,
  ItemID.KARILS_LEATHERSKIRT_50,
  ItemID.KARILS_LEATHERSKIRT_75,
  ItemID.KARILS_LEATHERSKIRT_100
]);
untradeableItemMap.setValue(ItemID.KARILS_CROSSBOW, [
  ItemID.KARILS_CROSSBOW_25,
  ItemID.KARILS_CROSSBOW_50,
  ItemID.KARILS_CROSSBOW_75,
  ItemID.KARILS_CROSSBOW_100
]);
untradeableItemMap.setValue(ItemID.DHAROKS_HELM, [
  ItemID.DHAROKS_HELM_25,
  ItemID.DHAROKS_HELM_50,
  ItemID.DHAROKS_HELM_75,
  ItemID.DHAROKS_HELM_100
]);
untradeableItemMap.setValue(ItemID.DHAROKS_PLATEBODY, [
  ItemID.DHAROKS_PLATEBODY_25,
  ItemID.DHAROKS_PLATEBODY_50,
  ItemID.DHAROKS_PLATEBODY_75,
  ItemID.DHAROKS_PLATEBODY_100
]);
untradeableItemMap.setValue(ItemID.DHAROKS_PLATELEGS, [
  ItemID.DHAROKS_PLATELEGS_25,
  ItemID.DHAROKS_PLATELEGS_50,
  ItemID.DHAROKS_PLATELEGS_75,
  ItemID.DHAROKS_PLATELEGS_100
]);
untradeableItemMap.setValue(ItemID.DHAROKS_GREATAXE, [
  ItemID.DHAROKS_GREATAXE_25,
  ItemID.DHAROKS_GREATAXE_50,
  ItemID.DHAROKS_GREATAXE_75,
  ItemID.DHAROKS_GREATAXE_100
]);
untradeableItemMap.setValue(ItemID.GUTHANS_HELM, [
  ItemID.GUTHANS_HELM_25,
  ItemID.GUTHANS_HELM_50,
  ItemID.GUTHANS_HELM_75,
  ItemID.GUTHANS_HELM_100
]);
untradeableItemMap.setValue(ItemID.GUTHANS_PLATEBODY, [
  ItemID.GUTHANS_PLATEBODY_25,
  ItemID.GUTHANS_PLATEBODY_50,
  ItemID.GUTHANS_PLATEBODY_75,
  ItemID.GUTHANS_PLATEBODY_100
]);
untradeableItemMap.setValue(ItemID.GUTHANS_CHAINSKIRT, [
  ItemID.GUTHANS_CHAINSKIRT_25,
  ItemID.GUTHANS_CHAINSKIRT_50,
  ItemID.GUTHANS_CHAINSKIRT_75,
  ItemID.GUTHANS_CHAINSKIRT_100
]);
untradeableItemMap.setValue(ItemID.GUTHANS_WARSPEAR, [
  ItemID.GUTHANS_WARSPEAR_25,
  ItemID.GUTHANS_WARSPEAR_50,
  ItemID.GUTHANS_WARSPEAR_75,
  ItemID.GUTHANS_WARSPEAR_100
]);
untradeableItemMap.setValue(ItemID.TORAGS_HELM, [
  ItemID.TORAGS_HELM_25,
  ItemID.TORAGS_HELM_50,
  ItemID.TORAGS_HELM_75,
  ItemID.TORAGS_HELM_100
]);
untradeableItemMap.setValue(ItemID.TORAGS_PLATEBODY, [
  ItemID.TORAGS_PLATEBODY_25,
  ItemID.TORAGS_PLATEBODY_50,
  ItemID.TORAGS_PLATEBODY_75,
  ItemID.TORAGS_PLATEBODY_100
]);
untradeableItemMap.setValue(ItemID.TORAGS_PLATELEGS, [
  ItemID.TORAGS_PLATELEGS_25,
  ItemID.TORAGS_PLATELEGS_50,
  ItemID.TORAGS_PLATELEGS_75,
  ItemID.TORAGS_PLATELEGS_100
]);
untradeableItemMap.setValue(ItemID.TORAGS_HAMMERS, [
  ItemID.TORAGS_HAMMERS_25,
  ItemID.TORAGS_HAMMERS_50,
  ItemID.TORAGS_HAMMERS_75,
  ItemID.TORAGS_HAMMERS_100
]);
untradeableItemMap.setValue(ItemID.VERACS_HELM, [
  ItemID.VERACS_HELM_25,
  ItemID.VERACS_HELM_50,
  ItemID.VERACS_HELM_75,
  ItemID.VERACS_HELM_100
]);
untradeableItemMap.setValue(ItemID.VERACS_BRASSARD, [
  ItemID.VERACS_BRASSARD_25,
  ItemID.VERACS_BRASSARD_50,
  ItemID.VERACS_BRASSARD_75,
  ItemID.VERACS_BRASSARD_100
]);
untradeableItemMap.setValue(ItemID.VERACS_PLATESKIRT, [
  ItemID.VERACS_PLATESKIRT_25,
  ItemID.VERACS_PLATESKIRT_50,
  ItemID.VERACS_PLATESKIRT_75,
  ItemID.VERACS_PLATESKIRT_100
]);
untradeableItemMap.setValue(ItemID.VERACS_FLAIL, [
  ItemID.VERACS_FLAIL_25,
  ItemID.VERACS_FLAIL_50,
  ItemID.VERACS_FLAIL_75,
  ItemID.VERACS_FLAIL_100
]);

// Dragon equipment ornament kits
untradeableItemMap.setValue(ItemID.DRAGON_SCIMITAR, [
  ItemID.DRAGON_SCIMITAR_OR
]);
untradeableItemMap.setValue(ItemID.DRAGON_SCIMITAR_ORNAMENT_KIT, [
  ItemID.DRAGON_SCIMITAR_OR
]);
untradeableItemMap.setValue(ItemID.DRAGON_DEFENDER_ORNAMENT_KIT, [
  ItemID.DRAGON_DEFENDER_T
]);
untradeableItemMap.setValue(ItemID.DRAGON_PICKAXE, [
  ItemID.DRAGON_PICKAXE_12797,
  ItemID.DRAGON_PICKAXEOR
]);
untradeableItemMap.setValue(ItemID.ZALCANO_SHARD, [ItemID.DRAGON_PICKAXEOR]);
untradeableItemMap.setValue(ItemID.DRAGON_KITESHIELD, [
  ItemID.DRAGON_KITESHIELD_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_KITESHIELD_ORNAMENT_KIT, [
  ItemID.DRAGON_KITESHIELD_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_FULL_HELM, [
  ItemID.DRAGON_FULL_HELM_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_FULL_HELM_ORNAMENT_KIT, [
  ItemID.DRAGON_FULL_HELM_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_CHAINBODY_3140, [
  ItemID.DRAGON_CHAINBODY_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_CHAINBODY_ORNAMENT_KIT, [
  ItemID.DRAGON_CHAINBODY_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_PLATEBODY, [
  ItemID.DRAGON_PLATEBODY_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_PLATEBODY_ORNAMENT_KIT, [
  ItemID.DRAGON_PLATEBODY_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_PLATESKIRT, [
  ItemID.DRAGON_PLATESKIRT_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_LEGSSKIRT_ORNAMENT_KIT, [
  ItemID.DRAGON_PLATESKIRT_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_PLATELEGS, [
  ItemID.DRAGON_PLATELEGS_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_LEGSSKIRT_ORNAMENT_KIT, [
  ItemID.DRAGON_PLATELEGS_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_SQ_SHIELD, [
  ItemID.DRAGON_SQ_SHIELD_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_SQ_SHIELD_ORNAMENT_KIT, [
  ItemID.DRAGON_SQ_SHIELD_G
]);
untradeableItemMap.setValue(ItemID.DRAGON_BOOTS, [ItemID.DRAGON_BOOTS_G]);
untradeableItemMap.setValue(ItemID.DRAGON_BOOTS_ORNAMENT_KIT, [
  ItemID.DRAGON_BOOTS_G
]);

// Rune ornament kits
untradeableItemMap.setValue(ItemID.RUNE_SCIMITAR, [ItemID.RUNE_SCIMITAR_23330]);
untradeableItemMap.setValue(ItemID.RUNE_SCIMITAR_ORNAMENT_KIT_GUTHIX, [
  ItemID.RUNE_SCIMITAR_23330
]);
untradeableItemMap.setValue(ItemID.RUNE_SCIMITAR, [ItemID.RUNE_SCIMITAR_23332]);
untradeableItemMap.setValue(ItemID.RUNE_SCIMITAR_ORNAMENT_KIT_SARADOMIN, [
  ItemID.RUNE_SCIMITAR_23332
]);
untradeableItemMap.setValue(ItemID.RUNE_SCIMITAR, [ItemID.RUNE_SCIMITAR_23334]);
untradeableItemMap.setValue(ItemID.RUNE_SCIMITAR_ORNAMENT_KIT_ZAMORAK, [
  ItemID.RUNE_SCIMITAR_23334
]);
untradeableItemMap.setValue(ItemID.RUNE_DEFENDER, [ItemID.RUNE_DEFENDER_T]);
untradeableItemMap.setValue(ItemID.RUNE_DEFENDER_ORNAMENT_KIT, [
  ItemID.RUNE_DEFENDER_T
]);

// Godsword ornament kits
untradeableItemMap.setValue(ItemID.ARMADYL_GODSWORD, [
  ItemID.ARMADYL_GODSWORD_OR
]);
untradeableItemMap.setValue(ItemID.ARMADYL_GODSWORD_ORNAMENT_KIT, [
  ItemID.ARMADYL_GODSWORD_OR
]);
untradeableItemMap.setValue(ItemID.BANDOS_GODSWORD, [
  ItemID.BANDOS_GODSWORD_OR
]);
untradeableItemMap.setValue(ItemID.BANDOS_GODSWORD_ORNAMENT_KIT, [
  ItemID.BANDOS_GODSWORD_OR
]);
untradeableItemMap.setValue(ItemID.ZAMORAK_GODSWORD, [
  ItemID.ZAMORAK_GODSWORD_OR
]);
untradeableItemMap.setValue(ItemID.ZAMORAK_GODSWORD_ORNAMENT_KIT, [
  ItemID.ZAMORAK_GODSWORD_OR
]);
untradeableItemMap.setValue(ItemID.SARADOMIN_GODSWORD, [
  ItemID.SARADOMIN_GODSWORD_OR
]);
untradeableItemMap.setValue(ItemID.SARADOMIN_GODSWORD_ORNAMENT_KIT, [
  ItemID.SARADOMIN_GODSWORD_OR
]);

// Jewellery ornament kits
untradeableItemMap.setValue(ItemID.AMULET_OF_TORTURE, [
  ItemID.AMULET_OF_TORTURE_OR
]);
untradeableItemMap.setValue(ItemID.TORTURE_ORNAMENT_KIT, [
  ItemID.AMULET_OF_TORTURE_OR
]);
untradeableItemMap.setValue(ItemID.NECKLACE_OF_ANGUISH, [
  ItemID.NECKLACE_OF_ANGUISH_OR
]);
untradeableItemMap.setValue(ItemID.ANGUISH_ORNAMENT_KIT, [
  ItemID.NECKLACE_OF_ANGUISH_OR
]);
untradeableItemMap.setValue(ItemID.OCCULT_NECKLACE, [
  ItemID.OCCULT_NECKLACE_OR
]);
untradeableItemMap.setValue(ItemID.OCCULT_ORNAMENT_KIT, [
  ItemID.OCCULT_NECKLACE_OR
]);
untradeableItemMap.setValue(ItemID.AMULET_OF_FURY, [
  ItemID.AMULET_OF_FURY_OR,
  ItemID.AMULET_OF_BLOOD_FURY
]);
untradeableItemMap.setValue(ItemID.FURY_ORNAMENT_KIT, [
  ItemID.AMULET_OF_FURY_OR
]);
untradeableItemMap.setValue(ItemID.TORMENTED_BRACELET, [
  ItemID.TORMENTED_BRACELET_OR
]);
untradeableItemMap.setValue(ItemID.TORMENTED_ORNAMENT_KIT, [
  ItemID.TORMENTED_BRACELET_OR
]);
untradeableItemMap.setValue(ItemID.BERSERKER_NECKLACE, [
  ItemID.BERSERKER_NECKLACE_OR
]);
untradeableItemMap.setValue(ItemID.BERSERKER_NECKLACE_ORNAMENT_KIT, [
  ItemID.BERSERKER_NECKLACE_OR
]);

// Ensouled heads
untradeableItemMap.setValue(ItemID.ENSOULED_GOBLIN_HEAD_13448, [
  ItemID.ENSOULED_GOBLIN_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_MONKEY_HEAD_13451, [
  ItemID.ENSOULED_MONKEY_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_IMP_HEAD_13454, [
  ItemID.ENSOULED_IMP_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_MINOTAUR_HEAD_13457, [
  ItemID.ENSOULED_MINOTAUR_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_SCORPION_HEAD_13460, [
  ItemID.ENSOULED_SCORPION_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_BEAR_HEAD_13463, [
  ItemID.ENSOULED_BEAR_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_UNICORN_HEAD_13466, [
  ItemID.ENSOULED_UNICORN_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_DOG_HEAD_13469, [
  ItemID.ENSOULED_DOG_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_CHAOS_DRUID_HEAD_13472, [
  ItemID.ENSOULED_CHAOS_DRUID_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_GIANT_HEAD_13475, [
  ItemID.ENSOULED_GIANT_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_OGRE_HEAD_13478, [
  ItemID.ENSOULED_OGRE_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_ELF_HEAD_13481, [
  ItemID.ENSOULED_ELF_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_TROLL_HEAD_13484, [
  ItemID.ENSOULED_TROLL_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_HORROR_HEAD_13487, [
  ItemID.ENSOULED_HORROR_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_KALPHITE_HEAD_13490, [
  ItemID.ENSOULED_KALPHITE_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_DAGANNOTH_HEAD_13493, [
  ItemID.ENSOULED_DAGANNOTH_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_BLOODVELD_HEAD_13496, [
  ItemID.ENSOULED_BLOODVELD_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_TZHAAR_HEAD_13499, [
  ItemID.ENSOULED_TZHAAR_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_DEMON_HEAD_13502, [
  ItemID.ENSOULED_DEMON_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_AVIANSIE_HEAD_13505, [
  ItemID.ENSOULED_AVIANSIE_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_ABYSSAL_HEAD_13508, [
  ItemID.ENSOULED_ABYSSAL_HEAD
]);
untradeableItemMap.setValue(ItemID.ENSOULED_DRAGON_HEAD_13511, [
  ItemID.ENSOULED_DRAGON_HEAD
]);

// Imbued rings
untradeableItemMap.setValue(ItemID.BERSERKER_RING, [ItemID.BERSERKER_RING_I]);
untradeableItemMap.setValue(ItemID.SEERS_RING, [ItemID.SEERS_RING_I]);
untradeableItemMap.setValue(ItemID.WARRIOR_RING, [ItemID.WARRIOR_RING_I]);
untradeableItemMap.setValue(ItemID.ARCHERS_RING, [ItemID.ARCHERS_RING_I]);
untradeableItemMap.setValue(ItemID.TREASONOUS_RING, [ItemID.TREASONOUS_RING_I]);
untradeableItemMap.setValue(ItemID.TYRANNICAL_RING, [ItemID.TYRANNICAL_RING_I]);
untradeableItemMap.setValue(ItemID.RING_OF_THE_GODS, [
  ItemID.RING_OF_THE_GODS_I
]);
untradeableItemMap.setValue(ItemID.RING_OF_SUFFERING, [
  ItemID.RING_OF_SUFFERING_I,
  ItemID.RING_OF_SUFFERING_R,
  ItemID.RING_OF_SUFFERING_RI
]);
untradeableItemMap.setValue(ItemID.GRANITE_RING, [ItemID.GRANITE_RING_I]);

// Bounty hunter
untradeableItemMap.setValue(ItemID.GRANITE_MAUL, [ItemID.GRANITE_MAUL_12848]);
untradeableItemMap.setValue(ItemID.MAGIC_SHORTBOW, [ItemID.MAGIC_SHORTBOW_I]);
untradeableItemMap.setValue(ItemID.MAGIC_SHORTBOW_SCROLL, [
  ItemID.MAGIC_SHORTBOW_I
]);
untradeableItemMap.setValue(ItemID.SARADOMINS_TEAR, [
  ItemID.SARADOMINS_BLESSED_SWORD
]);

// Jewellery with charges
untradeableItemMap.setValue(ItemID.RING_OF_WEALTH, [
  ItemID.RING_OF_WEALTH_I,
  ItemID.RING_OF_WEALTH_1,
  ItemID.RING_OF_WEALTH_I1,
  ItemID.RING_OF_WEALTH_2,
  ItemID.RING_OF_WEALTH_I2,
  ItemID.RING_OF_WEALTH_3,
  ItemID.RING_OF_WEALTH_I3,
  ItemID.RING_OF_WEALTH_4,
  ItemID.RING_OF_WEALTH_I4,
  ItemID.RING_OF_WEALTH_I5
]);
untradeableItemMap.setValue(ItemID.RING_OF_WEALTH_SCROLL, [
  ItemID.RING_OF_WEALTH_I,
  ItemID.RING_OF_WEALTH_I1,
  ItemID.RING_OF_WEALTH_I2,
  ItemID.RING_OF_WEALTH_I3,
  ItemID.RING_OF_WEALTH_I4,
  ItemID.RING_OF_WEALTH_I5
]);
untradeableItemMap.setValue(ItemID.AMULET_OF_GLORY, [
  ItemID.AMULET_OF_GLORY1,
  ItemID.AMULET_OF_GLORY2,
  ItemID.AMULET_OF_GLORY3,
  ItemID.AMULET_OF_GLORY5
]);
untradeableItemMap.setValue(ItemID.AMULET_OF_GLORY_T, [
  ItemID.AMULET_OF_GLORY_T1,
  ItemID.AMULET_OF_GLORY_T2,
  ItemID.AMULET_OF_GLORY_T3,
  ItemID.AMULET_OF_GLORY_T5
]);
untradeableItemMap.setValue(ItemID.SKILLS_NECKLACE, [
  ItemID.SKILLS_NECKLACE1,
  ItemID.SKILLS_NECKLACE2,
  ItemID.SKILLS_NECKLACE3,
  ItemID.SKILLS_NECKLACE5
]);
untradeableItemMap.setValue(ItemID.RING_OF_DUELING8, [
  ItemID.RING_OF_DUELING1,
  ItemID.RING_OF_DUELING2,
  ItemID.RING_OF_DUELING3,
  ItemID.RING_OF_DUELING4,
  ItemID.RING_OF_DUELING5,
  ItemID.RING_OF_DUELING6,
  ItemID.RING_OF_DUELING7
]);
untradeableItemMap.setValue(ItemID.GAMES_NECKLACE8, [
  ItemID.GAMES_NECKLACE1,
  ItemID.GAMES_NECKLACE2,
  ItemID.GAMES_NECKLACE3,
  ItemID.GAMES_NECKLACE4,
  ItemID.GAMES_NECKLACE5,
  ItemID.GAMES_NECKLACE6,
  ItemID.GAMES_NECKLACE7
]);

// Degradable/charged weaponry/armour
untradeableItemMap.setValue(ItemID.ABYSSAL_WHIP, [
  ItemID.VOLCANIC_ABYSSAL_WHIP,
  ItemID.FROZEN_ABYSSAL_WHIP
]);
untradeableItemMap.setValue(ItemID.KRAKEN_TENTACLE, [ItemID.ABYSSAL_TENTACLE]);
untradeableItemMap.setValue(ItemID.UNCHARGED_TRIDENT, [
  ItemID.TRIDENT_OF_THE_SEAS
]);
untradeableItemMap.setValue(ItemID.UNCHARGED_TRIDENT_E, [
  ItemID.TRIDENT_OF_THE_SEAS_E
]);
untradeableItemMap.setValue(ItemID.UNCHARGED_TOXIC_TRIDENT, [
  ItemID.TRIDENT_OF_THE_SWAMP
]);
untradeableItemMap.setValue(ItemID.UNCHARGED_TOXIC_TRIDENT_E, [
  ItemID.TRIDENT_OF_THE_SWAMP_E
]);
untradeableItemMap.setValue(ItemID.TOXIC_BLOWPIPE_EMPTY, [
  ItemID.TOXIC_BLOWPIPE
]);
untradeableItemMap.setValue(ItemID.TOXIC_STAFF_UNCHARGED, [
  ItemID.TOXIC_STAFF_OF_THE_DEAD
]);
untradeableItemMap.setValue(ItemID.SERPENTINE_HELM_UNCHARGED, [
  ItemID.SERPENTINE_HELM,
  ItemID.TANZANITE_HELM_UNCHARGED,
  ItemID.TANZANITE_HELM,
  ItemID.MAGMA_HELM_UNCHARGED,
  ItemID.MAGMA_HELM
]);
untradeableItemMap.setValue(ItemID.DRAGONFIRE_SHIELD_11284, [
  ItemID.DRAGONFIRE_SHIELD
]);
untradeableItemMap.setValue(ItemID.DRAGONFIRE_WARD_22003, [
  ItemID.DRAGONFIRE_WARD
]);
untradeableItemMap.setValue(ItemID.ANCIENT_WYVERN_SHIELD_21634, [
  ItemID.ANCIENT_WYVERN_SHIELD
]);
untradeableItemMap.setValue(ItemID.SANGUINESTI_STAFF_UNCHARGED, [
  ItemID.SANGUINESTI_STAFF
]);
untradeableItemMap.setValue(ItemID.SCYTHE_OF_VITUR_UNCHARGED, [
  ItemID.SCYTHE_OF_VITUR
]);
untradeableItemMap.setValue(ItemID.TOME_OF_FIRE_EMPTY, [ItemID.TOME_OF_FIRE]);
untradeableItemMap.setValue(ItemID.CRAWS_BOW_U, [ItemID.CRAWS_BOW]);
untradeableItemMap.setValue(ItemID.VIGGORAS_CHAINMACE_U, [
  ItemID.VIGGORAS_CHAINMACE
]);
untradeableItemMap.setValue(ItemID.THAMMARONS_SCEPTRE_U, [
  ItemID.THAMMARONS_SCEPTRE
]);

// Infinity colour kits
untradeableItemMap.setValue(ItemID.INFINITY_TOP, [
  ItemID.INFINITY_TOP_10605,
  ItemID.INFINITY_TOP_20574,
  ItemID.DARK_INFINITY_TOP,
  ItemID.LIGHT_INFINITY_TOP
]);
untradeableItemMap.setValue(ItemID.LIGHT_INFINITY_COLOUR_KIT, [
  ItemID.LIGHT_INFINITY_TOP
]);
untradeableItemMap.setValue(ItemID.DARK_INFINITY_COLOUR_KIT, [
  ItemID.DARK_INFINITY_TOP
]);
untradeableItemMap.setValue(ItemID.INFINITY_BOTTOMS, [
  ItemID.INFINITY_BOTTOMS_20575,
  ItemID.DARK_INFINITY_BOTTOMS,
  ItemID.LIGHT_INFINITY_BOTTOMS
]);
untradeableItemMap.setValue(ItemID.LIGHT_INFINITY_COLOUR_KIT, [
  ItemID.LIGHT_INFINITY_BOTTOMS
]);
untradeableItemMap.setValue(ItemID.DARK_INFINITY_COLOUR_KIT, [
  ItemID.DARK_INFINITY_BOTTOMS
]);
untradeableItemMap.setValue(ItemID.INFINITY_HAT, [
  ItemID.DARK_INFINITY_HAT,
  ItemID.LIGHT_INFINITY_HAT
]);
untradeableItemMap.setValue(ItemID.LIGHT_INFINITY_COLOUR_KIT, [
  ItemID.LIGHT_INFINITY_HAT
]);
untradeableItemMap.setValue(ItemID.DARK_INFINITY_COLOUR_KIT, [
  ItemID.DARK_INFINITY_HAT
]);

// Miscellaneous ornament kits
untradeableItemMap.setValue(ItemID.DARK_BOW, [
  ItemID.DARK_BOW_12765,
  ItemID.DARK_BOW_12766,
  ItemID.DARK_BOW_12767,
  ItemID.DARK_BOW_12768,
  ItemID.DARK_BOW_20408
]);
untradeableItemMap.setValue(ItemID.ODIUM_WARD, [ItemID.ODIUM_WARD_12807]);
untradeableItemMap.setValue(ItemID.MALEDICTION_WARD, [
  ItemID.MALEDICTION_WARD_12806
]);
untradeableItemMap.setValue(ItemID.STEAM_BATTLESTAFF, [
  ItemID.STEAM_BATTLESTAFF_12795
]);
untradeableItemMap.setValue(ItemID.LAVA_BATTLESTAFF, [
  ItemID.LAVA_BATTLESTAFF_21198
]);
untradeableItemMap.setValue(ItemID.TZHAARKETOM, [ItemID.TZHAARKETOM_T]);
untradeableItemMap.setValue(ItemID.TZHAARKETOM_ORNAMENT_KIT, [
  ItemID.TZHAARKETOM_T
]);

// Slayer helm/black mask
untradeableItemMap.setValue(ItemID.BLACK_MASK, [
  ItemID.BLACK_MASK_I,
  ItemID.BLACK_MASK_1,
  ItemID.BLACK_MASK_1_I,
  ItemID.BLACK_MASK_2,
  ItemID.BLACK_MASK_2_I,
  ItemID.BLACK_MASK_3,
  ItemID.BLACK_MASK_3_I,
  ItemID.BLACK_MASK_4,
  ItemID.BLACK_MASK_4_I,
  ItemID.BLACK_MASK_5,
  ItemID.BLACK_MASK_5_I,
  ItemID.BLACK_MASK_6,
  ItemID.BLACK_MASK_6_I,
  ItemID.BLACK_MASK_7,
  ItemID.BLACK_MASK_7_I,
  ItemID.BLACK_MASK_8,
  ItemID.BLACK_MASK_8_I,
  ItemID.BLACK_MASK_9,
  ItemID.BLACK_MASK_9_I,
  ItemID.BLACK_MASK_10_I,
  ItemID.SLAYER_HELMET,
  ItemID.SLAYER_HELMET_I,
  ItemID.BLACK_SLAYER_HELMET,
  ItemID.BLACK_SLAYER_HELMET_I,
  ItemID.PURPLE_SLAYER_HELMET,
  ItemID.PURPLE_SLAYER_HELMET_I,
  ItemID.RED_SLAYER_HELMET,
  ItemID.RED_SLAYER_HELMET_I,
  ItemID.GREEN_SLAYER_HELMET,
  ItemID.GREEN_SLAYER_HELMET_I,
  ItemID.TURQUOISE_SLAYER_HELMET,
  ItemID.TURQUOISE_SLAYER_HELMET_I,
  ItemID.TWISTED_SLAYER_HELMET,
  ItemID.TWISTED_SLAYER_HELMET_I,
  ItemID.HYDRA_SLAYER_HELMET,
  ItemID.HYDRA_SLAYER_HELMET_I
]);

// Pharaoh's Sceptres
untradeableItemMap.setValue(ItemID.PHARAOHS_SCEPTRE, [
  ItemID.PHARAOHS_SCEPTRE_1
]);
untradeableItemMap.setValue(ItemID.PHARAOHS_SCEPTRE, [
  ItemID.PHARAOHS_SCEPTRE_2
]);
untradeableItemMap.setValue(ItemID.PHARAOHS_SCEPTRE, [
  ItemID.PHARAOHS_SCEPTRE_4
]);
untradeableItemMap.setValue(ItemID.PHARAOHS_SCEPTRE, [
  ItemID.PHARAOHS_SCEPTRE_5
]);
untradeableItemMap.setValue(ItemID.PHARAOHS_SCEPTRE, [
  ItemID.PHARAOHS_SCEPTRE_6
]);
untradeableItemMap.setValue(ItemID.PHARAOHS_SCEPTRE, [
  ItemID.PHARAOHS_SCEPTRE_7
]);
untradeableItemMap.setValue(ItemID.PHARAOHS_SCEPTRE, [
  ItemID.PHARAOHS_SCEPTRE_8
]);

// Revertible items
untradeableItemMap.setValue(ItemID.HYDRA_LEATHER, [ItemID.FEROCIOUS_GLOVES]);
untradeableItemMap.setValue(ItemID.HYDRA_TAIL, [ItemID.BONECRUSHER_NECKLACE]);
untradeableItemMap.setValue(ItemID.DRAGONBONE_NECKLACE, [
  ItemID.BONECRUSHER_NECKLACE
]);
untradeableItemMap.setValue(ItemID.BOTTOMLESS_COMPOST_BUCKET, [
  ItemID.BOTTOMLESS_COMPOST_BUCKET_22997
]);
untradeableItemMap.setValue(ItemID.BASILISK_JAW, [ItemID.NEITIZNOT_FACEGUARD]);
untradeableItemMap.setValue(ItemID.HELM_OF_NEITIZNOT, [
  ItemID.NEITIZNOT_FACEGUARD
]);
untradeableItemMap.setValue(ItemID.TWISTED_HORNS, [
  ItemID.TWISTED_SLAYER_HELMET,
  ItemID.TWISTED_SLAYER_HELMET_I
]);
untradeableItemMap.setValue(ItemID.ELDRITCH_ORB, [
  ItemID.ELDRITCH_NIGHTMARE_STAFF
]);
untradeableItemMap.setValue(ItemID.HARMONISED_ORB, [
  ItemID.HARMONISED_NIGHTMARE_STAFF
]);
untradeableItemMap.setValue(ItemID.VOLATILE_ORB, [
  ItemID.VOLATILE_NIGHTMARE_STAFF
]);
untradeableItemMap.setValue(ItemID.NIGHTMARE_STAFF, [
  ItemID.ELDRITCH_NIGHTMARE_STAFF,
  ItemID.HARMONISED_NIGHTMARE_STAFF,
  ItemID.VOLATILE_NIGHTMARE_STAFF
]);

// Crystal items
untradeableItemMap.setValue(ItemID.CRYSTAL_TOOL_SEED, [
  ItemID.CRYSTAL_AXE,
  ItemID.CRYSTAL_AXE_INACTIVE,
  ItemID.CRYSTAL_HARPOON,
  ItemID.CRYSTAL_HARPOON_INACTIVE,
  ItemID.CRYSTAL_PICKAXE,
  ItemID.CRYSTAL_PICKAXE_INACTIVE
]);
untradeableItemMap.setValue(ItemID.DRAGON_AXE, [
  ItemID.CRYSTAL_AXE,
  ItemID.CRYSTAL_AXE_INACTIVE
]);
untradeableItemMap.setValue(ItemID.DRAGON_HARPOON, [
  ItemID.CRYSTAL_HARPOON,
  ItemID.CRYSTAL_HARPOON_INACTIVE
]);
untradeableItemMap.setValue(ItemID.DRAGON_PICKAXE, [
  ItemID.CRYSTAL_PICKAXE,
  ItemID.CRYSTAL_PICKAXE_INACTIVE
]);
untradeableItemMap.setValue(ItemID.BLADE_OF_SAELDOR_INACTIVE, [
  ItemID.BLADE_OF_SAELDOR,
  ItemID.BLADE_OF_SAELDOR_C
]);
untradeableItemMap.setValue(ItemID.CRYSTAL_WEAPON_SEED, [
  ItemID.CRYSTAL_BOW,
  ItemID.CRYSTAL_BOW_24123,
  ItemID.CRYSTAL_BOW_INACTIVE
]);
untradeableItemMap.setValue(ItemID.CRYSTAL_WEAPON_SEED, [
  ItemID.CRYSTAL_HALBERD,
  ItemID.CRYSTAL_HALBERD_24125,
  ItemID.CRYSTAL_HALBERD_INACTIVE
]);
untradeableItemMap.setValue(ItemID.CRYSTAL_WEAPON_SEED, [
  ItemID.CRYSTAL_SHIELD,
  ItemID.CRYSTAL_SHIELD_24127,
  ItemID.CRYSTAL_SHIELD_INACTIVE
]);

// Bird nests
untradeableItemMap.setValue(ItemID.BIRD_NEST_5075, [
  ItemID.BIRD_NEST,
  ItemID.BIRD_NEST_5071,
  ItemID.BIRD_NEST_5072,
  ItemID.BIRD_NEST_5073,
  ItemID.BIRD_NEST_5074,
  ItemID.BIRD_NEST_7413,
  ItemID.BIRD_NEST_13653,
  ItemID.BIRD_NEST_22798,
  ItemID.BIRD_NEST_22800,
  ItemID.CLUE_NEST_EASY,
  ItemID.CLUE_NEST_MEDIUM,
  ItemID.CLUE_NEST_HARD,
  ItemID.CLUE_NEST_ELITE
]);

// Ancestral robes
untradeableItemMap.setValue(ItemID.ANCESTRAL_HAT, [
  ItemID.TWISTED_ANCESTRAL_HAT
]);
untradeableItemMap.setValue(ItemID.ANCESTRAL_ROBE_TOP, [
  ItemID.TWISTED_ANCESTRAL_ROBE_TOP
]);
untradeableItemMap.setValue(ItemID.ANCESTRAL_ROBE_BOTTOM, [
  ItemID.TWISTED_ANCESTRAL_ROBE_BOTTOM
]);
const itemMap = untradeableItemMap.keys().reduce((map, tradeableItem) => {
  const maps = untradeableItemMap.getValue(tradeableItem);
  maps.forEach(m => {
    m.forEach(untradeableItem => {
      map[untradeableItem] = [...(map[untradeableItem] || []), tradeableItem];
    });
  });
  return map;
}, {} as IdMappings);

export function getMappedItems(itemId: number) {
  const mappedItems = itemMap[itemId];
  if (!mappedItems || mappedItems.length === 0) return [itemId];
  return mappedItems;
}
