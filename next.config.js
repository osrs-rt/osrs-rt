require('dotenv-extended').load();
const nextSourceMaps = require('@zeit/next-source-maps')();
const nextImages = require('next-images');

const withSourceMaps = nextSourceMaps({
  env: {
    theme_colour: process.env.theme_colour,
    api_url: process.env.api_url,
    graphql_url: process.env.graphql_url,
    graphql_subscriptions_url: process.env.graphql_subscriptions_url,
    sentry_dsn: process.env.sentry_dsn,
    sentry_release: process.env.CI_COMMIT_SHA,
    secret_mode: process.env.secret_mode,
    site_name: process.env.site_name
  },

  webpack: (config, { isServer }) => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty'
    };

    // Add a graphql loader for client queries
    config.module.rules.push({
      test: /\.(graphql|gql)$/,
      exclude: /node_modules/,
      loader: 'graphql-tag/loader'
    });

    if (!isServer) {
      config.resolve.alias['@sentry/node'] = '@sentry/browser';
    }

    // Point socket io client to the slim build instead
    config.resolve.alias['socket.io-client'] = 'socket.io-client/dist/socket.io.slim';

    return config;
  }
  // target: process.env.NODE_ENV === 'production' ? 'serverless' : undefined
});

module.exports = nextImages(withSourceMaps);
