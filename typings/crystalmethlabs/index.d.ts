// Type definitions for crystalmethlabs 1.1.0
// Project: osrs-alcher-guide
// Definitions by: Edward Salter <mayhem366@hotmail.com>

declare module 'crystalmethlabs' {
  export const skills = {
    overall: 'overall',
    attack: 'attack',
    defence: 'defence',
    strength: 'strength',
    hitpoints: 'hitpoints',
    ranged: 'ranged',
    prayer: 'prayer',
    magic: 'magic',
    cooking: 'cooking',
    woodcutting: 'woodcutting',
    fletching: 'fletching',
    fishing: 'fishing',
    firemaking: 'firemaking',
    crafting: 'crafting',
    smithing: 'smithing',
    mining: 'mining',
    herblore: 'herblore',
    agility: 'agility',
    thieving: 'thieving',
    slayer: 'slayer',
    farming: 'farming',
    runecrafting: 'runecrafting',
    hunter: 'hunter',
    construction: 'construction'
  };

  export type Skill = keyof typeof skills;

  type SupportedGame = 'osrs' | 'rs3';

  type ApiReturnValue<T> = Promise<{ err?: string } & T>;

  interface EHPStat {
    hours: number;
    rank: number;
  }

  interface EHPTrackedStat {
    hours: number;
    ranksGained: number;
    ehpGained: number;
  }

  interface OfficialStat {
    level?: number;
    xp: number;
    rank: number;
  }

  type PlayerStats = Record<Skll, OfficialStat> & { ehp: EHPStat };

  interface CMLStat {
    xpGained: number;
    ranksGained: number;
    levelsGained: number;
    ehpGained: number;
  }

  type TrackingStat = CMLStat & OfficialStat;
  type PlayerTrackingStats = Record<Skill, TrackingStat> & {
    ehp: EHPTrackedStat;
  };

  interface GainRecord {
    day: number;
    week: number;
    month: number;
  }
  type PlayerRecord = Record<Skill, GainRecord> & { ehp: GainRecord };

  interface VirtualHiscores {
    total: {
      rank: number;
      level: number;
    };
    recordsHeld: {
      held: number;
      rank: number;
    };
    frontPageCount: {
      count: number;
      rank: number;
    };
  }

  export default class Crystalmethlabs {
    constructor(game?: SupportedGame = SupportedGame.osrs);

    update(username: string): ApiReturnValue;
    lastcheck(username: string): ApiReturnValue<{ sec?: number }>;
    lastchange(username: string): ApiReturnValue<{ sec?: number }>;
    stats(username: string): ApiReturnValue<{ stats?: PlayerStats }>;
    track(
      username: string,
      timeperiod: number | string
    ): ApiReturnValue<{ stats?: PlayerTrackingStats }>;
    recordsOfPlayer(
      username: string
    ): ApiReturnValue<{ records?: PlayerRecord }>;
    virtualHiscores(username: string): ApiReturnValue<{ vh?: VirtualHiscores }>;
    ttmn(
      username: string
    ): ApiReturnValue<{ ttm?: { hours: number; rank: number } }>;

    /*

// TODO: ADD SUPPORT FOR RS3 skills


    .skills
    // Varies depening on RS3 and OSRS gametype


    * .currentTop(skill, timeperiod) -> {err, top}
    *  // [ { username: 'eg_froggen', gained: 106.69 },
    //   { username: 'razor_beast', gained: 102.32 },
    //   { username: 'fk_wmk', gained: 100.93 },
    //   ...
    //   ...
    //   { username: 'abekat', gained: 68.17 } ]
    *
    *
    *
    * .records(skill, timeperiod, [count]) -> {err, records}
    * skill String that can be set to any skill, including 'overall' & 'ehp'
timeperiod String that can be set to day, week or month
count Number | String for how many records to be shown (optional, default: 30)
    * // [ { username: 'p_udding', hours: 621.6 },
    //   { username: 'lynx_titan', hours: 544.07 },
    //   { username: 'fredimmu', hours: 530.14 } ]
    *
    *
    *
    * .compTotal(compID, skill) -> {err, xp}
    *   // [ { username: 'awildcow',
    //     startXP: 76251226,
    //     currentXP: 78880141,
    //     gainedXP: 2628915 },
    //   ...
    //   ...
    //   { username: 'ge_tracker',
    //     startXP: 2686104,
    //     currentXP: 2954615,
    //     gainedXP: 268511 } ]
    *
    *
    *
    * .previousName(username) -> {err, username}
    *Checks if there was a previous username for the account. If there is, username is set to the previous username. If given person has never changed username, there will be an err that says 'User not in database', and username will be undefined
    *
    *
    *
    *
    * .search(username) -> {err, exists}
    *
    *
    *
    * .convertXPtoLVL(xp, [cap])
    *
    *
    * .convertLVLtoXP(lvl)
    *
    *     * */
  }
}
