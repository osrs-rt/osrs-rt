declare module '*.graphql' {
  import { DocumentNode } from 'graphql';
  const s: DocumentNode;
  export default s;
}

declare module '*.graphqls' {
  import { DocumentNode } from 'graphql';
  const s: DocumentNode;
  export default s;
}