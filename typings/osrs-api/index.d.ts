// Type definitions for osrs-api 0.1.2
// Project: osrs-alcher-guide
// Definitions by: Edward Salter <mayhem366@hotmail.com>


declare module 'osrs-api' {
  import { AxiosInstance } from 'axios';

  export namespace constants {
    export const skills = {
      overall: "overall",
      attack: "attack",
      defence: "defence",
      strength: "strength",
      hitpoints: "hitpoints",
      ranged: "ranged",
      prayer: "prayer",
      magic: "magic",
      cooking: "cooking",
      woodcutting: "woodcutting",
      fletching: "fletching",
      fishing: "fishing",
      firemaking: "firemaking",
      crafting: "crafting",
      smithing: "smithing",
      mining: "mining",
      herblore: "herblore",
      agility: "agility",
      thieving: "thieving",
      slayer: "slayer",
      farming: "farming",
      runecrafting: "runecrafting",
      hunter: "hunter",
      construction: "construction"
    };

    export const minigames = {
      easyClueScrolls: "easyClueScrolls",
      mediumClueScrolls: "mediumClueScrolls",
      clueScrolls: "clueScrolls",
      bountyHunter: "bountyHunter",
      bountyHunterRogues: "bountyHunterRogues",
      hardClueScrolls: "hardClueScrolls",
      lastManStanding: "lastManStanding",
      eliteClueScrolls: "eliteClueScrolls",
      masterClueScrolls: "masterClueScrolls"
    };

    export const playerTypes = {
      normal: "normal",
      ironman: "ironman",
      ultimateIronman: "ultimate",
      hardcoreIronman: "hardcore_ironman",
      deadman: "deadman",
      seasonal: "seasonal"
    };

    export const hiscoreSkillEntryOrder: Array<keyof typeof skills>;
    export const hiscoreMinigameEntryOrder: Array<keyof typeof minigames>;
    export const ingameSkillOrder: Array<keyof typeof skills>;
  }

  export namespace grandExchange {
    interface GrandExchangeInstance {
      getItem(itemId: number): Promise<ItemResult>;
      getGraph(itemId): Promise<GraphResult>;
    }

    export function createGrandExchangeInterface(axiosInstance: AxiosInstance): GrandExchangeInstance;
    export function getItem(itemId: number): Promise<ItemResult>;
    export function getGraph(itemId): Promise<GraphResult>;
  }

  export namespace hiscores {
    export interface Player {
      name: string;
      type: PlayerType;
    }

    type OptionalPlayer = Partial<Player>;

    interface HiscoresInstance {
      getPlayer(player: OptionalPlayer): Promise<HiscoreResult>;
      getPlayers(players: Array<OptionalPlayer>): Promise<Array<HiscoreResult>>;
    }

    export function createHiscoresInterface(axiosInstance: AxiosInstance): HiscoresInstance;
    export function getPlayer(player: OptionalPlayer): Promise<HiscoreResult>;
    export function getPlayers(player: Array<OptionalPlayer>): Promise<Array<HiscoreResult>>;
  }

  type HiscoreSkillResult = Record<Skill, {
    rank: number;
    level: number;
    experience: number;
  }>;

  type HiscoreMinigameResult = Record<Minigame, {
    rank: number;
    score: number;
  }>;

  type HiscoreResult = hiscores.Player & HiscoreSkillResult & HiscoreMinigameResult;

  interface GraphResult {
    daily: {
      [index: number]: number;
    }

    average: {
      [index: number]: number;
    }
  }


  interface ItemResult {
    item: Item;
  }

  export interface Item {
    icon: string;
    icon_large: string;
    id: number;
    type: ItemType;
    typeIcon: string;
    name: string;
    description: string;
    current: CurrentItemPriceTrend;
    today: CurrentItemPriceTrend;
    members: boolean;
    day30: HistoricalItemPriceTrend;
    day90: HistoricalItemPriceTrend;
    day180: HistoricalItemPriceTrend;
  }

  export enum ItemType {
    default = 'Default'
  }

  export enum TrendType {
    positive = "positive",
    neutral = "neutral",
    negative = "negative"
  }

  export interface CurrentItemPriceTrend {
    trend: TrendType;
    price: number | string;
  }

  export interface HistoricalItemPriceTrend {
    trend: TrendType;
    change: string;
  }

  type valueof<T> = T[keyof T];

  export type PlayerType = valueof<typeof constants.playerTypes> | null;
  export type Skill = keyof typeof constants.skills;
  export type Minigame = keyof typeof constants.minigames;
}