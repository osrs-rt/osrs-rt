process.env.BABEL_ENV = 'test';

module.exports = function(wallaby) {
  return {
    files: [
      'components/**/*.js',
      'components/**/*.ts?(x)',
      'sharedCode/**/*.ts?(x)',
      'pages/**/*.js',
      'pages/**/*.ts?(x)',
      'utils/**/*.js',
      'utils/**/*.ts?(x)',
      'server/**/*.js',
      'server/**/*.ts?(x)',
      '!**/*.test.js',
      '!**/*.test.ts?(x)',
      '!dist/**',
      '__mocks__/*'
    ],

    tests: [
      '**/*.test.js',
      '**/*.test.ts?(x)',
      '!node_modules/**',
      '!dist/**',
      '!.next/**'
    ],

    env: {
      type: 'node',
      runner: 'node'
    },

    compilers: {
      '**/*.js': wallaby.compilers.babel(),
      '**/*.ts?(x)': wallaby.compilers.babel()
    },

    testFramework: 'jest'
  };
};
